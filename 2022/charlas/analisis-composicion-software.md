---
layout: 2022/post
section: proposals
category: talks
author: Julian Coccia
title: Análisis de Composición de Software
---

# Análisis de Composición de Software

> Con los objetivos de cumplir con las licencias de software libre y monitorizar las eventuales vulnerabilidades de los componentes libres que usamos, surge la necesidad de contabilizar los componentes que usamos. Este inventario se conoce como SBOM (del inglés Software Bill of Materials). Las herramientas para generar dichos inventarios son tan caras que solo están al alcance de las grandes empresas. Felizmente, hoy existe una alternativa totalmente libre llamada SCANOSS y una fundación española que ofrece un API gratuito para la comparación e identificación de software.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla larga / Presencial

-   Descripción:

> El inventariado del software libre consta en la detección tanto del software declarado (software con ficheros LICENSE.txt, o con encabezados que declaran licencias y autores) como en la detección del software no declarado (que no contiene encabezados o ficheros de licencia). Existen numerosas herramientas libres para la detección de software declarado, pero la detección de lo no declarado requiere una base de datos de software libre. La Fundación para la Transparencia del Software provee un servicio gratuito para realizar este tipo de detecciones. Vamos a explicar cómo funciona todo esto y cómo se genera un inventario SBOM, que comienza a ser un requerimiento cotidiano a través de la cadena de suministro de software.

-   Web del proyecto: <https://scanoss.com>

-   Público objetivo:

> Personas con interés en la transparencia de software y cómo facilitar su gestión en proyectos de software libre.

## Ponente:

-   Nombre: Julian Coccia

-   Bio:

> Luego de 10 años construyendo las herramientas y procesos de gestión de software libre en Ericsson, he sido cofundador y CTO de FOSSID, una herramienta de análisis de composición de software en Suecia. Hoy estoy a cargo de la tecnología de SCANOSS, la primer plataforma libre para análisis de composición de software.

### Info personal:

-   Web personal: <https://www.linkedin.com/in/juliancoccia/>
-   GitLab (o cualquier sitio de código colaborativo) o portfolio: <https://github.com/scanoss>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
