---
layout: 2022/post
section: proposals
category: talks
author: Marelisa Blanco
title: Desarrolladores asalariados ¿de quién es la obra?
---

# Desarrolladores asalariados ¿de quién es la obra?

> Puede que nos hayan contratado para crear software u obras en general, o puede ser que esté trabajando y, de repente, me encuentre creando obras.  ¿A quién pertenecen? ¿Puedo ser trabajador y mantener los derechos de autor? Estas y otras preguntas serán respondidas en esta charla

## Detalles de la propuesta:

-   Tipo de propuesta: Charla corta / Presencial

-   Descripción:

> Cuando creamos obras en el entorno empresarial damos por hecho que los derechos son totalmente del empresario. Eso es debido a la presunción del artículo 51 de la ley de propiedad intelectual. Pero no siempre es así y, por su redacción escueta, la interpretación nos da multiples escenarios.
Por ejemplo, los derechos cedidos pueden pactarse en el contrato de trabajo, explicando el alcance que tiene. También podría verse afectada nuestra capacidad creativa si tenemos una cláusula de no competencia durante unos cuantos años después de finalizar la relación laboral.<br><br>
En el artículo 51 no queda del todo claro durante cuanto tiempo se realiza la cesión de los derechos a favor de la empresa, por lo que tendríamos que ver en qué supuesto estamos. La cesión legal se limita a los derechos necesarios para explotar la obra ¿Pero cual es el alcance? ¿El trabajador se quedará con algunos derechos para sí? Eso sin contar que nos hayan contratado específicamente para crear obras en una empresa que se dedicará a explotarlas de una manera u otra.<br><br>
Porque también podría ser un programador que le dio por automatizar parte de su trabajo o mejorar los programas existentes ¿En tales casos también se cede de manera automática? Como podemos ver hay muchas preguntas interesantes para despejar, y es mi intención dar respuesta a las mismas.

-   Público objetivo:

> Desarrolladores y creadores

## Ponente:

-   Nombre: Marelisa Blanco

-   Bio:

> Marelisa Blanco, Abogada y dibujante especializada en propiedad intelectual, me dedico a hacer sencillo lo complicado. Colaboradora de Nolegaltech.

### Info personal:

-   Web personal: <https://www.akme.es/#akme>
-   Mastodon (u otras redes sociales libres): <https://mastodon.social/@Wavemare>
-   Twitter: <https://twitter.com/@marelisablanco>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
