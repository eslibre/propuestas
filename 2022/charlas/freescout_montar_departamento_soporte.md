---
layout: 2022/post
section: proposals
category: talks
author: Pablo Hinojosa Nava
title: FreeScout&#58 Cómo montar un departamento de soporte/atención al cliente con software libre
---

# FreeScout: Cómo montar un departamento de soporte/atención al cliente con software libre

> Una necesidad común entre las empresas es la necesidad de una plataforma para gestionar la atención al cliente y/o el soporte técnico. Lo más común es utilizar soluciones privativas (por ejemplo Zendesk), sin embargo hay alternativas libres lo suficientemente maduras como para que sea la nueva puerta de entrada para los clientes y futuros clientes. En esta charla veremos el caso concreto de la puesta en producción del departamento de soporte de Bitergia de FreeScout.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla larga / Presencial

-   Descripción:

> Empezaremos analizando cuál era el problema a solucionar, estado actual y nuevos requisitos. Hablaremos sobre el proceso de investigación de alternativas plataformas de soporte (basadas en software libre y no), toma de decisiones y análisis de pros y contras de cada plataforma basándonos en requisitos técnicos y económicos (modelos de negocio de cada plataforma de soporte candidata).  Analizaremos el caso concreto de FreeScout, una comunidad en Github que tiene más de 1500 estrellas. Finalmente hablaremos sobre la puesta en pruebas y paso a producción de la plataforma.

-   Web del proyecto: <https://freescout.net/>

-   Público objetivo:

> Cualquier persona/empresa que ofrezca un servicio de atención al cliente. Cualquier persona que quiera vender por internet.

## Ponente:

-   Nombre: Pablo Hinojosa Nava

-   Bio:

> Pablo Hinojosa Nava trabaja en el Departamento de Soporte de Bitergia, una empresa basada en software libre. Linux Sysadmin que ha trabajado en varios departamentos de soporte (Gigas Hosting, Kanteron Systems), miembro de Wikimedia España y colaborador de varios proyectos de software libre. Le interesa todo lo relacionado con la cultura libre en general y el software libre en particular.

### Info personal:

-   Twitter: <https://twitter.com/pablohn6>
-   GitLab (o cualquier sitio de código colaborativo) o portfolio: <https://gitlab.com/Pablohn26>


## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
