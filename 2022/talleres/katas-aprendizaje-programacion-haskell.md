---
layout: 2022/post
section: proposals
category: workshops
author: Reynaldo Cordero
title: Katas de aprendizaje de programación con Haskell mediante serendipia
---

# Katas de aprendizaje de programación con Haskell mediante serendipia

> Gracias a la serendipia (hallazgo valioso que se encuentra de forma inesperada) es como se aprende a hablar en la infancia. Sin clases formales (o informales): simplemente te llega información hablada de TODO tipo y de CUALQUIER nivel en contextos REALES, donde todo resulta nuevo, sorprendente y verdadero y de alguna manera capturamos, relacionamos y manejamos esta información "al vuelo". Una especie de entrenamiento personal de impresionantes resultados.<br><br>
En el taller se mostrará cómo instalar y utilizar un entorno de aprendizaje de programación con Haskell mediante serendipia en tu ordenador 100% software libre, de forma nativa o en una máquina virtual. Tras el taller, tienes 1500 katas a tu disposición.<br><br>
El proyecto entero se puede clonar desde Gitlab, siendo sus elementos principales dos archivos de comandos bash (un instalador, y el programa newk), y se ha desarrollado en un GNU/Linux, Ubuntu 20.04. Se pretende crear una comunidad de software libre a su alrededor.

## Detalles de la propuesta:

-   Tipo de propuesta: Taller / Presencial

-   Descripción:

> El taller te puede interesar porque está diseñada para atender los siguientes objetivos:<br><br>
* Impacto en la educación secundaria y bachillerato<br>
<br>Si eres profesor de secundaria, este taller te interesa especialmente, porque sin interferir para nada en tu ámbito docente, puede llenar las aulas de alumnos que por su cuenta han entrenado (o sea, han "pilotado, y ya") libremente y según su propia iniciativa y estilo, elementos clave de su formación personal y académica.<br><br>
Esto dotaría a preadolescentes y adolescentes de un punto óptimo de madurez y motivación para sacarle partido a sus estudios, al entrenar: método científico, matemáticas avanzadas, expresividad en el lenguaje, capacidad de observación, redacción de comentarios eficaz, cuidado e importancia de los detalles, incluso sentido artístico.<br><br>
* Facilitar el acceso a la programación<br>
<br>Si te interesa la programación, seguramente te resultará estimulante un método de aprendizaje que no sea el típico de libro/artículo/tutorial/ejercicios/proyectos/profesores sino que se dirija a entrenar/experimentar/manipular/refactorizar con código real y diverso, y donde se auguran progresos constantes, sin cuellos de botella, y donde se puede controlar la dirección y el enfoque en cada momento.<br><br>
* Revitalizar el interés por el lenguaje Haskell<br>
<br>Al protagonizar Haskell esta nueva puerta de entrada a la programación, se espera un número importante de nuevos usuarios. Dado que el procedimiento resulta ágil, directo y provechoso, se desmantelan muchos de los tópicos negativos injustos que lastraban al lenguaje.<br><br>
* Relanzar los Meetups de Haskell<br>
<br>Unido al relanzamiento del propio lenguaje, y de la programación funcional en general, los Meetups de Haskell pueden ofrecer Hackathones muy originales, fáciles de organizar y de realizar, especializados en este tipo de entrenamiento.<br><br>
* Aumento del interés por el software libre y abierto<br>
<br>El proyecto está fundamentado en el empleo 100% de software libre, donde se emplean muchos proyectos individuales con una gran comunidad que los respalda, y muy asentados, pero siempre con el principio de poder incorporar o sustituir con facilidad cualquier parte según se requiera por parte de los usuarios.<br><br>
La base del proyecto está realizada por Reynaldo Cordero y por Mercedes Cordero, y en unos días publicaremos una actualización importante del código en la web del proyecto.<br><br>
Tengo previsto realizar cambios menores en la redacción de la propuesta.

-   Web del proyecto: <https://gitlab.com/HaskellKatas/katas--proof-of-concept>

-   Público objetivo:

>
* Profesorado en general, especialmente de ESO y Bachillerato.
* Personas interesadas en aprender a programar desde cero o que quieran mejorar sus habilidades de programación.
* Personas con interés en los lenguajes de programaciónb
* SysOps y gente interesada en el software libre que tengan interés en combinar grandes proyectos de software para dotar de flexibilidad a un proyecto concreto.

## Ponente:

-   Nombre: Reynaldo Cordero

-   Bio:

> Analista/Programador en los Servicios Informáticos de la Universidad de Alcalá. Organizador en el Meetup HaskellMAD.

### Info personal:

-   Twitter: <https://twitter.com/naldoco>
-   GitLab (o cualquier sitio de código colaborativo) o portfolio: <https://gitlab.com/HaskellKatas/>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
