---
layout: 2022/post
section: proposals
category: workshops
author: Alexander Sander &#38; Lina Ceballos
title: Public Money? Public Code! - Contact your administration
---

# Public Money? Public Code! - Contact your administration

> Our initiative “Public Money? Public Code!” has the purpose that Software, funded by public money and used in the public administration shall be Free Software. In this workshop we want to show you how to convince your local administration or other institutions, decision makers and parties to use more Free Software in the future.

## Detalles de la propuesta:

-   Tipo de propuesta: Taller / Presencial

-   Descripción:

> We have an open letter, which you can sign as an individual or as a Non-governmental organization (NGO). So far we have over 29.000 people and nearly 200 NGOs supporting our goal. And already five public entities, the Swedish JobTech Development, the German "Samtgemeinde Elbmarsch", the Spanish parliament from Asturias "Junta General del Principado de Asturias", the Spanish city of Benigànim, and the Spanish city of Barcelona have signed our open letter. We would like to have more administrative units supporting our cause. But to achieve this we need your help. We will show you how we convince local administrations or other institutions, decision makers and parties to use more Free Software in the future.<br><br>
This Workshop is connected to the talk "What role did Free Software play during the corona crisis?"

-   Web del proyecto: <https://publiccode.eu/>

-   Público objetivo:

> Beginners, Activists

## Ponente:

-   Nombre: Alexander Sander, Lina Ceballos

-   Bio:

> Alexander has studied politics in Marburg, Germany and later has been an MEP Assistant in Brussels for three years and the General Manager of Digitale Gesellschaft e.V. in Berlin for four years. Furthermore he is the founder of NoPNR!, a campaign against the retention of travel data. He now works as a Policy Consultant for the Free Software Foundation Europe.<br><br>
Lina makes part of the Legal and Policy team at FSFE. She has acquired experience in advocating among decision makers and public administrations, legal affairs and licensing compliance as well as community building. She contributes to the Public Money? Public Code! initiative, the REUSE project and Router Freedom campaign.

### Info personal:

-   Mastodon (u otras redes sociales libres): <https://mstdn.io/@lexelas>
-   Twitter: <https://twitter.com/@lexelas> / <https://twitter.com/@lnceballosz>
-   GitLab (o cualquier sitio de código colaborativo) o portfolio: <https://github.com/lnceballosz>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
