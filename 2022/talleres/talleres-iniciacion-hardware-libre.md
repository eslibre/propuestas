---
layout: 2022/post
section: proposals
category: workshops
author: Bricolabs
title: Talleres de Iniciación al Hardware Libre
---

# Talleres de Iniciación al Hardware Libre

> Desde Bricolabs os proponemos un taller abierto de iniciación al Hardware Libre con dos montajes:
- Drawdio
- Oshwipin

## Detalles de la propuesta:

-   Tipo de propuesta: Taller / Presencial

-   Descripción:

> Nos gustaría presentar al público en general el concepto de Hardware Libre (O más concretamente Hardware de Diseño Abierto). De momento tenemos pensado acudir cinco personas durante una de las jornadas de esLibre.<br>
* Oshwipin:<br><br>
Un pin (o broche) de electrónica textil, los participantes pueden experimentar algunas técnicas para crear circuitos electricos con materiales textiles con un montaje muy sencillo. Los participante se pueden llevar puesto el montaje al acabar el taller :-)<br><br>
Enlace: <https://bricolabs.cc/wiki/proyectos/oshwi_broches_diy><br><br>
* Drawdio:<br><br>
Un generador de sonido inspirado en el conocido montaje Makey Makey. El Drawdio es un circuito relativamente sencillo y fácil de montar; basta con soldar los componentes a la placa de circuito impreso. El circuito no requiere ninguna programación para funcionar. Al finalizar el taller el participante puede llevarse el Drawdio construido. Creemos que sería factible soldar la placa en 2 horas, aunque quizás iríamos un poco justos.<br><br>
Enlace: <https://github.com/UC3Music/drawdio><br><br>
ES NECESARIO REGISTRARSE PARA ASISTIR A ESTE TALLER POR CUESTIONES DE DISPONIBILIDAD DE RECURSOS: <a href="https://eventos.librelabgrx.cc/events/2d429217-1456-4209-adbb-956658fdea6b" target="_blank">https://eventos.librelabgrx.cc/events/2d429217-1456-4209-adbb-956658fdea6b</a>

-   Web del proyecto: <https:/bricolabs.cc/wiki>

-   Público objetivo:

> Los talleres pretenden presentar el Hardware Libre al público en general. No son necesarios conocimientos previos de ninguna materia en especial, aunque conocimientos de cualquier lenguaje de programación serían muy útiles para la parte que implica escribir software.<br><br>
Los talleres están abiertos a mayores de 12 años y menores de 12 acompañados por un adulto. En total quince plazas.
- Para el montaje del Drawdio que implica soldadura de componentes nosotros ponemos los soldadores.
- Para la parte de textiles nosotros pondríamos todo el material.

## Ponente:

-   Nombre: Bricolabs

-   Bio:

> Bricolabs es una asociación con base en el Museo Domus de A Coruña, donde se reune gente amante del cacharreo de forma semanal. La asociación intenta difundir los valores y filosofía del Hardware y Software libre. Mas información en la web (<https://bricolabs.cc/>) y en la wiki (<https://bricolabs.cc/wiki>)

### Info personal:

-   Web personal: <https://bricolabs.cc/>
-   Twitter: <https://twitter.com/@Brico_Labs>
-   GitLab (o cualquier sitio de código colaborativo) o portfolio: <https://gitlab.com/brico-labs>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
