---
layout: 2022/post
section: proposals
category: workshops
author: Paula de la Hoz
title: Investigando amenazas en Linux
---

# Investigando amenazas en Linux

> En el taller analizaremos la estructura de malware creado específicamente para Linux, qué partes tiene y de qué manera podemos prevenir y actuar contra este tipo de amenazas desde la terminal en un equipo personal.

## Detalles de la propuesta:

-   Tipo de propuesta: Taller / Presencial

-   Descripción:

> Se tiene la creecia de que por utilizar Linux se está completamente seguro contra malware, virus, ransomware, etc, sin embargo esto no es cierto. La mayoría de ataques están dirigidos a Windows por una cuestión de porcentaje, pero existen varias amenazas contra Linux. En el taller analizaremos paso a paso uno de esos malwares, qué impacto tienen un equipo de Linux, y qué podemos programar tareas de hardening para protegernos frente a estas amenazas.

-   Público objetivo:

> Personas que tengan un equipo de Linux y tengan un conocimiento más o menos básico de bash y comandos de Linux.

## Ponente:

-   Nombre: Paula de la Hoz

-   Bio:

> Analista de ciberinteligencia en Sentinel One, profesora del grado de FP de ciberseguridad en Salesianos Atocha (Madrid). Presidenta cofundadora de la asociación Interferencias y colaboradora del programa Post Apocalipsis Nau (Radio Vallekas-El Salto radio). Entusiasta del software libre y la seguridad informática, escribo de SL y feminismo en El Binario y de ciberseguridad en <dev.to/terceranexus6>.

### Info personal:

-   Web personal: <https://terceranexus6.gitlab.io/website/>
-   Mastodon (u otras redes sociales libres): <https://cybre.space/@terceranexus6/>
-   Twitter: <https://twitter.com/@Terceranexus6>
-   GitLab (o cualquier sitio de código colaborativo) o portfolio: <https://gitlab.com/terceranexus6>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
