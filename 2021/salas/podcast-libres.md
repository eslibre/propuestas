---
layout: 2021/post
section: proposals
category: devrooms
title: Podcast Libres
---

## Descripción de la sala

Nos gustaría colaborar con la esLibre este año creando un puente entre el podcasting y los creadores de contenido de cultura libre y software libre en youtube/Fediverso con el evento de esLibre. Con el objetivo de sumar a la difusión del evento en podcast y canales de youtube/Peertube para aumentar la contribución a otras comunidades a esLibre.

Nuestra idea es la de crear una sala en la que se emitan grabaciones en diferido de podcast de diferentes referentes de estas comunidades y hacer grabaciones antes y después del evento que mejoren su difusión en estas comunidades.

En la sala, emitiremos tanto los programas especiales de los distintos proyectos que han dado promoción al evento con episodios especiales como entrevistas a algunos partipantes de otras salas del evento, como audios y vídeos grabados para su difusión en diferido en el evento que luego se dejarán disponibles en los podcast correspondientes.

Para finalizar realizaremos un directo para evaluar la experiencia.

## Comunidad que la propone

Sala organizada por Jorge Lama y David Vaquero. Ambos colaboramos activamente en la comunidad de cultura libre y software libre, lo hacemos a nivel personal. A los que ya se han sumado : Juan Febles (PodcastLinux), David Marzal, Voro y Julian (GNU/Linux Valencia), José Jimenez (Tomando un cafe), José Picon (Somos Tecnologic@s), Angel (uGeek), Paco Estrada (Compilando Podcast), Samuel (Yo Virtualizador), Pedro (Mosquetero Web) y Francisco Javier Teruelo (Ubuntu y otras hiervas) entre otros.

-   Web de la comunidad: <http://cursosdedesarrollo.com>
-   Mastodon (u otras redes sociales libres):
-   Twitter:
-   GitLab:
-   Portfolio o GitHub (u otros sitios de código colaborativo):

### Contacto(s)

-   Nombre de contacto: David Vaquero
-   Email de contacto: <pepesan@gmail.com>

## Público objetivo

Todo aquel interesado en la organización y promoción de un evento de software y cultura libres.

## Formato

Podcasts grabados y emitidos en diferido en una sala a parte en el evento + un directo en la última franja del sábado. Se podrán seguir a traves de la sala de BBB y comentar desde <a href="https://rocketchat.librelabucm.org/channel/Sala_Podcast_Libres" target="_blank">Rocket Chat</a>.

## Viernes 25

<h3 id="eslibre2021-RW">11:00-11:35 - Congreso esLibre 2021 según República Web</h3>

-   **Participantes**: Andros Fenollosa y David Vaquero.

-   **Descripción**: En esta charla hablaremos del congreso de esLibre, explicaremos las salas que hay presentes, los colaboradores principales, y destacaremos algunas charlas que pueden resultar interesantes para la audiencia del congreso.

-   **Disponible en**: <https://republicaweb.es/podcast/nos-vamos-al-congreso-eslibre-2021/>

<h3 id="eslibre2021-gnu-linux-valencia">11:35-12:13 - Especial esLibre 2021 GNU/Linux Valencia</h3>

-   **Participantes**: Alejandro, Taraak y David Marzal + Paula de la Hoz.

-   **Descripción**: Buenas, el episodio de hoy es un especial sobre el evento esLibre 2021, en el que Alejandro, Taraak y David Marzal hablaran sobre diferentes charlas, talleres y salas que hay aceptadas para el evento de este año.
Ademas podréis escuchar una breve entrevista con una de las organizadoras del evento, Paula de la Hoz que ademas es co-fundadora de interferencias (una de las salas que habrá disponibles). En esta charla hablaremos sobre que se puede encontrar quien acuda a esta edición online y en particular sobre las charlas que impartirá Paula.

-   **Disponible en**: Artículo original del episodio en <https://gnulinuxvalencia.org/podcast-eslibre-2021/> y de la entrevista en <https://gnulinuxvalencia.org/promo-eslibre-2021-con-paula-de-la-hoz/>


<h3 id="eslibre-podcastlinux">12:30-13:40 - #132 Especial Es_Libre 2021</h3>

-   **Participantes**: Paula de la Hoz, Germán Martínez, Rafael Mateus.

-   **Descripción**: ¡¡¡Muy buenas amante del Software Libre!!!
Bienvenido a otra entrega de Podcast Linux, la número 132. Un saludo muy fuerte de quien te habla, Juan Febles. Hoy tenemos un especial sobre el evento esLibre, edición online organizada por LibreLabUCM el 25 y 26 de junio.
Tenemos con nosotros a tres de sus organizadores.

-   **Disponible en**: <https://podcastlinux.com/posts/podcastlinux/132-Podcast-Linux/>

<h3 id="luis-fajardo-luis-falcon">16:00-16:21 - Entrevistas a Luis Fajardo y Luis Falcón</h3>

-   **Participantes**: Luis Fajardo y Luis Falcón con David Marzal.

-   **Descripción**: En estos audios, entrevistamos brevemente por separado a dos ponentes plenarios de esLibre, para que nos cuenten un poco sobre ellos y su participación en el evento de este año.

-   **Disponible en**: Artículos originales de sus respectivos episodios <https://gnulinuxvalencia.org/promo-eslibre-2021-con-luis-fajardo/> y <https://gnulinuxvalencia.org/promo-eslibre-2021-con-luis-falcon/>


<h3 id="mancomun-podcast">16:25-17:15 - esLibre con Rafael Mateus, Pedro Javier Fernández y Germán Martínez.
</h3>

-   **Participantes**: Brais Arias, Jorge Lama, Rafael Mateus, Pedro Javier Fernández y Germán Martínez.

-   **Descripción**: El Congreso esLibre, uno de los eventos a nivel español más importantes relacionados con el software libre y la cultura libre, tendrá lugar este año de forma virtual el 25 y 26 de junio. esLibre es un encuentro de personas interesadas en las tecnologías libres, enfocado a compartir conocimiento y experiencia alrededor de las mismas. Hoy contamos con varios miembros del equipo anfitrión de esta edición: Rafael Mateus y Pedro Javier Fernández, y también con Germán Martínez, de la organización general.

-   **Disponible en**: <https://www.mancomun.gal/es/noticias/57-mancomun-podcast-eslibre-con-rafael-mateus-pedro-javier-fernandez-y-german-martinez/>

<h3 id="drupal-2021">17:20-18:45 - Estado del desarrollo de Drupal en 2021</h3>

-   **Participantes**: Borja Vicente, Javier Archeni, David Vaquero

-   **Descripción**: Vuelve Drupal al podcast y para esta ocasión contamos con la compañía del desarrollador web especializado en Drupal Borja Vicente, creador de la web y el canal [escueladrupal.com](https://escueladrupal.com/). Invitamos a Borja para que nos cuente muchas cosas sobre su proyecto y el estado actual de Drupal. Borja desarrolla en backend con Drupal desde hace más de 10 años y actualmente su labor profesional se desarrolla con Drupal, aunque suele también experimentar con Symfony, Laravel, Django y también Ruby on Rails. Con Borja tratamos muchas cuestiones relacionados con Drupal y su proyecto de contenidos:
    - Evolución de Drupal hasta su última versión
    - Comunidad Drupal a nivel internacional y nacional
    - Proyectos o casos de uso ideales para desarrollar con Drupal 9
    - Recomendaciones esenciales para gestionar una instalación de Drupal
    - Motivaciones para crear Escuela Drupal y lo que gustaría conseguir
    - Entorno de desarrollo usado para Drupal
    - Módulos que más te ayudan al desarrollo web con Drupal

Muy agradecidos a Manu por propiciar en Twitter este episodio sobre Drupal con Borja.

-   **Disponible en**: <https://republicaweb.es/podcast/estado-de-drupal-con-borja-vicente-de-escueladrupal-com/>

## Sábado 26

<h3 id="software-libre-comunitat-valenciana">11:00-11:50 - La influencia de la política en la implementación del Software Libre. Nuestra experiencia en la Comunitat Valenciana.
</h3>

-   **Participantes**: Julian y Voro, de GNU/Linux Valencia.

-   **Descripción**: En el audio explicamos los últimos movimientos producidos en la Comunitat Valenciana, que consisten en la adquisición de licencias y servicios privativos. Una maniobra inexplicable, que pone en peligro la persistencia del Software Libre, en un lugar donde todo funciona bien y no es necesaria esta regresión técnica y social. Paradójicamente, esta iniciativa está promovida por el gobierno mas progresista que hemos tenido en esta Comunidad. Una vez mas, queda claro que la implementación del Software Libre en la administración y educación no tiene nada que ver con las razones éticas, ni con la cuestión económica. Simplemente es una cuestión política.

-   **Disponible en**: <https://devtube.dev-wiki.de/videos/watch/7860fffc-d7cb-4803-b9ba-4946dbaa36c7>

<h3 id="pentesting-movil">11:50-12:29 - Pentesting con el móvil</h3>

-   **Participantes**: Luis Madero

-   **Descripción**: Luis Madero nos va hacer una demostración de cómo encontrar vulnerabilidades en un entorno controlado. Lo va hacer desde el móvil y con software libre. Organizado por Somos tecnológicos: Érica Aguado, Gabs García y José Picón.

-   **Disponible en**: <https://devtube.dev-wiki.de/videos/watch/b01262be-9b95-4cb5-961b-5b35ec4116c0>

<h3 id="nolegaltech-tech-ladies">12:30-13:25 - Entrevistas a Andrea de NOlegaltech Radio y a Cris Pampín de Tech&Ladies PODCAST</h3>

-   **Participantes**: Andrea de NOlegaltech Radio y Cris Pampín de Tech&Ladies PODCAST.

-   **Descripción**: Charla entre Andrea y Cris donde comentan herramientas libres que usan en su trabajo, licencias, organización de eventos, hablan sobre sus podcast y alguna otra cosilla.

-   **Disponible en**: <https://devtube.dev-wiki.de/videos/watch/f97a3ec0-fab0-4919-933f-a1612d7fcfe1>

<h3 id="software-libre-cientifico-administrador-programador">16:00-17:15 - ¿Qué es el software libre para un científico, administrador o un programador?</h3>

-   **Participantes**: Pablo Martínez, JJ Melero y Jorge Aguilera.

-   **Descripción**: Charla para conocer los puntos de vista del software libre desde diversos perfiles, como lo usan , las ventajas y desventajas que encuentran. En definitiva, ver la utilidad del software libre en diferentes campos.

-   **Disponible en**: <https://anchor.fm/tomandouncafe/episodes/Tomando-un-Caf-90-Charla-para-EsLibre-sobre-Software-Libre-e13epvj>

<h3 id="mesa-redonda-sala-podcast">17:30-18:45 - Mesa redonda sobre la sala de podcasting</h3>

-   **Participantes**: Organizadores de la sala

-   **Descripción**: Debatiremos como ha sido la experiencia, puntos de mejora para siguientes ediciones...

-   **Disponible en**: <https://w.wiki/4Jsj>

<h3 id="uGeek-podcast">Audio extra de uGeek: Mis servicios favoritos</h3>

-   **Disponible en**: <https://ugeek.github.io/post/2021-06-26-mis-servicios-favoritos-2021-eslibre2021.html>
