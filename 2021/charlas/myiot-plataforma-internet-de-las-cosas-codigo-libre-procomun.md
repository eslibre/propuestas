---
layout: 2021/post
section: proposals
category: talks
author: Juan Félix Mateos Barrado
title: myIoT&#58 una plataforma de Internet de las cosas de código libre en procomún
---

myIoT es un fork de ThingsBoard CE desarrollado por la comunidad The Things Network Madrid con los siguientes principios:

1.  Tus datos son tus datos
2.  No segregación de los usuarios (unas solas credenciales para acceder a todos sus dispositivos)
3.  Sostenido y gestionado por su comunidad de usuarios sin ánimo de lucro
4.  Favorecer el despliegue de redes IoT abiertas, libres, neutrales, seguras y útiles en procomún

Con myIoT podrás monitorizar y controlar dispositivos IoT, presentar sus datos, configurar alarmas...

## Formato de la propuesta

Indicar uno de estos:

-   [ ]  Charla corta (10 minutos)
-   [x]  Charla (25 minutos)

## Descripción

myIoT permite a cualquier usuario monitorizar y controlar todos sus dispositivos IoT, independientemente de la tecnología que utilicen (WiFi, Bluetooth, LoRaWAN...).

Los dispositivos pueden organizarse jerárquicamente y representarse en mapas o planos.

La plataforma dispone de una biblioteca creciente de tipos de dispositivos, pero si no encuentras el que necesitas, podrás creártelo tú mismo.

Los dispositivos envían los datos a myIoT a través de peticiones HTTP o MQTT, pero también pueden recibir instrucciones desde la plataforma, logrando así la bidireccionalidad necesaria para gestionar actuadores.

Un usuario puede delegar uno de sus dispositivos en cualquier otro usuario, definiendo los privilegios que desea otorgarle sobre él, como visualizar sólo ciertas telemetrías, o configurar sólo algunos de sus parámetros de funcionamiento.

Un proveedor de servicios puede pre-aprovisionar dispositivos en myIoT para que posteriormente sus clientes los reclamen simplemente indicando unas credenciales; de este modo se reduce considerablemente la barrera de acceso para usuarios comunes.

myIoT dispone de un sistema de notificaciones en el que el usuario puede elegir entre correo electrónico, IFTTT, Telegram, Matrix...

-   Web del proyecto: <https://my.iotopentech.io>

## Público objetivo

- Todos los públicos
- Programadores Java / JavaScript / AngularJS
- Desarrolladores hardware de dispositivos IoT

## Ponente(s)

- Juan Félix Mateos Barrado
- Miembro de la comunidad The Things Network Madrid
- Profesor de nuevas tecnologías
- Desarrollador de prototipos electrónicos

### Contacto(s)

-   Nombre: Juan Felix Mateos
-   Email: <juanfelixmateos@gmail.com>
-   Web personal:
-   Mastodon (u otras redes sociales libres):
-   Twitter: <https://twitter.com/juanfelixmateos>
-   GitLab:
-   Portfolio o GitHub (u otros sitios de código colaborativo): <https://github.com/IoTopenTech/myIoTopenTech>

## Comentarios
