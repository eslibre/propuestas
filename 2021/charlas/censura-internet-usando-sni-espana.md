---
layout: 2021/post
section: proposals
category: talks
author: Eduardo Collado Cabeza
title: Censura en Internet usando el SNI en España
---

En España se están bloqueando webs aprovechando el diseño de la extesión SNI (Server Name Indication) de TLS.

SNI manda el primer paquete con el destino sin encriptar, de forma que es posible ver en tráfico https con certificados que hagan uso de SNI (la mayoría) cual es la web visitada.

Gracias a este diseño es posible filtrar tráfico por parte de las operadoras.

## Formato de la propuesta

Indicar uno de estos:
-   [ ]  Charla corta (10 minutos)
-   [x]  Charla (25 minutos)

## Descripción

La charla propuesta constaría de tres partes:

* Introducción
* Definición y explicación de conceptos
* Demostración

Se pretende mostrar a los asistentes el funcionamiento de TLS con la extensión SNI y mostrar cómo se pueden bloquear webs.

El problema que aquí se describe ataca directamente a la libertad de la gente y de la sociedad pues igual que se pueden bloquear webs con contenidos de los denominados "piratas" también se pueden bloquear web de partidos políticos, asociaciones o de cualquier otro tipo.

Estas prácticas suponen un riesgo a la integridad de la información que se puede consumir en internet y proporciona una herramienta de censura muy importante a cargo de gobiernos y grandes compañías.

Existen nuevos enfoques que tratan de solucionar este problema para que el tráfico no pueda ser capturado y bloqueado, pero a día de hoy vivimos en con un Internet que, al menos en las operadoras principales, está siendo inspeccionado paquete a paquete con el riesgo que tiene.

El objetivo es concienciar de estos problemas y riesgos.

-   Web del proyecto:

## Público objetivo

Público con conocimientos técnicos e interés que puedan ayudar a expandir el conocmiento sobre este la censura en internet y que es un tema que nos afecta a todos.

## Ponente(s)

Profesionalmente me dedico al mundo del networking (redes) desde hace más de 20 años, además he sido profesor en la Universidad Antonio de Nebrija e instructor de Cisco.

Actualmente desarrollo mi actividad profesional en Tecnocrática Centro de Datos y mantengo un podcast con contenido técnico sobre redes y sistemas.

### Contacto(s)

-   Nombre: Eduardo Collado Cabeza
-   Email: <edu@eduangi.com>
-   Web personal: <https://www.eduardocollado.com>
-   Mastodon (u otras redes sociales libres): <https://mastodon.social/@ecollado>
-   Twitter: <https://twitter.com/ecollado>
-   GitLab:
-   Portfolio o GitHub (u otros sitios de código colaborativo): <https://github.com/educollado>

## Comentarios
