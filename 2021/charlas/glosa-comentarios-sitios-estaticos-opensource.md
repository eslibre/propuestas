---
layout: 2021/post
section: proposals
category: talks
author: Andros Fenollosa Hurtado
title: Glosa, comentarios para sitios estáticos
---

Glosa es una solución open source que intenta acabar con la gran dependencia que existe sobre algunos servicios como Disqus y otros sistemas que incorporan comentarios a sitios estáticos haciéndote dueño de tu información.

## Formato de la propuesta

Indicar uno de estos:
-   [ ]  Charla corta (10 minutos)
-   [x]  Charla (25 minutos)

## Descripción

Glosa es una solución open source que intenta acabar con la gran dependencia que existe sobre algunos servicios como Disqus y otros sistemas que incorporan comentarios a sitios estáticos. Ofreciendo todo lo necesario para independizarse a nivel tecnológico: API, integración en HTML, PWA para su administración e importador.

Está construido sobre Clojure usando Tadam Web Framework y Vue para el front-end, además de mucha pasión y amor por el software libre.

-   Web del proyecto: <https://github.com/glosa/>

## Público objetivo

- Blogueros.
- Desarrolladores Web.
- Quienes busquen ser dueños de su información.
- DevOps.

## Ponente(s)

Es desarrollador Fullstack, Project Manager en Sapps Estudio. Compagina su actividad profesional con la docencia, siendo su mayor pasión. Ha dado otras charlas y talleres como la PyConES y WordCamp. Colabora en el Podcast "República Web", bloguero, enamorado de la programación funcional y donante de código en diversos proyectos open source.

### Contacto(s)

-   Nombre: Andros Fenollosa Hurtado
-   Email: <andros@fenollosa.email>
-   Web personal: <https://programadorwebvalencia.com/>
-   Mastodon (u otras redes sociales libres): <https://mastodon.technology/@androsfenollosa>
-   Twitter: <https://twitter.com/androsfenollosa>
-   GitLab: <https://gitlab.com/tanrax>
-   Portfolio o GitHub (u otros sitios de código colaborativo): <https://github.com/tanrax/>

## Comentarios
