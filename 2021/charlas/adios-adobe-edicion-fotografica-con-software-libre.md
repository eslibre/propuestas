---
layout: 2021/post
section: proposals
category: talks
author: Juanjo Salvador
title: ¡Adiós Adobe! Edición fotográfica con software libre
---

Como aficionado a la fotografía, cada vez que he entablado conversación con gente del gremio, ha salido a relucir la creencia de que sin un Mac y la suite de Adobe, no vas a ningún lado. Pero, ¿y si te dijese que eso es totalmente falso? ¿Y si te dijese que puedes lograr grandes trabajos, utilizando solamente software libre? Es hora de cazar el mito.

## Formato de la propuesta

Indicar uno de estos:
-   [ ]  Charla corta (10 minutos)
-   [x]  Charla (25 minutos)

## Descripción

Adobe ha dominado durante muchos años las suites de creatividad tales como Photoshop, Premiere, Lightroom, etc. Sin embargo, desde hace unos años, cada vez existen más alternativas libres y opensource que son capaces de igualar las capacidades de las herramientas privativas.

Algunas de las cosas que veremos son:

* GIMP, aquella que muchos conocemos pero sin conocer su verdadero poder.
* Darktable, revelado digital que no tiene nada que envidiar.
* Digikam, herramienta profesional con la potencia de una comunidad.
* Siril, para procesado de imágenes astronómicas.

-   Web del proyecto:

## Público objetivo

Cualquiera con inquietudes sobre fotografía, desde el más amateur hasta el profesional.

## Ponente(s)

Programador aficionado a la fotografía, usuario de Linux y responsable a tiempo parcial de edición y corrección de imágenes de recreación histórica. Es la primera charla que doy sobre este tema concreto, pero acumulo un par de años de trabajo con las herramientas mencionadas.

### Contacto(s)

-   Nombre: Juanjo Salvador
-   Email:
-   Web personal: <https://jsalvador.me/>
-   Mastodon (u otras redes sociales libres): <https://mastodon.social/@jsalvador>
-   Twitter: <https://twitter.com/Linuxneitor>
-   GitLab: <https://gitlab.com/JuanjoSalvador>
-   Portfolio o GitHub (u otros sitios de código colaborativo):

## Comentarios
