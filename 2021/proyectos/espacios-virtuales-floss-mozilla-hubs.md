---
layout: 2021/post
section: proposals
category: projects
title: Espacios Virtuales FLOSS&#58 Mozilla Hubs
---

En el grupo AVFloss venimos utilizando la plataforma Mozilla Hubs para reuniones de trabajo, realizar talleres, etc...

Es una plataforma accesible tanto desde móviles, PCs o gafas virtuales que permite la creación e investigación en un entorno VR que usualmente es privativo y elitista.

Estamos editando una serie de video-tutoriales en español, sobre su uso, administración  y creación de espacios XR, que estarán disponibles para las fechas del congreso esLibre.

-   Web del proyecto: <https://github.com/mozilla/hubs>

### Contacto(s)

-   Nombre: AVFloss
-   Email:
-   Web personal: <https://avfloss.github.io/>
-   Mastodon (u otras redes sociales libres): <https://floss.social/@av>
-   Twitter: <https://twitter.com/AV_floss>
-   GitLab:
-   Portfolio o GitHub (u otros sitios de código colaborativo): <https://github.com/AVFLOSS/avfloss.github.io>

## Comentarios
