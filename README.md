## esLibre 2024

- Envío de propuestas (info en Español): <https://eslib.re/2024/propuestas/>
- Submission of proposals (info in English): <https://eslib.re/2024/proposals/>

- Fecha límite segunda fase / Deadline second phase: **31 de enero / March 31th**
