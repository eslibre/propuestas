---
layout: 2024/post
section: proposals
category: devrooms
author: Oficinas de Software Libre y de Conocimiento Abierto de Universidad españolas
title: Conocimiento abierto en la Universidad
---

# Conocimiento abierto en la Universidad

## Detalles de la propuesta:

>La Universidad Pública Española ha recibido recientemente cambios legislativos significativos.<br><br>
Por un lado, la aprobación de la LOSU ha cambiado el marco de trabajo de los equipos rectorales de 4 años (más otros 4 posibles en caso de reelección) a un único mandato de 6. Esto hace que los equipos que entran desde este año enmarquen su trabajo hasta el 2030, año en que la mayoría de instituciones se han comprometido a conseguir los Objetivos de Desarrollo Sostenible (ODS). El Software Libre y el Conocimiento Abierto pueden jugar un papel fundamental en este camino.<br><br>
Por otro lado, la convocatoria para la evaluación de la actividad investigadora (sexenios) de 2023 por primera vez se recoge la valoración de otros resultados más allá de las publicaciones científicas, como pueden ser datos, códigos, software, creaciones artísticas, patentes y registros de propiedad intelectual, o aportaciones a congresos. En concreto, se exige el depósito de artículos en repositorios institucionales para facilitar su difusión, así como la disponibilidad del código como software libre (bajo la definición de la Open Source Initiative). Esta demanda, largamente solicitada por la comunidad no está exenta de dificultades y ambigüedades en su aplicación.<br><br>
También, la situación a nivel tecnológico en las universidades está centrada en dos aspectos. El primero es la situación post-pandemia: se ha ampliado enormemente la actividad online. Lamentablemente, en muchos casos ha sido a costa de realizar determinados acuerdos con empresas que proporcionan servicios "en la nube" con unos acuerdos no todo lo claros que debiesen respecto a la privacidad y soberanía de datos. Y en segundo lugar la masiva adopción de Inteligencias Artificiales generativas (basadas en modelos grandes de lenguaje) está generando muchas dudas tanto éticas como de privacidad.<br><br>
Así pues, se plantea esta dev-room como un espacio para reflexionar sobre los tres ejes de la actividad universitaria: docencia, investigación y gestión tanto en el pasado como el presente y el futuro.<br><br>
Nuestra intención es hacer una llamada a todas las universidades españolas para que puedan participar en presencial o remoto y sea el punto de partida de una comunidad más formal que pueda colaborar en los retos que se presentan.

-   Propuestas de la sala (sábado 25 de mayo de 12:00 a 14:00):

    - Presentación por los organizadores (5-10 min.)
    - Breve presentación de cada participante y la situación en la Uni de la que viene (20 min en total, ampliables si hubiera mucha asistencia)
    - Discusión sobre los demás temas de interés: 5 min. de presentación por alguien que se encargue de presentar el tema, 10 min. de debate (más o menos)
        - Instituciones referentes a software libre y conocimiento abierto en las universidades españolas
        - El movimiento del software libre en España y su contexto
        - Software libre / conocimiento abierto en la docencia: una nueva era marcada por el software como servicio en la nube (SaaS)
        - Software libre / conocimiento abierto en la investigación: sexenios y acreditación del profesorado
        - Software libre / conocimiento abierto en la gestión universitaria: una mirada hacia dentro
        - Software libre / conocimiento abierto y Oficinas de Software Libre (OSPOS) empresariales
    - Conclusiones: listado final de acciones (5-10 min), parcialmente recogidas durante la discusión de los demás temas<br><br>

-   Público objetivo:

>Comunidad universitaria española: estudiantes, profesores/investigadores (PDI) y  Personal Técnico, de Gestión y de Administración y Servicios (PTGAS)

## Comunidad que propone la sala:

-   Nombre: Oficinas de Software Libre y de Conocimiento Abierto de Universidad españolas

-   Info: 

>Aunque existe una "Comisión Sectorial de Digitalización de Crue Universidades Españolas" (CRUE-TIC) no hay un grupo de trabajo específico de software libre / conocimiento abierto.<br><br>
Queremos que este encuentro ponga las bases del desarrollo de una comunidad universitaria estable que pueda ganar consistencia en el futuro próximo

-   Web: <https://tic.crue.org/>

## Comentarios

>El objetivo de este dev-room establecer una comunidad de software libre a nivel nacional que tenga una continuación en el tiempo y se puedan establecer sinergias.<br><br>
Actualmente no nos podemos considerar una comunidad como tal, y creemos que este debe ser el camino.

## Condiciones aceptadas

-   [x]  Aceptamos seguir el código de conducta (<https://eslib.re/conducta>) durante nuestra participación en el congreso

