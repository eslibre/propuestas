---
layout: 2024/post
section: proposals
category: devrooms
author: KDE España
title: Akademy-es 2024
redirect_to: https://www.kde-espana.org/programa-akademy-es-2024
---

# Akademy-es 2024 "esLibre" Edition

## Detalles de la propuesta:

>Akademy-es (#akademyes, que es la etiqueta para las redes sociales) es evento más importante para los desarrolladores y simpatizantes de KDE en España, que se ha ido celebrando desde el 2006 con éxito creciente en diferentes ciudades de España: Málaga, Bilbao, A Coruña, Almeria, Barcelona, Madrid, Zaragoza, Valencia, etc.<br><br>
En general, las Akademy-es son el lugar adecuado para conocer a los desarrolladores, diseñadores, traductores, usuarios y empresas que mueven este gran proyecto.<br><br>
En ellas se realizan ponencias, se presentan programas, se explica el desarrollo del Software KDE (en campos tan diversos como el de los programadores, traductores, diseñadores, promotores, etc.) pero sobre todo se conoce a gente muy interesante y se cargan baterías para el futuro.

-   Formato:

>Utilizaremos la charla para realizar múltiples actividades, principalmente:
- Charlas de 45 minutos
- Charlas relámpago (lightning talk) de 10 minutos
- Reuniones
   
-   Público objetivo:

>Desde desarrolladores hasta usuarios básicos del entorno de trabajo.

## Comunidad que propone la sala:

-   Nombre: KDE España

-   Info:

>KDE España está formado por un grupo de desarrolladores y contribuidores al proyecto KDE con la intención de dinamizar el desarrollo y uso del entorno de escritorio KDE.<br><br>
Entre ellos podemos encontrar a gran variedad de colaboradores de KDE, incluyendo programadores, empaquetadores, documentadores, promotores, traductores, artistas, etc.<br><br>
En la actualidad lo componen unos 40 socios.

-   Web: <https://www.kde-espana.org/>
-   Mastodon (u otras redes sociales libres): <https://floss.social/@kde_espana>
-   Twitter: <https://twitter.com/KDE_Espana>
-   GitLab (u otra forja) o portfolio general: <https://invent.kde.org>

## Condiciones aceptadas

-   [x]  Aceptamos seguir el código de conducta (<https://eslib.re/conducta>) durante nuestra participación en el congreso

