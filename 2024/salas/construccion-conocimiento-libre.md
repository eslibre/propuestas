---
layout: 2024/post
section: proposals
category: devrooms
author: Wikimedia España
title: Construcción del conocimiento libre
---

# Construcción del conocimiento libre

## Detalles de la propuesta:

>La sala busca mostrar herramientas y recursos que contribuyen al desarrollo del conocimiento libre, con el fin de reforzar competencias en la materia y atraer nuevos perfiles a la comunidad de Wikimedia.

-   Propuestas de la sala (viernes 24 de mayo):

    - 16:00 - 16:45: Pablo Aragón - Investigación al servicio de lxs desarrolladorxs de Wikimedia
    - 16:45 - 17:30: Pau Giner - Herramientas para traducir Wikipedia
    - 17:45 - 18:30: Modesto Escobar y Ángel F. Zazo Rodríguez - Uso y elaboración de galerías gráficas
    - 18:30 - 20:00: Mini-hackathon 
    - 20:00 - 21:30: Paseo fotógrafico por la zona<br><br>

-   Público objetivo:

>Personas que se dedican a la investigación académica, agentes e instituciones que promueven el conocimiento, participantes en la comunidad del software libre, wikimedistas y todas las personas que tengan interés en los temas a tratar.

## Comunidad que propone la sala:

-   Nombre: Wikimedia España

-   Info:

>Somos el capítulo reconocido por la Fundación Wikimedia en España, una asociación sin ánimo de lucro que promueve el conocimiento libre y los proyectos Wikimedia, siendo Wikipedia el más conocido. Nos constituimos en 2011 y llevamos todos estos años trabajando para mejorar la accesibilidad, el uso y la participación en la enciclopedia libre y sus proyectos hermanos, tanto a nivel social como institucional.

-   Web: <https://wikimedia.es/>
-   Twitter: <https://twitter.com/@wikimedia_es>

## Condiciones aceptadas

-   [x]  Aceptamos seguir el código de conducta (<https://eslib.re/conducta>) durante nuestra participación en el congreso

