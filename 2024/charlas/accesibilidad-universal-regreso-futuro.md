---
layout: 2024/post
section: proposals
category: talks
author: Víctor Hernández
title: La accesibilidad universal en Regreso al Futuro
---

# La accesibilidad universal en Regreso al Futuro

>¿La accesibilidad universal en los entornos gráficos es un estorbo? Esto imposibilita la presencia de GNU/Linux en muchos ámbitos. Esta carencia en los escritorios libres donde están presentes excluyen a personas en el ámbito educativo, laboral, entre otros.
Pequeños cambios fáciles que darían a los escritorios retos a afrontar. El diseño accesible es la clave para emocionar y favorecer la adopción entornos GNU/Linux en un público general.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla larga / Presencial
-   Idioma: Español

-   Descripción:

>La importancia de la accesibilidad para la toda la sociedad. Exponer situaciones cotidianas donde la accesibilidad universal nos rodea sin ser conscientes. Ejemplos donde la accesibilidad apartar valor: tutoriales sobre escritorios y aplicaciones, sostenibilidad y otros. Características de accesibilidad que sin saberlo utilizamos en los escritorio con otros objetivos. Características que no estaban pensadas para la accesibilidad pero ayudan. Curiosidades de los ratones en los escritorios. Menú de ventana. Algunos atajos para el escritorio<br><br>
La accesibilidad es en realidad personalización, experiencia de producto, valor añadido más allá de las dificultades de la baja visión y otras problemáticas.
Una mejor perspectiva para los escritorios actuales. Preguntas e impresiones de la asistencia.

-   Público objetivo:

>Desarrolladores de interfaz en escritorio o parte de este. Asequible para todos los públicos,

## Ponente:

-   Nombre: Víctor Hernández

-   Bio: 

>Persona con baja visión. Mi primera distribución GNU/Linux Marzo del 2007 con Ubuntu 6.10. Mi vida laboral no tiene relación con el software. Conozco en gran medida GNOME 2.x y XFCE y medianamente LXDE, MATE, Cinnamon, KDE, GNOME 3, Compiz Fusion y quizá algún otro. He tratado el tema de la accesibilidad visual participado en eventos:
- Hablemos en GNU/Linux Valencia, 23 Noviembre 2017.
- Akdemy-es en Málaga en 2023. Accesibilidad, personas con baja visión en KDE Neón.<br><br>
Actualmente, participo en el proyecto podcast de Accesiblidad con Tecnologías Libres, de Jorge Lama, donde expongo mi experiencia en diferentes escritorios. 

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso

