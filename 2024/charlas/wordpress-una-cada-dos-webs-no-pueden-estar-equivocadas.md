---
layout: 2024/post
section: proposals
category: talks
author: David Martinez
title: WordPress&#58; una de cada dos webs no pueden estar equivocadas
---

# WordPress: una de cada dos webs no pueden estar equivocadas

>WordPress es un CMS libre y gratuito. Hay estadísticas poco fiables que dicen que el 90% de internet usa PHP, y otras más fiables, que dicen que casi la mitad de las web usan WordPress. Veremos las ventajas e inconvenientes (que no todo es maravilloso) del desarrollo con este popular CMS

## Detalles de la propuesta:

-   Tipo de propuesta: Charla larga / Presencial
-   Idioma: Español

-   Descripción:

>"Si haces Wordpress no eres informático de verdad"<br><br>
Lo he escuchado cientos de veces. Especialmente en estos últimos años. No sé si tienen razón, pero a veces he llegado a creérmelo (y eso que se supone que yo soy un informático pata negra, con ingeniería y todo).<br><br>
Entiendo perfectamente a los que reniegan de Wordpress... ellos solamente han visto barbaridades con Wordpress.<br><br>
Pero también hay páginas buenas, que pueden hacer cosas impensables sin el soporte de un CMS como Wordpress, así que veremos unas cuantas posibilidades de  funcionalidades increíbles desarrolladas con plugins y temas WordPress.

-   Público objetivo:

>Desarrolladores interesados en tecnologías Web y pequeños empresarios o autónomos que valoran usar WordPress para su web

## Ponente:

-   Nombre: David Martinez

-   Bio:

>Uso WordPress a nivel personal desde el año 2005 y a nivel profesional desde el año 2015. Actualmente soy el organizador del grupo de MeetUp en Ponferrada.<br><br>
Empecé mi trayectoria en mundo de las grandes consultoras, cuando era jefe de proyecto y scrum master, vi que me estaba alejando demasiado del código y empecé a realizar pequeños proyectos como freelance.<br><br>
Unos años más más tarde, he empezado a trabajar exclusivamente por mi cuenta, llevando el aprendizaje de las grandes empresas del país (y algunas internacionales) a pequeñas y medianas empresas.

### Info personal:

-   Web personal: <https://martinezmartinez.com>
-   Twitter: <https://twitter.com/_Inforrada>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso

