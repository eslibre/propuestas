---
layout: 2024/post
section: proposals
category: talks
author: Ivan Vilata i Balaguer
title: gwit&#58; una web resilient de repos Git
---

# gwit Una Web resilient de repos Git

>La càrrega d’administració de sistemes (DNS, certificats, allotjament, censura, atacs DoS) deixa fora de línia llocs web independents cada dia, o fa que autors potencials es decanten per no començar‐los. Alhora, els projectes de Web Descentralitzada són massa complexos per a prosperar en la «smolnet» i entorns de permacomputació (dedicats a tecnologies més humanes, duradores i respectuoses amb la privadesa, com el protocol Gemini). Git (el sistema distribuït de control de versions més popular) cobreix necessitats similars, però sol emprar forges centralitzades com GitHub per a allotjar repos i enllaçar‐los.<br><br>
gwit afegeix hiperenllaços i allotjament múltiple arbitrari a Git de forma minimalista, creant una Web de llocs replicats, verificables i duradors que es presta a la publicació autònoma i amb requeriments mínims per a la smolnet. Tot açò només emprant Git amb commits signats, més un sistema social de presentacions, i URIs pròpies.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla larga / Presencial
-   Idioma: Valencià

-   Descripción:

>gwit s’inspira en l’article «Low budget P2P content distribution with git» de Solderpunk, el creador del protocol Gemini. Igual que altres projectes de Web Descentralitzada/Web3 com IPFS, Dat/Hypercore, ZernoNet etc., gwit pretén recuperar la publicació autònoma de les mans de corporacions centralitzadores, però es centra en contingut estàtic lleuger (sobre tot textual) amb un enfocament minimalista, i prova d’explotar la naturalesa distribuïda de Git al màxim per a publicar contingut (tant de llocs com de repos de codi existents). Així evita solucions ad hoc complexes i intensives en recursos (adoptant en canvi una base madura i duradora), i resulta suficientment simple per a ser entés i implementat per una sola persona en un temps raonable.<br><br>
Un lloc gwit no és més que una branca amb commits signats a un repo Git, però açò dóna per a permetre replicar el lloc en diferents ubicacions sense permís de qui l’ha creat. Com que visitar un lloc implica descarregar‐ne tot el contingut i la seua història, gwit es converteix en una Wayback Machine automàtica, confiable i replicada que pot mantenir vius els llocs durant molt de temps. A banda, la navegació, cerca, etc. en llocs ja visitats es pot fer fora de línia de forma privada.<br><br>
La distribució de llocs pot ocórrer sobre qualsevol sistema acceptat per Git, cosa que possibilita la publicació oportunista en forges web, servidors HTTP estàtics o fins i tot dispositius fora de línia com discs USB, permetent l’accés confiable amb connexió limitada o censura estricta. Un sistema simple de presentació de llocs basat en efectes de xarxa menuda («small-world») permet trobar com accedir a nous llocs sense sistemes de directori centralitzats com DNS.<br><br>
Ara mateix gwit només és una especificació, però ha despertat cert interés a la «smolnet».  En aquesta xerrada presentaré gwit, quins problemes prova de resoldre i com ho fa per a mantenir un enfocament minimalista. Tan important (o més) que la presentació serà el debat posterior, on m’agradaria contrastar i debatre les idees presentades amb qui hi tinga interés.

-   Web del proyecto: <https://gwit.site/>

-   Público objetivo: Persones interessades en la publicació autònoma de contingut textual lleuger (diaris, notícies, obres de referència), tant si han tingut com no exposició a sistemes de la «smolnet» com els protocols Gemini o Gopher, i especialment si no tenen experiència en l’administració de sistemes o la publicació Web (o sí la tenen, però ja n’han tingut prou).<br><br>
També persones interessades en sistemes distribuïts o descentralitzats de publicació, o que tinguen experiència en l’elaboració o revisió d’estàndards tècnics, així com el desenvolupament de programes per a la publicació, intercanvi o lectura de contingut textual.

## Ponente:

-   Nombre: Ivan Vilata i Balaguer

-   Bio:

>He treballat amb, traduït, gestionat i desenvolupat Programari Lliure des de finals dels ’90. A banda d’administrar sistemes, he desenvolupat i dissenyat programari per a la gestió massiva de dades (PyTables, Blosc), per a experiments sobre xarxes comunitàries (CONFINE), reutilització de maquinari (eReuse), i eines cooperatives resistents a censura per a compartir contingut web sobre xarxes P2P (Ouinet i CENO Browser).<br><br>
L’experiència en computació distribuïda i la Web d’aquestes últimes, més la meua proximitat amb les xarxes comunitàries (com guifi.net) i la inspiració del protocol Gemini, la «smolnet» i la permacomputació m’han portat a proposar gwit com a eina lleugera i sostenible per a publicar contingut al si d’una comunitat.

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso

