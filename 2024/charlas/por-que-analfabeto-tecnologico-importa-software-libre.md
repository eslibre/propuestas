---
layout: 2024/post
section: proposals
category: talks
author: Ana Galán
title: Por qué soy un analfabeto tecnológico pero me importa el Software Libre 
---

# Por qué soy un analfabeto tecnológico pero me importa el Software Libre 

>En esta charla veremos la importancia de saber explicar la importancia del software libre a gente que no tenga ningún conocimiento técnico, usando para ello mi experiencia personal.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla corta / Presencial
-   Idioma: Español

-   Descripción:

>¿Por qué una persona con formación en ciencias sociales y humanidades, con escasos conocimientos técnicos, se interesa por el Software Libre? En esta charla, a través de la experiencia personal, veremos cómo explicar el Software libre al resto de la sociedad

## Ponente:

-   Nombre: Ana Galán

-   Bio:

>FSFE Communications manager

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso

