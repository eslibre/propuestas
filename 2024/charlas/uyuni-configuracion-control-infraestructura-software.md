---
layout: 2024/post
section: proposals
category: talks
author: Raúl Osuna Sánchez-Infante
title: Uyuni&#58; la solución de configuración y control de infraestructura open-source para la infrastructura definida por software
---

# Uyuni&#58; la solución de configuración y control de infraestructura open-source para la infrastructura definida por software

>Uyuni es una herramienta de configuración y control de infraestructura que ahorra tiempo, costes y dolores de cabeza administrando y actualizando máquinas, en la escala de hasta decenas de miles.<br><br>
Su principal foco trata sobre habilitar el despliegue automatizado de parches y paquetes, basándose en canales de software y repositorios que pueden asignarse a los sistemas. Sin embargo, ¡es capaz de mucho más!

## Detalles de la propuesta:

-   Tipo de propuesta: Charla larga / Presencial
-   Idioma: Español

-   Descripción:

> Aparte del despliegue de parches y paquetes mencionado en el resumen, Uyuni hace más fácil integrar y administrar cualquier servidor GNU/Linux conectado a la red local (no sólo openSUSE o de la familia de SUSE Linux Enterprise, sino también casi cualquier otra distribución, con ejemplos como CentOS, Rocky Linux, Alma Linux, Ubuntu, Debian, Red Hat Enterprise Linux, Raspberry Pi OS...), desde dispositivos "IoT edge" hasta entornos de Kubernetes, sin importar dónde esté alojado (datacenter privado o de un tercero, o incluso en la nube pública).<br><br>
Uyuni habilita una funcionalidad conocida como CLM (Channel Lifecycle Management), que permite congelar el contenido de los repositorios a los administradores, por ejemplo filtrando por una fecha concreta (aunque existen otros posibles filtros, ¡y no sólo relacionados con la fecha!)<br><br>
Uyuni es una única herramienta para el despliegue de plantillas de sistemas operativos reforzados (sistemas físicos/"bare metal", máquinas virtuales o contenedores) a escala masiva de servidores y dispositivos IoT, para un abastecimiento rápido, consistente e iterable y una configuración sin comprometer velocidad y seguridad.<br><br>
La funcionalidad conocida como "auditoría CVE" permite comprobar el estado de parches de seguridad públicos, y con la ayuda de OpenSCAP, también es posible comprobar la conformidad con ciertas especificaciones y aplicar soluciones para cumplir los requisitos, directamente desde Uyuni.<br><br>
Uyuni utiliza Salt por debajo para la mayoría de las tareas, tales como el control de configuración o paquetes y parches.

-   Web del proyecto:
    + <https://www.uyuni-project.org/>
    + <https://github.com/uyuni-project>
    + <https://github.com/uyuni-project/uyuni-docs><br><br>

-   Público objetivo:

>Administradores de sistemas con un nivel intermedio (una conocimiento básico de administración de sistemas GNU/Linux para situar el contexto en el que puede ser útil la herramienta).

## Ponente:

-   Nombre: Raúl Osuna Sánchez-Infante

-   Bio:

>- Ingeniero de Telecomunicación por la Escuela Técnica Superior de Ingeniería de Bilbao, 2006.
- Graduado en Física por la UNED, 2021.
- Trabajando para SUSE desde 2013, tanto físicamente en la oficina de Núremberg (Alemania, 2013-2018), como remotamente desde el 2018 desde mi localidad, Vitoria-Gasteiz.
- Ingeniero de Soporte Técnico en el departamento de Soporte al Cliente, principalmente dedicado a SUSE Manager (la versión empresarial de Uyuni), de 2013 a 2023.
- Desde septiembre de 2023, Ingeniero de Distribución Software para Uyuni y SUSE Manager, dentro del departamento de Tecnología y Producto de SUSE.

>He presentado sesiones sobre SUSE en eventos internos con clientes, como la SUSECon (he acudido a Orlando 2014, Amsterdam 2015, Washington DC 2016, Praga 2017, Nashville 2019, presentando sesiones en todas excepto en la primera, además de la edición virtual de 2023. Algunas de estas sesiones se pueden encontrar todavía por Youtube).<br><br>
>Apasionado por el Software Libre, he usado distribuciones como Debian, Ubuntu, Fedora, Gentoo... y desde 2013, principalmente toda la familia de SUSE. En todos mis sistemas personales, a día de hoy, no uso otra cosa que no sea openSUSE.<br><br>
>Además soy jugador de ajedrez y "powerlifter", ambas cosas a nivel amateur.

### Info personal:

-   Twitter: <https://twitter.com/@UyuniProject>
-   GitLab (u otra forja) o portfolio general: <https://github.com/uyuni-project/uyuni>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso

