---
layout: 2024/post
section: proposals
category: talks
author: Andrés Valero
title: Gestionando Linux en edge con y sin Kubernetes
---

# Gestionando Linux en edge con y sin Kubernetes

>Exploraremos estrategias de gestión de infraestructuras Linux en entornos de Edge, desde la administración de Linux independiente hasta la integración eficiente con Kubernetes. Descubre soluciones prácticas y accesibles para optimizar operaciones en arquitecturas altamente distribuidas.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla larga / Presencial
-   Idioma: Español

-   Descripción:

> En esta  charla, nos sumergiremos en el mundo de la gestión de infraestructuras en entornos Edge, abordando estrategias que abarcan desde la administración independiente de Linux hasta la integración eficiente con Kubernetes.<br><br>
Comenzaremos explorando el proyecto Uyuni, una herramienta potente y versátil que permite la gestión centralizada de sistemas Linux. Descubriremos cómo Uyuni facilita el monitoreo, la actualización y la configuración de múltiples nodos, proporcionando una base sólida para entornos distribuidos en el borde de la red. Veremos casos prácticos y escenarios donde Uyuni brilla al ofrecer una administración robusta y segura sin depender de Kubernetes.<br><br>
Luego, nos sumergiremos en el fascinante universo de Kubernetes, la orquestación de contenedores que ha revolucionado la forma en que desplegamos y gestionamos aplicaciones. Exploraremos dos proyectos ligeros y eficientes: Elemental y K3S. Estos proyectos están diseñados para simplificar la implementación y administración de clústeres Kubernetes en entornos con recursos limitados, como los sistemas en el borde de la red.<br><br>
Elemental, con su enfoque minimalista, busca facilitar la gestión de clústeres Kubernetes sin sacrificar la funcionalidad. Descubriremos cómo este proyecto aporta agilidad y simplicidad a la administración de aplicaciones en el borde.<br><br>
Por otro lado, exploraremos K3S, una distribución liviana de Kubernetes que conserva la potencia y versatilidad del orquestador de contenedores, pero está diseñada específicamente para escenarios donde los recursos son escasos. Veremos cómo K3S simplifica la implementación y gestión de clústeres Kubernetes, brindando una solución eficiente y optimizada para entornos en el borde de la red.<br><br>
A lo largo de la charla, analizaremos casos prácticos, estrategias de implementación y las ventajas específicas que ofrecen estos proyectos en distintos contextos. Desde pequeños entornos hasta soluciones con 100.000 nodos distribuidos, exploraremos cómo estas herramientas pueden adaptarse y prosperar en diversas situaciones del mundo real.

-   Web del proyecto:
    + <https://www.uyuni-project.org/>
    + <https://elemental.docs.rancher.com/>
    + <https://k3s.io/><br><br>

-   Público objetivo:

>Gestores e Ingenieros de Infrastructuras. Responsables de sistemas. Estudiantes de Ingeniería Informática buscando proyectos para aprender y colaborar

## Ponente:

-   Nombre: Andres Valero

-   Bio:

> Andrés siempre ha tenido una pasión profunda por el software de código abierto. A pesar de esto, no decidió convertir su afición en una carrera profesional hasta hace relativamente poco. Antes de empezar en el campo de las tecnologías de la información, Andrés dedicó muchos años a trabajar como vendedor. Eventualmente decidió cambiar de rumbo profesional y se formó como consultor en la industria. Desde allí, avanzó a una posición de Arquitecto de Soluciones, donde se especializó en gestión y tecnologías nativas de la nube. Actualmente, Andrés trabaja en SUSE cómo Technical Marketing Manager para tecnologías de edge y cloud-native. Además de su labor en SUSE, Andrés ha participado en numerosos eventos tecnológicos alrededor del mundo, incluyendo el Red Hat Summit, los Kubernetes Community Days en España, y el Open Infrastructure Summit 2019 en Shanghái.

### Info personal:

-   Web personal: <https://www.linkedin.com/in/miguelpc/>
-   Mastodon (u otras redes sociales libres): <https://tty0.social/@mmmmmmpc>
-   Twitter: <https://twitter.com/mmmmmmpc>
-   GitLab (u otra forja) o portfolio general: <https://github.com/mmmmmmpc>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso

