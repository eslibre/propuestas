---
layout: 2024/post
section: proposals
category: talks
author: Albert Astals Cid
title: Seguridad en documentos PDF&#58; cómo evitar la manipulación y garantizar la autenticidad con firmas digitales y software libre
---

# Seguridad en documentos PDF: cómo evitar la manipulación y garantizar la autenticidad con firmas digitales y software libre

>Muchas personas consideran un documento PDF algo con cierto valor de oficialidad, en esta charla veremos lo relativamente fáciles que son de editar y por que deberíamos intentar siempre usar firmas digitales si el contenido de es "importante".

## Detalles de la propuesta:

-   Tipo de propuesta: Charla larga / Presencial
-   Idioma: Español

-   Descripción:

>Editar PDF es "fácil", por ejemplo, podemos cambiar el precio en un presupuesto que nos envíen. En esta charla haremos un ejemplo rápido de como se puede hacer esas cosas y daremos herramientas para asegurarnos que los PDF importantes que nosotros creamos no puedan ser modificados y que en caso de serlo se pueda identificar de forma relativamente fácil.

-   Público objetivo:

>Personas interesadas en la seguridad digital, especialmente aquellas que trabajan con documentos PDF en su día a día.

## Ponente:

-   Nombre: Albert Astals Cid

-   Bio:

>Albert ha estado involucrado en la mejora de herramientas libres sobre PDF durante casi dos décadas.

### Info personal:

-   Mastodon (u otras redes sociales libres): <http://fosstodon.org/tsdgeos>
-   Twitter: <https://twitter.com/tsdgeos>
-   GitLab (u otra forja) o portfolio general: <https://invent.kde.org/aacid>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso

