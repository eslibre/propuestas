---
layout: 2024/post
section: proposals
category: talks
author: Alexander Soto
title: Construyendo un ecosistema de dispositivos e-ink de hardware libre
---

# Construyendo un ecosistema de dispositivos e-ink de hardware libre

>Modos es una compañía de hardware y software libre que esta construyendo un ecosistema de dispositivos E Ink con un enfoque en la creación de tecnología tranquila, inclusiva y humana.<br><br>
Esta charla discutirá brevemente nuestra tecnología, presentará los hallazgos de nuestra encuesta comunitaria, los desafíos que enfrentamos al desarrollar nuestro controlador de pantalla y chasis, y las consideraciones de diseño en la creación de aplicaciones para pantallas de tinta electrónica. También describiremos nuestros próximos pasos, la dirección futura y cómo pueden participar.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla corta / Presencial
-   Idioma: Español

-   Descripción:

> Nuestra charla comenzará hablando de Modos, quiénes somos y por qué estamos construyendo un ecosistema de dispositivos de tinta electrónica de hardware libre. Luego, brevemente una descripción general de cómo funciona la tinta electrónica, exploraremos sus ventajas y desventajas y por qué es una opción para los dispositivos informáticos personales.<br><br>
A continuación, compartiremos información de nuestra encuesta comunitaria, que reunió más de 2,700 respuestas. Discutiremos los comentarios recibidos y revelaremos por qué las personas están interesadas en usar E Ink, cómo imaginan integrarla en su vida diaria y qué tipos de dispositivos les interesan más.<br><br>
También cubriremos los desafíos que enfrentamos durante el desarrollo de nuestro controlador de pantalla y los aspectos de diseño de nuestro chasis. Además, examinaremos cómo las propiedades de las pantallas E Ink influyen en el desarrollo de aplicaciones. Nos centraremos en diseñar estrategias que optimicen la experiencia del usuario.<br><br>
Finalmente, los invitamos a visitar nuestra mesa de exposición. Aquí podrá utilizar nuestros prototipos de dispositivos E Ink.<br><br>
Esta charla tiene como objetivo compartir una visión general de nuestro trabajo en Modos. Estamos emocionados de compartir nuestro viaje, destacando nuestros logros y desafíos.<br>
- Demostración del monitor de papel: <https://www.youtube.com/watch?v=Ds38T8wVuDg>
- Demostración del Chasis de monitor de papel: <https://www.youtube.com/watch?v=20VBjSgXlYE>
- Chasis de monitor de papel: <https://cloud.modos.tech/s/wM9yPPozTkEoDeC>
- Github Modos Labs: <https://github.com/Modos-Labs>
- NLnet; Caster: <https://nlnet.nl/project/Modos/>

-   Web del proyecto: <https://www.modos.tech/>

-   Público objetivo:

>Personas interesadas en hardware/software libre, E Ink, accesibilidad, tecnología asistiva, DIY.

## Ponente:

-   Nombre: Alexander Soto

-   Bio:

> Alexander Soto es un organizador comunitario, educador, ingeniero de software y fundador de Modos. Sus intereses incluyen la construcción de comunidades, la justicia social, la educación y el uso de la tecnología para abordar problemas sociales.

### Info personal:

-   Mastodon (u otras redes sociales libres): <https://fosstodon.org/@modos>
-   Twitter: <https://twitter.com/Modostech>
-   GitLab (u otra forja) o portfolio general: <https://github.com/Modos-Labs/>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso

