---
layout: 2024/post
section: proposals
category: talks
author: Francesc Fort, Carmen Bachiller
title: Proyecto GLAM TelecomVLC&#58; museo de las telecomunicaciones UPV
---

# Proyecto GLAM TelecomVLC: museo de las telecomunicaciones UPV

>En esta sesión se mostrarán los resultados del trabajo del Museo de la Telecomunicación "Vicente Miralles Segarra", vinculado a la Escuela Técnica Superior de Ingenieros de Telecomunicación de la UPV, en la digitalización de pistas de audio provinientes de cilindros de cera, y su posterior liberación con licencias Creative Commons mediante las plataforma Wikimedia.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla corta / Presencial
-   Idioma: Español

-   Descripción:

>Esta obra presenta una colección de grabaciones en cilindros de cera de música popular y folclórica española del siglo XX. El fonógrafo (desarrollado en 1870) fue el primer dispositivo utilizado para grabar y reproducir audio. El soporte utilizado eran cilindros de cera, donde posteriormente se podía reproducir una grabación mecánica del sonido en surcos. La cera es un material que se degrada con mucha facilidad, y con cada reproducción la grabación se destruye, por lo que, aunque fueron muy populares, pocos discos han sobrevivido hasta nuestros días.<br><br>
El Museo de Telecomunicaciones de la Universidad Politécnica de Valencia (UPV) recibió en 2016 una colección de 61 grabaciones en cilindros de cera junto con un fonógrafo, siempre que fueran conservadas digitalmente. El proyecto fue abordado de forma multidisciplinaria, por especialistas en conservación y telecomunicaciones. Los objetivos fueron la restauración del fonógrafo, la conservación preventiva y curativa de la colección de cilindros de cera, la digitalización y restauración de registros sonoros, la catalogación y difusión de la colección y la exposición en el Museo.<br><br>
El trabajo se enmarca en el contexto de la preservación del patrimonio tecnológico, pero también, e igualmente relevante, intangible. Los registros sonoros son una fuente muy importante para conocer la cultura intangible de una comunidad y han sido, desde su aparición, una forma de preservarla. Las grabaciones contienen piezas de música popular y folclórica española del siglo XX. Los géneros tratados son muy variados: flamenco, zarzuela, coplas, música de banda, canciones infantiles, etc. grabadas por cantaores profesionales (como los cantaores flamencos El Mochuelo y La Rubia) y aficionados. El estado de conservación de los cilindros era muy malo en algunos casos, por lo que la calidad de los discos musicales rescatados, incluso después de un proceso de restauración digital, no es óptima. Sin embargo, la mera existencia de dichas grabaciones y el hecho de que se hayan conservado hasta el día de hoy es casi un milagro.<br><br>
El año 2023, dicha colección fue subida y catalogada en Wikimedia Commons, dada la naturaleza de las pistas de audio originales, que se encuentran en Dominio Público.

-   Web del proyecto: <https://ca.wikipedia.org/wiki/Museu_de_la_Telecomunicaci%C3%B3_Vicente_Miralles_Segarra>

-   Público objetivo:

>De interés para el público general. Dirigido a gente interesada en las licencias libres, el GLAM, y la historia de la telecomunicación, particularmente el audiovisual.

## Ponente:

-   Nombre: Francesc Fort

-   Bio: 

>Wikimedista

-   Nombre: Carmen Bachiller

-   Bio:

>Responsable del Museo

### Info personal:

-   Web personal: <http://cultura.upv.es/colecciones/content/museotelecomunicacion/cas/index.html>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso

