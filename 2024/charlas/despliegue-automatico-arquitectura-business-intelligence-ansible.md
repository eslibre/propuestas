---
layout: 2024/post
section: proposals
category: talks
author: Jordi Isidro Llobet
title: Despliegue automático de una arquitectura business intelligence libre con Ansible
---

# Despliegue automático de una arquitectura business intelligence libre con Ansible

>En un mundo lleno de nubes y bigdata hay una gran cantidad de empresas y organizaciones perqueñas y medianas que también tienen que tomar decisiones en su día a día, pero no pueden permitirse los costes de las nubes o no quieren que sus datos y los de sus contactos queden expuestos o desconozcan qué se hace con ellos.<br><br>
En esta sesión presentaremos una arquitectura para sistemas de toma de decisiones basada en herramientas libres.<br><br>
A demás, podremos desplegar esta arquitectura de forma fácil en distintos servidores y adaptarla a nuestras necesidades mediante Ansible.


## Detalles de la propuesta:

-   Tipo de propuesta: Charla larga / Presencial
-   Idioma: Español

-   Descripción:

> Cuando se habla de sistemas decisionales o Business Intelligence, últimamente siempre lo vemos relacionado con Big Data, tiempo real, y, casi siempre, asociado a las grandes nubes. Y también con altos costes económicos. 
Hay una gran cantidad de empresas que, por su medida o cantidad de datos que manejan, no necesitan sistemas tan complejos y caros, pero también tienen que tomar decisiones en su día a día.<br><br>
Afortunadamente, también cada vez hay mas empresas compromentidas con la privacidad de los datos, y desde la Economía Social y Solidaria, también impulsando el software libre.<br><br>
En esta charla vamos a proponer una arquitectura de Business Intelligence que sea escalable para poderse adaptar a los distintos tipos de organizaciones y también se adapte a la evolución de estas organizaciones. Todos los componentes usados seran de software libre, por que no necesitamos herramientas privativas para analizar nuestros datos.<br><br>
Esta arquitectura la vamos a desplegar con Ansible. Ansible es una herramienta que nos permite automatizar la instalación y configuración de todos los componentes necesarios en un servidor, ayudando así a su replicabilidad.<br><br>
Ansible nos permitirá evolucionar la arquitectura creada, si es necesario, y también replicarla en distintos servidores, ya sean diferentes entornos de una misma organización o para  organizaciones diferentes.<br><br>
Además nos permitirá también poder añadir otras herramientas a la arquitectura de forma fácil.<br><br>
Entre las herramientas que vamos a ver como instalar con ansible veremos:  PostgreSQL, DBT, Apache Airflow, Apache superset.

-   Web del proyecto: <https://gitlab.com/coopdevs/bi-provisioning/>

-   Público objetivo:

>Ingenieras/os de datos, amantes de las bases de datos, adminstradoras/es de sistemas, personas con curiosidad para entrar en el mundo de la toma de decisiones.

## Ponente:

-   Nombre: Jordi Isidro Llobet

-   Bio:

> Entusiasta del Business Intelligence y algo fanático de los datos. Soy ingeniero informático y desde que hice la primera asignatura donde enseñaban SQL decidí que me quería dedicar el resto de mi vida laboral a hacer queries y pelearme con bases de datos.<br><br>
Llevo casi 20 años en el mundo del Business Intelligence, normalmente trabajando para el mal, pero siempre con un ojo puesto en el software libre y la economia social. En 2021 fui rescatado por Coopdevs y ahora ya puedo trabajar en Business Intelligence para la economia social y solidaria en software libre, vamos, un sueño. En Coopdevs no solo me dedico a montar sistemas decisionales, sinó que también estoy en el equipo de administración de sistemas, ayudando a montar y mantener servidores.

### Info personal:

-   GitLab (u otra forja) o portfolio general:
    + <https://git.coopdevs.org/jordiisidro>
    + <https://github.com/jordiisidro>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso

