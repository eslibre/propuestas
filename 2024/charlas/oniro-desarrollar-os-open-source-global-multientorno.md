---
layout: 2024/post
section: proposals
category: talks
author: Juan Rico
title: Oniro&#58; cómo desarrollar un OS open source global multientorno e impulsar su adopción a través de la cooperación entre fundaciones FOSS
---

# Oniro: cómo desarrollar un OS open source global multientorno e impulsar su adopción a través de la cooperación entre fundaciones FOSS

>El objetivo de la charla es mostrar cómo a través del uso de tecnologías de código abierto y la colaboración entre Fundaciones globales es posible desarrollar un sistemas operativo open source que facilita la tarea de los desarrolladores de dispositivos y abre la posibilidad a los proveedores de servicio de participar en ecosistemas globales que garantizan la privacidad y el respeto a la propiedad intelectual como ejes de actuación.<br><br>
La charla cubrirá el alcance técnico del proyecto, mostrando ejemplos reales de dispositivos disponibles en el mercado que utilizan la tecnología base de OpenHarmony y el trabajo que desde Oniro se hace para adecuar la tecnología a los requerimientos de los mercados occidentales. Además, se provera de la información para sostener el valor de las fundaciones Open Source como Eclipse como garantes del desarrollo respetuoso siguiendo los principios de las cuatro libertades del open source.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla larga / Presencial
-   Idioma: Español

-   Descripción:

>La charla estará enfocada en primer lugar a presentar Oniro, una plataforma de sistema operativo open source que provee de una base tecnológica común a los desarrolladores de dispositivos. Esta aproximación tiene la ventaja de reducir drásticamente las dependencias y coplejidad en el desarrollo de productos multi-plataforma. Además del objetivo base, desde un punto de vista de negocio, se ha incluido un sistema de validación de las licencias de todos los componentes que son utilizados, tanto los desarrollados dentro del propio proyecto, como aquellas librerías de terceras partes cuyas dependencias deben ser analizadas para no incurrir en infringimiento de la propiedad intelectual. Todo esto desarrollado e implementado de forma nativa sobre un IDE propio.<br><br>
Oniro además utiliza como base de código OpenHarmony, sistema operativo desarrollado por la Open Atom Foundation y asumido como standard de facto para el desarrollo del concepto de smart-home dentro del mercado chino. Como segundo área de la charla se tratará el hecho de cómo la colaboración entre la Open Atom Foundation en China y la Eclipse Foundation en Europa permite el desarrollo global apto para ser adoptado por el mercado de soluciones open source.<br><br>
Como tercer punto de la charla se cubrirán los principales casos de uso que son cubiertos por Oniro, desde móviles hasta dispositivos IoT de diferentes capacidades. Se incluirán ejemplos prácticos y reales, así como las oportunidades que ofrecen a desarrolladores de dispositivos y aplicaciones.<br><br>
Para finalizar, se explicará cómo Oniro facilita el desarrollo de dispositivos y soluciones que cumplan con el CiberResilient Act (CRA) aprobado por la Comisión Europea.<br><br>

-   Web del proyecto: <https://oniroproject.org/>

-   Público objetivo:

>- Desarrolladores involucrados tanto en los campos de IoT como de mobile.
- Cualquier persona interesada en la participación en proyectos de código abierto.
- Personas que utilicen tecnologías de código abierto y que quieran entender el impacto de la gestión adecuada de la integración de contribuciones de terceras partes en sus proyectos.

## Ponente:

-   Nombre: Juan Rico

-   Bio:

>Juan Rico es ingeniero de Telecomunicación y Master en Tecnologías de la información y Comunicaciones en redes móviles por la Universidad de Cantabria. Su carrera se ha desarrollada muy vinculado al desarrollo de soluciones IoT y la explotación de los servicio asociados a ellos en campos tan diversos como aeronáutica, smart cities, agricultura, smart grids o smart homes. En los últimos años se ha especializado en el lanzamiento de soluciones disruptivas tecnológicas a los mercados formándose en Georgia Institute of Technology  y Harvard Business School. Actualmente es program manager en la fundación Eclipse promoviendo el uso de tecnologías de código abierto.

### Info personal:

-   Web personal: <https://www.linkedin.com/in/juanricofdez/>
-   Twitter: <https://twitter.com/j_rico_>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso

