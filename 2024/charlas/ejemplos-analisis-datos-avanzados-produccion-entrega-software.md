---
layout: 2024/post
section: proposals
category: talks
author: Agustín Benito Bethencourt
title: Ejemplos de uso de análisis de datos avanzados en procesos de producción y entrega de software
---

# Ejemplos de uso de análisis de datos avanzados en procesos de producción y entrega de software

>La charla promueve los beneficios en la utilización de análisis avanzados de datos para la mejora del rendimiento de procesos de producción de software a escala, a través de dos ejemplos como son el anaálisis de depndencias y la detección y estudio de causas de cuellos de botella en el flujo de código.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla larga / Presencial
-   Idioma: Español

-   Descripción:

>La mejora del rendimiento de los procesos de producción de productos complejos basados en software para reducir los tiempos de lanzamiento al mercado es uno de los principales retos para cualquier organización de cierto tamaño.<br><br>
El uso de análisis avanzados de datos como actividad complementaria para llevar a cabo mejoras en ese rendimiento  no está muy extendido pero está ganando en popularidad en diversos sectores. El análisis de datos ayuda a describir de principio a fin los procesos de producción de manera simple, a detectar la mayor parte de las fortalezas y delibilades del sistema de producciṕn de software, separando causas y síntomas, y sirve como base para el uso de modelos de riesgo, predictivos y otros con el fin de ayudar a la toma de decisiones de relevancia.<br><br>
La charla demostrará a los asistentes los beneficios de aplicar técnicas avanzadas de análisis de datos para el estudio del comportamiento de sistemas de producción de software con el fin de mejorar su rendimiento mediante dos ejemplos:<br><br>
- El análisis de dependencias de los diferentes elementos de estudio (código, equipos de desarrollo, defectos, etc.)
- Relación entre diferentes fuentes de datos para la detección de cuellos de botella, de sus síntomas y potenciales causas.<br><br>
Asimismo se relacionarán estos ejemplos con el fomento de buenas prácticas de desarrollo y producción de software.

-   Web del proyecto: <https://toscalix.com/consulting/delivery-performance-analytics/>

-   Público objetivo:

>La charla va dirigida a desarrolladores, lideres técnicos y gestores que promueven buenas prácticas relacionadas con principios ágiles así como entrega contínua y que tienen problemas para diseminar dichas prácticas a escala.

## Ponente:

-   Nombre: Agustín Benito Bethencourt

-   Bio:

>Agustín Benito Bethencourt descubrió el Software Libre durante su etapa universitaria, pero en 2003 decidió centrar su carrera como emprendedor en el Software de Código Abierto. Desde entonces, ha liderado equipos y proyectos en el diseño, desarrollo y soporte de productos basados en esta tecnología en sectores como automotriz, IoT, Edge, industrial y escritorio.<br><br>
Actualmente, trabaja como consultor independiente, centrado en Delivery Performance Analytics[1]. Es miembro activo de KDE y Software Heritage, de este último en calidad de Embajador. Defensor del trabajo remoto, reside gran parte del año entre Málaga y La Palma, Islas Canarias.<br><br>
[1] https://toscalix.com/consulting/delivery-performance-analytics/
<br><br>Para más información sobre toscalix, visita https://toscalix.com/about/

### Info personal:

-   Web personal: <http://www.toscalix.com>
-   Mastodon (u otras redes sociales libres): <https://mastodon.social/@toscalix>
-   Twitter: <https://twitter.com/toscalix>
-   GitLab (u otra forja) o portfolio general: <https://gitlab.com/toscalix>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso

