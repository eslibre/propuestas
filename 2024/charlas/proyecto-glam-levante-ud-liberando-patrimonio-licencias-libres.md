---
layout: 2024/post
section: proposals
category: talks
author: Francesc Fort, Emilio Nadal
title: Proyecto GLAM Levante UD&#58; liberando el patrimonio bajo licencias libres
---

# Proyecto GLAM Levante UD: liberando el patrimonio bajo licencias libres

>Desde 2023, el área de patrimonio del Levante Unión Deportiva viene colaborando con el Movimiento Wikimedia para liberar contenido, mayormente documental y fotográfico, en Wikimedia Commons.<br><br>
El objetivo es aprovechar las licencias libres para difundir la historia de una institución decana en el deporte valenciano, y pionera en el deporte femenino.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla corta / Presencial
-   Idioma: Valencià

-   Descripción:

> Explicaremos las fases desarrolladas en el proyecto GLAM, y mostraremos los resultados de las mismas, analizando su alcance, y qué representan para el club.


-   Web del proyecto:

-   Público objetivo:

>Personas aficionadas al futbol y gente interesada en las licencias libres por la parte del contenido.

## Ponente:

-   Nombre: Francesc Fort

-   Bio: 

>Wikimedista

-   Nombre: Emilio Nadal

-   Bio:

>Área de Patrimonio del Levante UD

### Info personal:

-   Web personal: <https://museo.levanteud.com/historia/>
-   Twitter: <https://twitter.com/LUDhistoria>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso

