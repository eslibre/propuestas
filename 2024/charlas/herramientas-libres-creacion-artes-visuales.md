---
layout: 2024/post
section: proposals
category: talks
author: Cristian González Guerrero
title: Herramientas libres para la creación de artes visuales
---

# Herramientas libres para la creación de artes visuales

>¿Cuáles son los programas que usan los cineastas profesionales y para qué sirven? ¿Alguno de ellos es software libre? ¿Qué hay de la industria fotográfica o de las artes visuales?<br><br>
En esta charla exploramos los flujos de trabajo de las industrias de la fotografía y del cine, e indagaremos en las herramientas software consideradas indispensables en la actualidad. En el camino, veremos que muchos de los procesos pueden agilizarse usando software libre. Analizamos las principales características de estas herramientas, comparándoloas con sus análogas privativas más conocidas. Desde la captura de imágenes hasta la postproducción, exploramos el impacto de las herramientas libres, que democratizan y  potencian la creación artística, promoviendo una comunidad más diversa y colaborativa.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla larga / Presencial
-   Idioma: Español

-   Descripción:

> El ponente actualmente realiza estudios de formación profesional de Iluminación, captación y tratamiento de la imagen. Como parte del programa de estudios, está obligado a usar software comercial para la realización de sus tareas de clase. Pero continuamente se pregunta: ¿todo esto podría hacerse con sotware libre?<br><br>
En esta charla analizaremos los flujos de trabajo de las industrias de la fotografía y del cine y veremos cómo las herramientas digitales han agilizado los procesos de producción, con herramientas consideradas indispensables en la actualidad. También veremos cómo las comunidades de software libre y de conocimiento abierto han contribuido con soluciones libres que perfectamente podrían usarse en la industria. Indagaremos en los motivos de la adopción mayoritaria del software privativo, así como en las ventajas del uso de software libre. Trataremos de responder a la pregunta ¿de verdad es tan necesario el software privativo en estas industrias?<br><br>
La idea es hablar, al menos, de las siguientes aplicaciones libres:
- Para foto:
  - digiKam
  - darktable
  - GIMP
- Para video:
  - Manuskript (me lo tengo que mirar aún)
  - Kdenlive

>En la presentación de cada solución libre, desarrollaremos brevemente para explicar las características que las hacen únicas e introducir su aprendizaje. Finalmente, exploraremos recursos que permitan profundizar en el aprendizaje de estas potentes herramientas.

-   Web del proyecto:

-   Público objetivo:

>Estudiantes o profesionales de las artes visuales, fotografía, cine, creadores de contenido o cualquier interesado en la creación de artes visuales.

## Ponente:

-   Nombre: Cristian González Guerrero

-   Bio:

> Tras graduarse en ingeniería de telecomunicaciones y trabajar durante seis años en la industria del software, Cristian decide cambiar por completo el rumbo y estudiar un FP en imagen y sonido. Por desgracia, descubre que la tendencia general en esta industria es la de usar software privativo, por lo que se cuestiona si podría hacer lo mismo con software libre.

### Info personal:

-   Web personal: <https://guyikcgg.github.io/>
-   Mastodon (u otras redes sociales libres): <https://mastodon.social/@guyikcgg>
-   GitLab (u otra forja) o portfolio general: <https://gitlab.com/guyikcgg>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso

