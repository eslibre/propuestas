---
layout: 2024/post
section: proposals
category: talks
author: Florencio Cano Gabarda
title: Análisis de datos con Python y vulnerabilidades de seguridad en 2023
---

# Análisis de datos con Python y vulnerabilidades de seguridad en 2023

>En esta charla voy a mostrar como analizar datos con Python con el objetivo de extraer información util. En concreto, analizaré las vulnerabilidades de seguridad a las que se le han asignado CVEs durante el año 2023. Analizaré tendencias, criticidades más frecuentes, CWEs más frecuentes y otra información de interes que pueda servirnos para mejorar la seguridad del software en 2024 y los años venideros.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla larga / Presencial
-   Idioma: Español

-   Descripción:

>Utilizaré entre otras librerías pandas para analizar los datos de CVE publicados por MITRE. Los CVE son un código que se asigna a las vulnerabilidades públicas para tenerlas identificadas. Habitualmente junto al CVE se asigna un CVSS, que es un valor de la criticidad de la vulnerabilidad y también un CWE que tiene que ver con el tipo de vulnerabilidad. En la charla analizaré tendencias, ¿hay más vulnerabilidad o menos que otros años? ¿cuál es la tendencian? ¿están cambiando los tipos de vulnerabilidades? Si somos desarrolladores, ¿qué tipo de vulnerabilidades tenemos que tener más en cuenta? Haré una presentación son slides porque en treinta minutos no me da tiempo hacerlo en directo pero mostraré como he procesado los datos y luego los números y gráficas más interesantes sobre los CVEs.

-   Público objetivo:

>Personas interesadas en introducirse en el análisis de datos con Python. También interesados en la seguridad informática y en el desarrollo seguro de código.

## Ponente:

-   Nombre: Florencio Cano Gabarda

-   Bio:

>Soy aficionado a la seguridad informática desde 1999 y trabajo en este campo desde 2004. Entre 2008 y 2015 tuve mi propia empresa de seguridad informática donde hacíamos entre otras cosas tests de intrusión y análisis forense. Entre 2015 y 2020 trabajé en Mercadona donde empecé como project mánager de proyectos de seguridad informática y donde durante tres años fui el máximo responsable de la división de seguridad informática como CISO. En 2020 empecé a trabajar en Red Hat donde ahora tengo el rol de Principal Product Security Architect y team lead del equipo de Security Architects.

### Info personal:

-   Mastodon (u otras redes sociales libres): <https://infosec.exchange/@florenciocano>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso

