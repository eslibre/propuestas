---
layout: 2024/post
section: proposals
category: talks
author: KANNO
title: Turbulencia en el capitalismo informacional&#58; hacia una concepción rizomática del hacking
---

# Turbulencia en el capitalismo informacional&#58; hacia una concepción rizomática del hacking

>Esta charla forma parte de un trabajo de investigación en el que se propone un enfoque rizomático del hacking. En filosofía, un rizoma se entiende como un sistema de conexiones horizontales, múltiples y no jerárquicas, que abarca diversos nodos. En el hacking encontramos diferentes nodos, desde el hacking ético hasta el cibercrimen, la ciberseguridad, el ciberespionaje, el software libre y el hacktivismo. En la charla se estudia el nodo que denomino “Ciber(in)Seguridad”, dónde se confronta el cibercrimen y el ciberespionaje estatal, y se plantea la cuestión de cómo se traza la línea entre lo malicioso y lo legítimo. También se propone el nodo “Contra(la)Cultura”, donde se examinan los fundamentos éticos del hacking y su relevancia en la producción de arte crítico y activista en Internet, como sucede en el Net.Art de los 90. Se concluye con una defensa del nodo contracultural del hacking, que funciona como una expresión de diversidad, cultura y tecnología libre.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla larga / Presencial
-   Idioma: Español

-   Descripción:

> Preguntarse sobre el significado del hacking, hackear o ser hacker es tan complejo como cuestionar qué es el arte, hacer arte y ser artista. Ambos universos, el del hacking y el del arte, se entrelazan en una red de significados que abarcan diversos contextos, épocas y consideraciones.<br><br>
En los años 80, ser hacker implicaba principalmente ser un apasionado de los sistemas informáticos. Sin embargo, en 2024, esta etiqueta también abarca, de manera equívoca, a cibercriminales, pentesters y hackers contratados por agencias de inteligencia para actividades de espionaje. De manera similar, el cuestionamiento sobre el arte en los años 80 nos llevaría a considerar nuestras respuestas en función de las corrientes artísticas predominantes tanto en esa década como en la contemporaneidad.<br><br>
En esta charla, se propone un enfoque rizomático del hacking. El término "rizoma" se refiere a un sistema de conexiones horizontales, múltiples y no jerárquicas, utilizado en filosofía para describir fenómenos que no pueden ser reducidos a una estructura lineal y fija, como los procesos sociales, culturales y mentales, así como el propio hacking y el arte. Comprender los diversos nodos que conforman la práctica del hacking en la sociedad es fundamental para diferenciarnos y promover los valores que defendemos: el software libre, el hardware libre, la privacidad, la diversidad y, sobre todo, la cultura libre.<br><br>
Esta charla se divide en cuatro bloques: la presentación del concepto de rizoma, el nodo de la “Ciber(in)Seguridad”, el nodo de la “Contra(la)Cultura” y el cierre.<br><br>
En primer lugar, se aborda una aproximación visual del rizoma y sus distintos nodos.<br><br>
En segundo lugar, se analiza el nodo que se ha denominado como el de la Ciber(in)Seguridad, que confronta el cibercrimen y el ciberespionaje estatal. Aunque aparentemente opuestos, ambos utilizan tácticas como el espionaje, la extorsión, la infección y el miedo para alcanzar sus objetivos. Se plantea la pregunta sobre cómo se traza la línea entre lo malicioso y lo legítimo, quién la establece y bajo qué justificaciones. ¿Por qué consideramos malicioso un ransomware pero no un software privativo que actúa como spyware para "protegernos"? ¿Qué diferencias hay entre la botnet Mirai y una empresa como Microsoft? ¿Y qué ocurre con Pegasus?<br><br>
En tercer lugar, se explora el nodo de la Contra(la)Cultura, donde se examinan los fundamentos éticos del hacking, como la contracultura, la libertad de acceso, la colaboración, la curiosidad, la diversidad y la privacidad, y se destaca su relevancia en la producción de arte crítico y activista en los albores de la era digital. Se hace mención al surgimiento del Net.art en la década de 1990, como un movimiento que aprovechó Internet como su principal medio de expresión y difusión. Se destacan obras como FloodNet, que fusionan arte conceptual y herramientas para la acción colectiva en línea, como una forma de protesta virtual.<br><br>
Finalmente, se concluye con una defensa del nodo contracultural del hacking, que funciona como una expresión de cultura, tecnología y diversidad libre.

-   Público objetivo:

>Público general

## Ponente:

-   Nombre: KANNO

-   Bio:

> KANNO (Laia Caballero Cano, 1999) es una artista transmedia de Barcelona residiendo actualmente en Valencia, especializada en tecnología, audiovisuales y sonido. Está graduada en Bellas Artes por la Universitat de Barcelona y actualmente cursa un máster en Artes Visuales y Multimedia por la Universitat Politècnica de València. Su investigación y producción comparte influencias del Hacktivismo, el Net.Art y el Post-Internet, abordando planteamientos creativos, críticos y contemplativos sobre la tecnología, la ciberseguridad y la cultura de Internet. Mediante la instalación, el sonido, la creación digital y la programación, presenta metodologías así como discursos alternativos sobre los fenómenos sociales online, el hacking y la disidencia.<br><br>
Aunque suelan habitar la esfera digital, sus producciones han sido expuestas en festivales y exposiciones como Ciudad Glitch (CCCC, Valencia), VOLUMENS (Valencia), ArtNou (Barcelona), dedicado al arte emergente de la capital catalana, y SenseTítol, una exposición organizada por la Universidad de Barcelona en la que presentan una selección de los mejores trabajos de investigación y producción de la promoción. Además de participar en exposiciones, también se ha involucrado en equipos de producción para eventos como LlumBCN (Ajuntam ent de Barcelona) y Sónar Tribute (HOC, Barcelona), en los que además ha sido invitada a realizar un DJ set para los mismos. Su trabajo fue presentado en el programa de radio "Rastros de Carmín" de Pilar Talavera, en El Prat Radio, quien junto a Víctor Ramírez en la sección Noise/Punk/Abstracción, presentan a la artista y uno de sus trabajos sonoros, Not Anymore, en el que se elabora un discurso paralelo entre la muerte y la desconexión digital.

### Info personal:

-   Web personal: <https://lcabcan.upv.edu.es/>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso

