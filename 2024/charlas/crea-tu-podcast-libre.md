---
layout: 2024/post
section: proposals
category: talks
author: Lorenzo Carbonell
title: Crea tu podcast libre, sin esfuerzo ni inversión (eso si, grabar tienes que grabar)
---

# Crea tu podcast libre, sin esfuerzo ni inversión (eso si, grabar tienes que grabar)

>El objetivo de esta charla es explicar las posibilidad de crear un podcast con su web asociada partiendo de herramientas Open Source y dos plataformas como son Internet Archive y GitLab. El planteamiento es hacerlo con el mínimo de esfuerzo e inversión posibles, pero maximizando los resultados.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla corta / Presencial
-   Idioma: Español

-   Descripción:

>El objetivo es explicar de forma sencila como puedes crear un pocast junto a una web con una mínima inversión partiendo de herramientas OpenSource.
La cuestión es que si bien actualmente existen distintas plataformas que te permiten alojar tu podcast, realmente son plataformas de terceros, donde tu podcast queda a su merced.<br><br>
Sin embargo una posibilidad muy interesante es la de publicar tus episodios del podcast en Internet Archive, para que esté disponible a todo el que quiera de forma completamente libre. Sin embargo, llegados a este punto, es necesario crear un feed, que distribuir y mejor si tenemos una página web.<br><br>
Todo esto lleva un trabajo asociado, que en el caso de otras plataformas de terceros es mucho mas sencillo.<br><br>
Sin embargo, hace algunos meses, implementé una sencilla herramienta que partiendo de Internet Archive, GitLab y Docker te permite tener tanto el feed, como la web, como la publicación en redes sociales como Mastodon. con solo lo que haces en Internet Archive.<br><br>
Es decir, con la metadata de Internet Archive se genera feed, web, redes sociales, lo que haces en una plataforma de terceros pero libre y pública.<br><br>
Creo que es una forma muy interesante de crear un podcast sin estar "atado" a terceros.<br><br>

-   Web del proyecto: <https://atareao.es/podcast/hacer-tu-propio-podcast-a-lo-facil/>

-   Público objetivo:

>Cualquier persona interesada en crear su propio podcast.

## Ponente:

-   Nombre: Lorenzo Carbonell

-   Bio:

>Soy el ser humano que está detrás del proyecto https://atareao.es. He impartido distintas charlas y talleres sobre Linux, Docke, etc. También en ocasiones anteriores he participado en esLibre.

### Info personal:

-   Web personal: <https://atareao.es>
-   Mastodon (u otras redes sociales libres): <https://mastodon.social/@atareao>
-   Twitter: <https://twitter.com/atareao>
-   GitLab (u otra forja) o portfolio general: <https://gitlab.com/atareao/>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso

