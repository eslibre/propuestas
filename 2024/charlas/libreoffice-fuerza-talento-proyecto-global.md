---
layout: 2024/post
section: proposals
category: talks
author: Gabriele Ponzo
title: LibreOffice&#58; fuerza y talento de un proyecto global
---

# LibreOffice: fuerza y talento de un proyecto global

>Introducción al proyecto LibreOffice, a la Fundación que lo coordina y cómo formar parte de los dos. 

## Detalles de la propuesta:

-   Tipo de propuesta: Charla larga / Presencial
-   Idioma: Español

-   Descripción:

>Describiré el proyecto a través de algunas cifras significativas y la comunidad que lo mantiene vivo, explicando de qué formas se puede participar y por qué.

-   Web del proyecto: <https://es.libreoffice.org>

-   Público objetivo:

>El objetivo de la charla es mostrar algunos datos interesantes sobre uno de los mayores proyectos de código abierto del mundo y, posiblemente, reclutar nuevos voluntarios para hacerlo aún mejor. También es importante mostrar a las autoridades y administraciones públicas la importancia del software libre.

## Ponente:

-   Nombre: Gabriele Ponzo

-   Bio:

>Un usuario pionero desde los días de StarOffice, se unió a Progetto Linguistico Italiano OpenOffice durante la Conferencia de 2009 en Orvieto, y ha formado parte de la comunidad OpenOffice.org hasta el nacimiento de LibreOffice. La formación y el soporte son sus actividades más habituales y ha estado trabajando en los proyectos LibreUmbria y LibreDifesa como profesor.<br><br>
Es uno de los fundadores de LibreItalia, donde también es parte de la Junta Directiva. Sus contribuciones son sobre todo hablar en conferencias, algunas traducciones, y el apoyo a los usuarios en las listas de correo italiano e internacional, Formador certificado y experto en migración desde febrero de 2015.

### Info personal:

-   Mastodon (u otras redes sociales libres): <https://mastodon.uno/@gippy>
-   Twitter: <https://twitter.com/@PonzoGabriele>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso

