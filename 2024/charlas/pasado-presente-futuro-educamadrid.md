---
layout: 2024/post
section: proposals
category: talks
author: Adolfo Sanz De Diego
title: Pasado, presente y futuro de EducaMadrid
---

# Pasado, presente y futuro de EducaMadrid

>EducaMadrid es la Plataforma Educativa de la Comunidad de Madrid. Basada en Software Libre, en un entorno seguro, soberano y sostenible, ofrece múltiples servicios interconectados y complementarios: aulas virtuales, páginas web de centros, blogs de profesores, mediateca, nube, correo, videoconferencia, MAdrid_linuX, etc. La Pandemia primero, y la Competencia Digital Docente después, ha catapultado su uso en el ámbito educativo no Universitario. En esta charla quiero contar cómo empezamos, cómo crecimos, cómo es nuestro día a día, tanto a nivel técnico como a nivel funcional, y cómo veo su futuro.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla corta / Presencial
-   Idioma: Español

-   Descripción:

>EducaMadrid es la Plataforma Educativa de la Comunidad de Madrid. Basada en Software Libre, en un entorno seguro, soberano y sostenible, ofrece múltiples servicios interconectados y complementarios: aulas virtuales, páginas web de centros, blogs de profesores, mediateca, nube, correo, videoconferencia, MAdrid_linuX, etc. La Pandemia primero, y la Competencia Digital Docente después, ha catapultado su uso en el ámbito educativo no Universitario. En esta charla quiero contar cómo empezamos, cómo crecimos, cómo es nuestro día a día, tanto a nivel técnico como a nivel funcional, y cómo veo su futuro.

-   Web del proyecto: <https://www.educa2.madrid.org/educamadrid/>

-   Público objetivo:

>Cualquier persona en general.

## Ponente:

-   Nombre: Adolfo Sanz De Diego

-   Bio:

>Jefe de Servicio de Plataformas Educativas (@EducaMadrid y @MAX_MAdridlinuX). Colaboro además como profesor universitario y formador técnico.

### Info personal:

-   Web personal: <https://www.asanzdiego.com/>
-   Mastodon (u otras redes sociales libres): <https://mastodon.social/@asanzdiego>
-   Twitter: <https://twitter.com/@asanzdiego>
-   GitLab (u otra forja) o portfolio general: <https://github.com/asanzdiego>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso

