---
layout: 2024/post
section: proposals
category: talks
author: Ana Galán 
title: Youth Hacking 4 Freedom&#58; la competición de programación gratuita para jóvenes europeos 
---

# Youth Hacking 4 Freedom: la competición de programación gratuita para jóvenes europeos 

>¿Tienes entre 14 y 18 años, te gusta programar y vives en Europa? ¿Te gustaría conocer a otros jóvenes europeos con tus mismas inquietudes y tener la posibilidad de ganar hasta 4000 euros? Youth Hacking 4 Freedom es para ti.<br><br>
En esta presentación explicaremos esta competición de la FSFE que ya va por su tercera edición 

## Detalles de la propuesta:

-   Tipo de propuesta: Charla corta / Presencial
-   Idioma: Español

-   Descripción:

>¿Tienes entre 14 y 18 años, te gusta programar y vives en Europa? ¿Te gustaría conocer a otros jóvenes europeos con tus mismas inquietudes y tener la posibilidad de ganar hasta 4000 euros? Youth Hacking 4 Freedom es para ti.<br><br>
En esta presentación explicaremos esta competición de la FSFE que ya va por su tercera edición 

-   Web del proyecto: <https://fsfe.org/activities/yh4f/>

-   Público objetivo:

>Jóvenes y no tan jóvenes entusiastas del software libre.
Si hubiese un publico aún más joven (niños en edad escolar) contamos también con la posibilidad de realizar una pequeña actividad con un puzzle .

## Ponente:

-   Nombre: Ana Galán 

-   Bio:

> FSFE Senior Communications Manager

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso

