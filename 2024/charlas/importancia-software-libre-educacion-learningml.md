---
layout: 2024/post
section: proposals
category: talks
author: Juan David Rodríguez García
title: La importancia del software libre en educación&#58; el caso de LearningML
---

# La importancia del software libre en educación: el caso de LearningML

>LearningML es una plataforma educativa para el aprendizaje y la enseñanza de los fundamentos del Machine Learning a estudiantes desde los 10 a los 16 años especialmente. Ha sido construida sobre los cimientos del software libre y se distribuye como tal. En esta charla se contará la historia de este proyecto, lo que tenemos preparado para su futuro, y algunos aspectos técnicos, haciendo hincapié en el papel protagonista que el software libre ha jugado en su desarrollo.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla larga / Presencial
-   Idioma: Español

-   Descripción:

> En 2019 comencé a desarrollar LearningML, una plataforma educativa para el aprendizaje y la enseñanza de los fundamentos del Machine Learning a estudiantes desde los 10 a los 16 años especialmente. Y en marzo de 2020 comenzó su andadura en linea.<br><br>
Desde entonces, un gran número de docentes de todo el mundo, especialmente españoles e iberoamericanos, la están usando y desarrolando interesantes actividades para introducir en sus clases contenidos de Inteligencia Artificial y Machine Learning.<br><br>
Aunque comenzó siendo una aplicación web,  durante el año 2022 desarrollé en colaboración con la fundación Cruzando de Chile LearningML-Desktop, una versión de escritorio que puede instalarse en computadoras con Windows, MacOS o Linux y que independiza su uso de la conexión a Internet. El principal motivo de este desarrollo fue facilitar la enseñanza de la Inteligencia Artificial y el Pensamiento Computacional en lugares desfavorecidos con problemas o ausencia de conectividad a Internet. Aunque, como efecto colateral, aprovechando que puede instalarse en Linux, esta versión también ha sido incorporada al sistema operativo MAX, la distribución educativa de Linux de la Comunidad de Madrid.<br><br>
También, en colaboración  con la asociación Echidna Educación, desarrollé una modificación de Scratch para programar las tarjetas Echidna. De esta manera se puede combinar la creación de proyectos educativos que combinan la Inteligencia Artificial y la Robótica Educativa. Actualmente estamos desarrollando una versión de escritorio, indendiente de Internet, que hemos denominado EchidnaML y que también será instalable en los sistemas operativos Windows, MacOS o Linux.<br><br>
Actualmente con LearningML se pueden construir modelos de Machine Learning capaces de reconocer textos, imágenes o conjuntos numéricos. En un futuro el proyecto pretende crecer para reconocer más tipos de objetos, como sonidos y poses. También pretendo añadir un módulo de Inteligencia Artificial generativa con la misma filosofía que el resto de la aplicación: enseñar los fundamentos a través de la construcción de sistemas de IA sencillos pero reales.<br><br>
En la charla que propongo contaré con más detalles esta historia y la importancia que el software libre ha tenido en el desarrollo de la herramienta. También pretendo esbozar la arquitectura software de las distintas versiones; on-line, desktop y la conexión con las placas Echidna.<br><br>
Aunque propongo una charla de 30 minutos, para poder abarcar todo lo anterior, si no fuera posible podría reducirla a 15 minutos.<br><br>

-   Web del proyecto: <https://learningml.org>

-   Público objetivo:

>Docentes de todos los niveles educativos, desarrolladores de aplicaciones interesados en el software libre y todo tipo de personas interesadas en conocer recursos para enseñar y aprender contenidos de Inteligencia Artificial de una manera práctica y sencilla.

## Ponente:

-   Nombre: Juan David Rodríguez García

-   Bio:

> Soy Asesor Técnico Docente en el Instituto Nacional de Tecnologías Educativas y Formación del Profesorado donde desempeño tareas de administración de sistemas y control del ciclo de vida del software. Participé en el diseño e implementación de las 2 primeras ediciones de la Escuela de Pensamiento Computacional e Inteligencia Artificial del INTEF. Desde hace algún tiempo, y en mi tiempo libre, estoy trabajando en el desarrollo del pensamiento computacional a través de actividades prácticas sobre inteligencia artificial. He comenzado a explorar cómo el aprendizaje automático, una de las técnicas de inteligencia artificial más utilizadas en la actualidad, puede ser enseñado en la escuela. Para esto estoy desarrollando una plataforma educativa (http://learningml.org) diseñada para construir fácilmente modelos de aprendizaje automático que pueden ser usados en programas Scratch.<br><br>
Imparto cursos para la formación del profesorado sobre Programación, Inteligencia Artificial y Pensamiento Computacional y he participado en distintos eventos relacionados con el mundo educativo (EDAulaBlog, Evento sobre IA de la Organización de Estados Iberoamericanos, Evento sobre IA de la fundación Cruzando de Chile, Evento sobre IA de la fundación Telefónica, Eploratori Datos Éticos, Fundación Bofill...)

### Info personal:

-   Web personal: <https://juandarodriguez.es>
-   Twitter: <https://twitter.com/@juandalibaba>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso

