---
layout: 2024/post
section: proposals
category: talks
author: Miguel Pellicer
title: Fundación Apereo&#58; al servicio del software libre educativo 
---

# Fundación Apereo: al servicio del software libre educativo 

>Apereo Foundation con base en New Jersey, se fundó para ayudar a las organizaciones educativas a fomentar, desarrollar y sostener tecnologías abiertas para apoyar el aprendizaje, la enseñanza y la investigación. Un ejemplo de colaboración a nivel mundial en el que organizaciones e individuos colaboran en diversidad de proyectos tecnológicos para ayudar en la educación del futuro.<br><br>
Con 2.200 miembros, más de 550 contribuidores de código, 50 instituciones, 12 proyectos y 5 en incubación, Apereo representa una de las comunidades más relevantes del espacio educativo.<br><br>
Una rápida exposición dónde se explicará las bases, organización y base democrática sobre la que se basa Apereo para su funcionamiento así como la presentación de proyectos tecnológicos sostenidos por la Fundación. 

## Detalles de la propuesta:

-   Tipo de propuesta: Charla corta / Presencial
-   Idioma: Español

-   Descripción:

>La Fundación Apereo es una organización benéfica pública estadounidense de ámbito mundial al servicio del amplio y diverso sector educativo. La misión principal de Apereo es "ayudar y facilitar a las organizaciones educativas que colaboran para fomentar, desarrollar y sostener tecnologías abiertas e innovación para apoyar el aprendizaje, la enseñanza y la investigación". Apereo se esfuerza por hacer realidad esa misión a través de iniciativas impulsadas por la comunidad: sirviendo de hogar a proyectos de software de código abierto, cultivando redes de prácticas entre iguales y abogando por la adopción y participación en recursos abiertos.<br><br>
Actúa como patrocinador fiscal proporcionando a los proyectos miembros infraestructura técnica, así como servicios empresariales, jurídicos y administrativos centralizados.<br><br>
Promueve la colaboración práctica entre organizaciones educativas, el intercambio de mejores prácticas y la difusión de resultados con enfoques innovadores para aplicar arquitecturas y sistemas de tecnología abierta en un contexto educativo.<br><br>
Desarrolla y sostiene una comunidad global de interés entre instituciones educativas para avanzar en el software de código abierto y los estándares, tecnologías, arquitecturas, sistemas y contenidos abiertos, promoviendo la colaboración internacional en estas áreas.<br><br>
Crea, a través de sus diversas actividades, tanto conferencias, proyectos y actividades de divulgación, con una atmósfera de confianza, buena voluntad y respeto mutuo entre todos los participantes.<br><br>
Proporciona gobernanza e infraestructura para la gestión de la propiedad intelectual del código abierto con el fin de aumentar su sostenibilidad.

-   Web del proyecto: <https://www.apereo.org/>

-   Público objetivo:

>Universidades e instituciones públicas con área de formación.

## Ponente:

-   Nombre: Miguel Pellicer

-   Bio:

>Miembro PMC Apereo

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso

