---
layout: 2024/post
section: proposals
category: talks
author: G{á}maliel, Rita Barrachina
title: Comunicaciones libres
---

# Comunicaciones libres

>Propuestas para reconstruir el panorama comunicativo en base a la cultura libre. Se puede comunicar sin alimentar más al monstruo y, quien quiera probar, puede empezar ya mismito.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla larga / Presencial
-   Idioma: Español

-   Descripción:

>Bajo el nombre de "Comunicaciones Libres" se aglutinan criterios y prácticas para difundir proyectos sin depender de multinacionales, recreando así un ecosistema digital soberano y saludable. Las herramientas están y si no, nos las inventamos. Pero para usarlas hay que ser consciente de la distopía digital y querer hacer algo al respecto.<br><br>
Os invitamos a repensar el Komun, a usar software libre y servidores de proximidad, a conocer truquis para librarse de las multinacionales pero, sobre todo, a organizarnos mejor como gentes que llevamos un nuevo mundo en nuestras herramientas.

-   Web del proyecto: <https://foro.komun.org/c/comunicaciones-libres>

-   Público objetivo:

>- Personas creadoras de tecnologías auditables y/o entidades sociales que todavía promocionan el capitalismo cognitivo.
- Personas interesadas en la comunicación en general.

## Ponente:

-   Nombre: G{á}maliel y Rita Barrachina

-   Bio:

>G{á}maliel: como buen ángel caído, fue desterrado por buscar la cultura libre y no seguir los mandatos del Dios "industria". Actualmente, trabaja ayudando a cooperativas a liberar sus herramientas y compartir su conocimiento en busca de una soberanía integral. Si tienes la suerte de encontrártelo quizás puedas contratarlo. Pista. El Komun se escribe con K.<br><br>
Rita: divulgadora de cultura libre digital y lingüística. Socia de eXO, miembro de Anartist, Komun, Fedicat y la Juna, co-inventora de SnapArcade. A pesar del temporal, intento narrar en positivo. 

### Info personal:

-   Web personal: <https://komunikilo.org/>
-   Mastodon (u otras redes sociales libres): <https://hostux.social/@komun>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso

