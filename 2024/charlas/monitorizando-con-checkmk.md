---
layout: 2024/post
section: proposals
category: talks
author: Isabel Bernal Márquez
title: Monitorizando con Checkmk
---

# Monitorizando con Checkmk

>Checkmk es una solución de monitorización open source. Basado inicialmente en Nagios, combina varios tipos de monitorización y más de 1800 plugins, y el número va aumentando gracias a su comunidad.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla larga / Presencial
-   Idioma: Español

-   Descripción:

>Para simplificar la configuración, todos los componentes de checkmk se distribuyen completamente integrados, sin necesidad de configurar bases de datos o servidores web. Tiene un foro muy activo, que es el principal punto de encuentro entre desarrolladores y usuarios.<br><br>
En esta charla trataré aspectos de su instalación, configuración e integración con otras herramientas como Centreon. Mi idea es dar una visión global a través de ejemplos (en 30 minutos no me da tiempo a profundizar), pero creo que bastará para ver su potencia. Si esto sale bien en un futuro me gustaría pasar a formato taller más práctico.

-   Web del proyecto: <https://checkmk.com/>

## Ponente:

-   Nombre: Isabel Bernal Márquez

-   Bio:

>Me dedico laboralmente al desarrollo y mantenimiento de sistemas de monitorización (Nagios/Centreon/check_mk) para la Junta de Andalucía, y en ocasiones escribo artículos de temática GNU/Linux y monitorización en LinkedIn y en mi blog. En mi GitLab subo plugins que desarrollo para Centreon.<br><br>
Hace muchos años (hacia el 2004-2008) administraba el foro de Debian esDebian, ya desaparecido.

### Info personal:

-   Web personal: <https://binbash.btw.so/>
-   GitLab (u otra forja) o portfolio general: <https://gitlab.com/ibel_lovelace1>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso

