---
layout: 2024/post
section: proposals
category: talks
author: Daniel Esteban Roque
title: ¿Y si nos quedamos sin Internet? Desarrollo de la competencia digital offline con MAdrid_linuX (MAX)
---

# ¿Y si nos quedamos sin Internet? Desarrollo de la competencia digital offline con MAdrid_linuX (MAX)

>¿Qué ocurre cuando en un centro educativo nos quedamos sin conexión a Internet ¿Cerramos el centro? ¿Paralizamos cualquier actividad relacionada con la competencia digital? ¿Y si esta desconexión no es algo accidental y se basa en una decisión del centro o del usuario? En estas situaciones MAdrid_linuX (MAX) es lo que necesitas.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla larga / Presencial
-   Idioma: Español

-   Descripción:

>En pleno año 2024 donde el estar conectados a la red parece algo imprescindible e, incluso, innegociable, presentamos una experiencia de desarrollo de la competencia digital offline utilizando un ordenador.<br><br>
Durante media hora el ponente utilizará el sistema operativo MAX (MAdrid_linuX) para desarrollar todo tipo de tareas cotidianas del docente y actividades de desarrollo de competencia digital del alumno o de cualquier persona:
- Grabación de una cuña publicitaria
- Pixelar fotos
- Pixelar vídeos
- Generar subtítulos de manera automática y modificarlos.
- Efecto Chroma en vídeo
- Película de Stop motion
- Película Time Lapse
- Uso de herramientas de accesibilidad
- Grabación de pantalla
- Entrenamiento de una IA<br><br>
Estas son algunas de las tareas que en directo realizará el ponente.

-   Web del proyecto: <https://www.educa2.madrid.org/web/max/>

-   Público objetivo:

>Cualquier persona pero sobre todo profesionales relacionados con la educación.

## Ponente:

-   Nombre: Daniel Esteban Roque

-   Bio:

>Maestro y Director del CRA Los Olivos durante 15 años. Un entorno rural donde se desarrolla el proyecto "Somos teCnoRurAles" basado en el uso de Software Libre, la distribución GNULinux MAX y la Plataforma EducaMadrid.<br><br>
En la actualidad Asesor Técnico Docente del Servicio de Plataformas Educativas de la Consejería de Digitalización de la Comunidad de Madrid donde realizo, entre otras funciones, la coordinación del desarrollo de MAX.

### Info personal:

-   Twitter: <https://twitter.com/directorcra>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso

