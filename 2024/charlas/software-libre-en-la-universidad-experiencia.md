---
layout: 2024/post
section: proposals
category: talks
author: Alejandro Rodríguez Antolín 
title: Software Libre en la Universidad&#58; la experiencia en el Grado de Historia y Ciencias de la Música de la Universidad Autónoma de Madrid
---

# Software Libre en la Universidad: La experiencia en el Grado de Historia y Ciencias de la Música de la Universidad Autónoma de Madrid

>La charla versará sobre la experiencia como docente universitario con el software utilizado habitualmente en los estudios de musicología y en el campo de la creación, edición y producción musical, así como el plan de integración e implementación de alternativas libres a este tipo de software en dichos entornos académicos

## Detalles de la propuesta:

-   Tipo de propuesta: Charla corta / Presencial
-   Idioma: Español

-   Descripción:

>Histórica y tradicionalmente, el software que se utiliza en los grados de Musicología y/o Historia y Ciencias de la Música es privativo y con licencias de uso restrictivas y caras. Esto supone varios inconvenientes a la hora de planificar la docencia con este tipo de software. Por un lado, los alumnos aprenden a manejar programas sometidos a elevados costes de adquisición; por el otro, no pueden acceder al código de dicho software, ni para comprender su funcionamiento ni para su modificarlos (implicando, a su vez, la imposibilidad de desarrollar módulos nuevos).<br><br>
Este tipo de estrategias "vician" el sistema educativo, obligando a los alumnos al consumo de software privativo tanto durante su etapa académica como en sus futuros entornos profesionales (incluso, formando a su vez a otros alumnos), así como limitando su versatilidad y polivalencia en el conocimiento y uso de múltiples programas. Sin embargo, y gracias a que hoy en día contamos con alternativas libres y profesionales al software privativo utilizado en el campo de la musicología y la edición y producción musical, es posible trazar estrategias de implementación de dichas alternativas en los entornos académicos y artísticos.<br><br>
En esta charla haremos un repaso de qué tipo de software privativo se suele utilizar en los estudios universitarios de musicología (centrándonos en la experiencia previa como docente en el Grado de Historia y Ciencias de la Música y Tecnología Musical de la Universidad Autónoma de Madrid) y cuáles son las alternativas libres a esos programas. Además, se presentará la nueva versión de AudioFLOSS como proyecto recopilatorio de software libre, y portable, para Windows.

-   Web del proyecto: <https://audiofloss.melisa.gal/>

-   Público objetivo:

>Personas interesadas en el software libre en general, interesados en el software de creación y producción musical, docentes, musicólogos y/o músicos

## Ponente:

-   Nombre: Alejandro Rodríguez Antolín 

-   Bio:

>Musicólogo por la Universidad Autónoma de Madrid. Máster en Estudios Artísticos, Literarios y de la Cultura en la especialidad de Música y Artes Escénicas. Doctor con la especialidad en Musicología, especializado en música electroacústica, arte sonoro y experimentaciones sonoro-tecnológicas en España. Melómano, cinéfilo, videojugador, aficionado al ajedrez y friki de las tecnologías sonoras en general. Ya ha participado anteriormente en el marco del esLibre, además de realizar a lo largo de su carrera diversas conferencias y talleres relativos al software libre en general, y el software libre para la creación, edición y producción musical en particular.

### Info personal:

-   Twitter: <https://twitter.com/@AlexRoan3>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso

