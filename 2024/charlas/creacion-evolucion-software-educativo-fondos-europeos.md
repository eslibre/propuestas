---
layout: 2024/post
section: proposals
category: talks
author: Juan Tatay Galvany
title: Creación y evolución de software educativo con fondos europeos
---

# Creación y evolución de software educativo con fondos europeos.

>Presentación de 3 proyectos financiados para la creación y mejora de soluciones como son Juez LTI, y los proyectos de Unidigital: uniadaptive y S2U Sakai.<br><br>
El alcance de todos los proyectos mejora la oferta de la comunidad de software libre, mediante nuevas herramientas o el desarrollo de código integrado en las ramas master de los proyectos. 

## Detalles de la propuesta:

-   Tipo de propuesta: Charla corta / Presencial
-   Idioma: Español

-   Descripción:

>Un problema recurrente en el desarrollo de software de código abierto es la financiación del trabajo de investigación y desarrollo necesario.<br><br>
Para ello, las empresas de software libre, trabajamos para promocionar y promover entre los clientes la necesidad de mejorar el producto, ampliar su alcance de acuerdo con las necesidades de sus usuarios, y contribuir a los proyectos de los que se derivan los nuevos desarrollos, dichas mejoras o ampliaciones para mejorar el proyecto inicial.<br><br>
Hemos participado en varios proyectos con fondos europeos, tanto derivados de fondos de investigación compartidos por varias entidades de ámbito comunitario, como en proyectos derivados de los PERTE, como es el caso “unidigital” en el que hemos trabajado con 2  diferentes consorcios de universidades españolas.<br><br>
Los proyectos han consistido en el desarrollo de herramientas LTI, el estándar de interoperabilidad en eLearning, y la incorporación de herramientas e integraciones para mejorar el alcance de plataformas LMS de ámbito internacional, incorporando las mejoras solicitadas desde universidades españolas a universidades en todo el mundo.<br><br>
De este modo, hemos trabajado en JuezLTI, una herramienta para el perfeccionamiento y autocorrección de ejercicios en diferentes lenguajes de programación; en uniadaptive, una LTI para la edición de itinerarios formativos incorporando capacidades adaptativas que se ajustan al progreso del alumno y S2U, que incorpora desarrollos e integraciones de nuevas herramientas que mejoran la capacidad de los centros educativos con el LMS Sakai, de origen global y cuyas recientes innovaciones han sido aportadas por universidades españolas.

-   Web del proyecto:

>- <https://juezlti.eu/es/>
- <https://github.com/uniadaptiveLTI>
- <https://sakaiproject.atlassian.net/jira/software/c/projects/S2U/issues>

-   Público objetivo:

>Universidades e instituciones públicas con área de formación.

## Ponente:

-   Nombre: Juan Tatay Galvany

-   Bio:

>20 años en proyectos de software open source, en los ámbitos de la salud y  educación online, siempre involucrado en la promoción del software libre y la gestión de empresas de desarrollo de software basado en open source, con especial foco en gestión de proyectos, operaciones y desarrollo de negocio.

### Info personal:

-   Web personal: <https://www.linkedin.com/in/juantatay/>
-   Mastodon (u otras redes sociales libres): <https://mastodon.social/@juan_silta>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso

