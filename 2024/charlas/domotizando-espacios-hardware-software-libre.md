---
layout: 2024/post
section: proposals
category: talks
author: Ismail Ali Gago
title: Domotizando espacios con hardware y software libre&#58; una propuesta didáctica
---

# Domotizando espacios con hardware y software libre. Una propuesta didáctica..

>Con el auge de la robótica, y la domótica en los centros docentes de secundaria, y en entornos domésticos, para la automatización de procesos, me parece de interés la presentación y descripción de un sistema dómotico basado en los dispositivos de hardware libre Home Assistant Yellow (<https://www.home-assistant.io/yellow/>), HomeAssistant Blue (<https://www.home-assistant.io/blue/>) y/o Raspberry, con sensores zigbee, y de coste asumible.<br><br>
Todo ello controlado desde el ecosistema de software libre Home Assistant y el protocolo de comunicación abierto Zigbee.<br><br>
Sistemas domóticos aplicables y utilizables tanto en entornos educativos como domésticos.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla larga / Presencial
-   Idioma: Español

-   Descripción:

>En esta charla presentaré ejemplos de utilización de la domótica en entornos educativos y domésticos.<br><br>
Todo ello utilizando exclusivamente hardware y software libre y protocolos de comunicación abiertos, respetando la privacidad y seguridad de los datos generados y transmitidos en el sistema y evitando la utilización de plataformas y servicios comerciales que hacen minería de datos.<br><br>
La experiencia está englobada en los contenidos de robótica, y  domótica, de los centros docentes de secundaria, y me parece de interés la presentación y descripción de un sistema dómotico basado en los dispositivos de hardware libre Home Assistant Yellow (<https://www.home-assistant.io/yellow/>), HomeAssistant Blue (<https://www.home-assistant.io/blue/>) y/o Raspberry, con sensores Zigbee, y de coste asumible.<br><br>
Todo ello controlado desde el ecosistema de software libre Home Assistant y el protocolo de comunicación abierto zigbee (<https://csa-iot.org/newsroom/connectivity-standards-alliance/>).<br><br>
Se presentarán ejemplos de utilización de sensores que permitiran controlar dispositivos de climatizacion, sensores para medir, por ejemplo, temperatura y humedad; enchufes inteligentes que permiten controlar otros dispositivos, como por ejemplo sistemas de autoconsumo fotovoltaico, su producción y la gestión del vertido cero para no producir excedentes; encendido y apagado de ordenadores y sistemas multimedia, sistemas de gestión de presencia mediante cámaras de vídeo vigilancia, etc...<br><br>
Se describirán, si el tiempo asignado nos lo permite, las ventajas e inconvenientes del protocolo de comunicación zigbee frente a otros sistemas de comunicación domótica como la wifi.<br><br>
Sistemas domóticos aplicables y utilizables tanto en los entornos educativos descritos como en entornos domésticos.

-   Público objetivo:

>- Profesorado y alumnado de todos los niveles y etapas educativas.
- Desarrolladores de software libre aplicado a la educación y a la robótica.
- Cualquier persona interesada en las aplicaciones de la robótica y la domótica, tanto a nivel didáctico como para uso doméstico.
- Cualquier persona interesada en el hardware y en el software libre y en el conocimiento libre y abierto.
- Público en general.
- STEAM, TIC, OpenScience, Robótica, Domótica. 

## Ponente:

-   Nombre: Ismail Ali Gago

-   Bio:

>Profesor de Biología y Geología a nivel de Enseñanza Secundaria.<br><br>
También he sido profesor Asociado de Nuevas Tecnologías aplicadas a la Educación en la Facultad de Formación de Profesorado de la Universidad Autónoma de Madrid.<br><br>
Promotor y primer coordinador del Grupo de Desarrollo MAX MAdrid_linuX: <https://www.educa2.madrid.org/web/max><br><br>
Promotor y primer coordinador del Grupo de Desarrollo eXeLearning: <https://exelearning.net/><br><br>
Actualmente sigo colaborando con ambos grupos y como formador de formadores en las áreas TIC y STEAM.<br><br>
Scientix Ambassador: <https://www.scientix.eu/in-your-country/scientix-4-teacher-panel##ES><br><br>
Aficionado a la robótica y la domótica.

### Info personal:

-   Web personal: <https://www.educa2.madrid.org/web/ismail.ali> / <https://es.linkedin.com/in/ismail-ali-gago-3aa5862b>
-   Twitter: <https://twitter.com/@ismagago>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso

