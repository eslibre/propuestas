---
layout: 2024/post
section: proposals
category: talks
author: Jorge Lama, David Marzal
title: Accesibilidad con Tecnologías Libres&#58; un repaso a los proyectos más interesantes que hemos tratado en los programas de este podcast
---

# Accesibilidad con Tecnologías Libres: un repaso a los proyectos más interesantes que hemos tratado en los programas de este podcast

>En esta charla, varios de los colaboradores, daremos a conocer varios proyectos relacionados con el mundo de la accesibilidad que hemos abordado en el podcast "Accesibilidad con Tecnologías Libres". También comentaremos nuestra experiencia organizando un programa de podcasting de este tipo.<br><br>
En este podcast colaboran: David Marzal, Pablo Arias, Thais Pousada, Jonathan Chacón, Jorge Lama, Victor y Markolino.
- Más información sobre el podcast:
    + Web: <https://accesibilidadtl.gitlab.io/>
    + Feed de suscripcción: <https://accesibilidadtl.gitlab.io/feed>

## Detalles de la propuesta:

-   Tipo de propuesta: Charla larga / Presencial
-   Idioma: Español

-   Descripción:

>En esta charla, varios de los colaboradores, daremos a conocer varios proyectos relacionados con el mundo de la accesibilidad que hemos abordado en el podcast "Accesibilidad con Tecnologías Libres". También comentaremos nuestra experiencia organizando un programa de podcasting de este tipo.<br><br>
En este podcast colaboran: David Marzal, Pablo Arias, Thais Pousada, Jonathan Chacón, Jorge Lama, Victor y Markolino.
- Más información sobre el podcast:
    + Web: <https://accesibilidadtl.gitlab.io/>
    + Feed de suscripcción: <https://accesibilidadtl.gitlab.io/feed>
    
-   Web del proyecto: <https://accesibilidadtl.gitlab.io/>

-   Público objetivo:

>Todo el mundo interesado en temas de accesibilidad y tecnologías libres, además del interesado en crear proyectos de divulgación audiovisuales.

## Ponente:

-   Nombre: Jorge Lama y David Marzal

-   Bio:

>Jorge Lama: Productor del podcast "Accesibilidad con Tecnologías libres" y muchos otros. Me apasiona la tecnología, el audio y todo lo que tiene que ver con la Cultura libre y Software libre.<br><br>
David Marzal: Cultura Libre, Residuo Cero, Escéptico, Veganismo... aunque ahora mayormente conciliando la crianza/paternidad. Made in Cartagena. UTC +1/+2. 🐧 :archlinux: :kde: :opensource: :vegan: 🦀 🐍 🐋 🦭 🐘 🐙 🐇 🐝 🐞 🐡 :fediverse: :rss:
Free Software & Zero Waste. Go vegan!, Go Podcasting!

### Info personal:

-   Mastodon (u otras redes sociales libres): <https://mastodon.social/@raivenra> / <https://mastodon.escepticos.es/@DavidMarzalC>
-   Twitter: <https://twitter.com/@raivenra>
-   GitLab (u otra forja) o portfolio general: <https://accesibilidadtl.gitlab.io/>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso

