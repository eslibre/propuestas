---
layout: 2024/post
section: proposals
category: talks
author: José Antonio González Nóvoa, Jorge Lama
title: El papel de las tecnologías libres para reducir el impacto ambiental y social
---

# El papel de las tecnologías libres para reducir el impacto ambiental y social.

>Con esta charla se busca dar a conocer y concienciar sobre el impacto ambiental y social de las tecnologías que consumimos en nuestro día a día, y del papel que juegan las tecnologías libres para reducirlo. Se tratarán temas como el derecho a reparar, la huella de carbono generada durante todo el ciclo de vida y la electrónica ética. ¿Cómo de libre es el hardware libre?

## Detalles de la propuesta:

-   Tipo de propuesta: Charla corta / Presencial
-   Idioma: Español

-   Descripción:

> Con esta charla se busca dar a conocer y concienciar sobre el impacto ambiental y social de las tecnologías que consumimos en nuestro día a día, y del papel que juegan las tecnologías libres para reducirlo. Se tratarán temas como el derecho a reparar, la huella de carbono generada durante todo el ciclo de vida y la electrónica ética. ¿Cómo de libre es el hardware libre?

-   Web del proyecto: <https://galicia.isf.es/>

-   Público objetivo:

>Público en general

## Ponentes:

-   Nombre: José Antonio González Nóvoa

-   Bio:

>Montañero y cicloturista. Voluntario de Enxeñería Sen Fronteiras Galicia. Ingeniero con especial interés en la parte más social de la tecnología, en concreto en los campos de la electrónica ética y la soberanía tecnológica. Participé previamente como ponente en dos ediciones de la esLibre, además de en otros eventos similares organizados en Galicia por comunidades relacionadas con las tecnologías libres. También impartí charlas sobre esta temática en institutos de secundaria y en otros ámbitos.

-   Nombre: Jorge Lama

-   Bio:

>Soy miembro de varios colectivos relacionados con el mundo del software libre y la cultura libre. Me gusta la tecnología y la música, y disfruto utilizando tecnologías libres. También me gusta dar charlas y talleres, intentado dar a conocer estos mundos. Llevo más de 10 años produciendo podcast, por lo que tengo conocimiento en temas de grabación y edición de sonido, así como de micrófonos, interfaces de sonido, altavoces y otro tipo de cacharros (amplificadores, ecualizadores, crossovers, etc). Soy coordinador del Banco de reciclaje electrónico en A Coruña de Enxeñería Sen Fronteiras, donde preparamos equipos usados con software libre y los donamos a través de diferentes entidades.

### Info personal:

-   Web personal: <https://galicia.isf.es/>
-   Mastodon (u otras redes sociales libres): <https://mastodon.gal/@ESFGalicia>
-   Twitter: <https://twitter.com/@ESFGALICIA>

## Comentarios

>Participamos en las dos anteriores ediciones del esLibre (Vigo y Zaragoza) hablando sobre uno de los proyectos que llevamos a cabo en Enxeñería Sen Fronteiras Galicia: los bancos de reciclaxe electrónica con software libre. Esta vez nos gustaría hablar de la parte social y ambiental que hay detrás de todo esto, y del papel clave que desempeñan las tecnologías libres para reducir este impacto.

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso

