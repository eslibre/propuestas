---
layout: 2024/post
section: proposals
category: talks
author: Kyva
title: Veenk&#58; financiación de software libre efectiva
---

# Veenk&#58; financiación de software libre efectiva

>La idea es presentar el proyecto que estoy a punto de lanzar llamado Veenk que busca crear, difundir y financiar proyectos de software libre a través de una web. Me gustaría contextualizar la presentación hablando primero de los retos a la hora de desarrollar software libre y poder encontrar una financiación sostenible con la información que he ido descubriendo al crear Veenk.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla larga / Presencial
-   Idioma: Español

-   Descripción:

> Me gustaría empezar la charla hablando del estado actual de la industria y el desarrollo de software con la creciente popularidad de las startups y su modelo de ser gratuitas para canibalizar el mercado y que además alardean de grandes valores como ser open-source, sostenibles, cuidar el medio ambiente, etc. Creo que esto es interesante ya que han logrado camuflarse y muchas veces se hacen pasar por software libre degenerando la propia definición. Haré mención de otros problemas que presentan como la invasión de la privacidad.<br><br>
Una vez esté claro que es, y que no es, el software libre y su importancia hablaré de los retos que afrontan los desarrolladores de estos proyectos como la organización, crear comunidad y lograr financiación entre otras. Profundizando en la financiación hablaré de las distintas alternativas, herramientas, retos y las implicaciones ética que puedan tener.<br><br>
Ligando con lo anterior presentaré Veenk y como este puede ser de interés y utilidad para todo lo anteriormente mencionado.<br><br>
Actualmente trabajo solo en Veenk con la esperanza de que sea un proyecto cooperativo donde más gente quiera formar parte y el plan es crear una asociación sin ánimo de lucro que sirva de soporte legal para poder recolectar donaciones para los proyectos de código libre en vez de dar comisiones a empresas como Paypal, Patreon, Ko-fi, etc.<br><br>
Veenk cuenta con una web donde les usuaries pueden encontrar alternativas libres a populares aplicaciones propietarias. Esta web también recopila información de interés para desarrolladores, mecenas y traductores que busquen colaborar.<br><br>
Y eso es todo lo que me gustaría mencionar en la charla, cuando haga las pruebas veré que tiempo me da para profundizar en cada aspecto.

-   Web del proyecto: <https://web.veenk.org>

-   Público objetivo:

>Creo que la charla es interesante para cualquier perfil que pueda asistir al evento, personas interesadas en el software libre, y en especial desarrolladores que quieran hacer una vida desarrollando software libre con un modelo de financiación ético.<br><br>
La primera parte de la charla sobre las startups, financiación ética y la importancia de las donaciones es de vital interés para toda la población.<br><br>
La segunda parte sobre Veenk es de igual interés para cualquiera ya que el proyecto busca ser útil para que usuarios no técnicos encuentren alternativas libres para usar o donar.

## Ponente:

-   Nombre: Kyva

-   Bio:

> Mi nombre es Kyva y tengo 27 años, vivo en Murcia y haría la charla yo solo.
Soy desarrollador web y activista digital. Utilizo y difundo alternativas de software libre y la importancia de las mismas.
También he creado y administro 3 instancias del fediverso (Peertube, Pixelfed, Firefish).<br><br>
En el pasado fui emprendedor lo cual dice de mi 2 cosas, que sé he podido practicar presentando ideas a una sala aunque tenga un poco de miedo escénico y que sé como funciona el mundillo de vender software para mejorar el mundo cuando el interés real es hacerse rico por lo que puedo exponer sus malas prácticas :)<br><br>
Sería mi primera charla en un evento de este estilo.<br><br>
PD: soy anarquista y vegano que no viene mucho al caso pero permea en todo lo que hago.

### Info personal:

-   Mastodon (u otras redes sociales libres): <https://meetiko.org/@kyva>
-   GitLab (u otra forja) o portfolio general: <https://codeberg.org/kyva>

## Comentarios

>He puesto la web del proyecto que tenía pensado lanzar este mes aunque probablemente se retrase así que dependiendo de cuando se mire puede no estar disponible.

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso

