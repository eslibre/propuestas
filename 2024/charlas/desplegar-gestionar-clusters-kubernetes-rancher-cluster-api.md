---
layout: 2024/post
section: proposals
category: talks
author: Andres Valero
title: Desplegar y gestionar clusters de Kubernetes con Rancher y Cluster API
---

# Desplegar y gestionar clusters de Kubernetes con Rancher y Cluster API

>En esta charla mostraremos cómo utilizar Cluster API desde Rancher para gestionar el ciclo de vida de clusters de Kubernetes, las aplicaciones desplegadas y monitorizar los clusters. Todo de una forma sencilla y siguiendo los estándares de la CNCF.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla larga / Presencial
-   Idioma: Español

-   Descripción:

> En el complejo mundo de Kubernetes y de DevOps gestionar clusters a escala, aplicaciones y todo lo relacionado con Kubernetes es extremadamente complejo. EL Open Source Software presenta multiples soluciones y proyectos que ayudan como este caso de uso, pero pocas tan completas como Rancher. Al mismo Tiempo el proyecto Cluster API de la CNCF pretende estandarizar la manera en que se gestiona el ciclo de vida de los clusters de Kubernetes,

-   Web del proyecto:
    + <https://github.com/kubernetes-sigs/cluster-api>
    + <https://github.com/rancher/rancher><br><br>

-   Público objetivo:

>Equipos de Operaciones, DevOps, desarrolladores, entusiastas de Kubernetes y del open source.

## Ponente:

-   Nombre: Andres Valero

-   Bio:

> Andrés siempre ha tenido una pasión profunda por el software de código abierto. A pesar de esto, no decidió convertir su afición en una carrera profesional hasta hace relativamente poco. Antes de empezar en el campo de las tecnologías de la información, Andrés dedicó muchos años a trabajar como vendedor. Eventualmente decidió cambiar de rumbo profesional y se formó como consultor en la industria. Desde allí, avanzó a una posición de Arquitecto de Soluciones, donde se especializó en gestión y tecnologías nativas de la nube. Actualmente, Andrés trabaja en SUSE cómo Technical Marketing Manager para tecnologías de edge y cloud-native. Además de su labor en SUSE, Andrés ha participado en numerosos eventos tecnológicos alrededor del mundo, incluyendo el Red Hat Summit, los Kubernetes Community Days en España, y el Open Infrastructure Summit 2019 en Shanghái.

### Info personal:

-   GitLab (u otra forja) o portfolio general: <https://github.com/avaleror>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso

