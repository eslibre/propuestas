---
layout: 2024/post
section: proposals
category: talks
author: Jose Manrique Lopez de la Fuente
title: Sostenibilidad del ecosistema tecnológico&#58; el papel crítico de una OSPO
---

# Sostenibilidad del ecosistema tecnológico: el papel crítico de una OSPO

>La sostenibilidad de un ecosistema tecnológico va más allá de la financiación de proyectos o fundaciones open source. Las Open Source Program Office (OSPO) como entidades establecidas dentro de las empresas, pueden jugar un papel fundamental en promover y visibilizar la necesidad de trabajar en pro de esta sostenibilidad. Esta charla pretende ser una inspiración y punto de inicio de la discusión de cómo podemos afrontar esta tarea.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla larga / Presencial
-   Idioma: Español

-   Descripción:

>En esta charla, exploraremos el rol que juega una Oficina de Programas de Open Source (OSPO) dentro de las empresas, y cómo a partir de esa labor, ayudar a la sostenibilidad del ecosistema tecnológico de la industria en la que se enmarca la empresa:
- Presentaremos algunos desafíos a los que se enfrentan las OSPOs como: la integración de prácticas de open source en entornos corporativos, el equilibrio entre compartir innovación y mantener ventajas competitivas, y la navegación por el complejo panorama de licencias de software libre.
- También compartiremos ejemplos prácticos y discutiremos estrategias efectivas para superar estos obstáculos, incluyendo la creación de políticas de contribución, el fomento de una cultura de colaboración abierta, y el desarrollo de sólidas relaciones comunitarias.
- Y por último, presentaremos el concepto de "sostenibilidad del ecosistema tecnológico" para abrir el debate sobre las actuaciones que puede abordar una OSPO para mejorar el ecosistema tecnológico.<br><br>
Esta presentación está diseñada para ofrecer a los participantes una comprensión profunda del valor estratégico de las OSPOs y herramientas prácticas para maximizar su eficacia dentro de sus organizaciones.

-   Web del proyecto: <https://github.com/InditexTech>

-   Público objetivo:

>Responsables de OSPOs, responsables de proyectos tecnológicos en cualquier industria, miembros de la comunidad, personas emprendedoras y empresarias.

## Ponente:

-   Nombre: Jose Manrique Lopez de la Fuente

-   Bio:

>Manrique es responsable de la Open Source Office (OSO) en Inditex Tecnología Digital y un apasionado del software libre y las comunidades de desarrollo. Es ingeniero industrial, con experiencia en investigación y desarrollo en el Centro Tecnológico de Informática y Comunicaciones del Principado de Asturias (CTIC), grupos de trabajo del W3C, Ándago Engineering y Continua Health Alliance. Ex-director ejecutivo de la Asociación Española de Empresas de Código Abierto (ASOLIF), ex-consultor experto del Centro Nacional de Referencia de Código Abierto (CENATIC) y socio en Bitergia.<br><br>
Involucrado, no tanto como le gustaría, en varias comunidades relacionadas con el software libre y de código abierto, como CHAOSS(Community Health Analytics for Open Source Software) e InnerSource Commons. Ha sido reconocido en el pasado como AWS Data Hero y GitLab Community Hero.<br><br>
Puedes encontrarle en la red como @jsmanrique, y cuando no está en línea, le encanta el surf y pasar tiempo con su familia y amistades.

### Info personal:

-   Web personal: <https://jsmanrique.es>
-   Mastodon (u otras redes sociales libres): <https://mastodon.social/@jsmanrique>
-   Twitter: <https://twitter.com/jsmanrique>
-   GitLab (u otra forja) o portfolio general: <https://gitlab.com/jsmanrique>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso

