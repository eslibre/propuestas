---
layout: 2024/post
section: proposals
category: talks
author: Andros Fenollosa
title: Maza ad blocking&#58; bloqueando publicidad nativamente.
---

# Maza ad blocking&#58; bloqueando publicidad nativamente.

>He creado y manteniendo durante más de 3 años un bloqueador de publicidad minimalista, local y sencillo de utilizar. Maza ad blocking es un script en Bash que le indica a tu sistema operativo que ignore la publicidad, y en consecuencia afecta a todos tus navegadores (Chrome, Firefox...) o software instalado.<br><br>
Es compatible con MacOS, Linux, BSD y Windows Subsystem for Linux (WSL). Incluso puedes utilizarlo para crear un servidor DNS que te ayude a eliminar cualquier anuncio de cualquier dispositivo.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla corta / Presencial
-   Idioma: Español

-   Descripción:

> En la charla hablaré sobre una estrategia para bloquear la publicidad usando DNS, como puede implementarse rápidamente con Maza ad blocking, diferentes implementaciones, características, avances durante los últimos años y retos futuros.

-   Web del proyecto: <https://github.com/tanrax/maza-ad-blocking>

-   Público objetivo:

>Personas con un nivel básico en el uso de terminal que estén interesados en bloquear la publicidad.

## Ponente:

-   Nombre: Andros Fenollosa

-   Bio:

> Andros Fenollosa es un ingeniero de software con una larga experiencia que ha ejercido de CTO (The Chief Technology Officer) durante los últimos años. Compagina su tiempo con la docencia desde hace más de una década, formando a futuros desarrolladores en tecnologías Web. Por otro lado, en su tiempo libre mantiene varios proyectos Opensource (Maza ad blocking, Joplin, Django LiveView…), escribe libros técnicos, crea tutoriales en su blog y cursos gratuitos en su web, publica una Newsletter mensual llamada Infinita Recursión, está chalado por la programación funcional y siente un amor incondicional por Emacs.

### Info personal:

-   Web personal: <https://andros.dev>
-   Mastodon (u otras redes sociales libres): <https://social.andros.dev>
-   Twitter: <https://twitter.com/@androsfenollosa>
-   GitLab (u otra forja) o portfolio general: <https://git.andros.dev>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso

