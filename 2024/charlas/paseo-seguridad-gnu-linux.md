---
layout: 2024/post
section: proposals
category: talks
author: Eloy Pérez González
title: Un paseo por la seguridad de GNU/Linux
---

# Un paseo por la seguridad de GNU/Linux

>La idea es presentar todos los mecanismos de seguridad, tanto de espacio de usuario como de kernel, que intervienen cuando ejecutamos acciones en Linux, más allá de los conocidos permisos de fichero.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla larga / Presencial
-   Idioma: Español

-   Descripción:

> En la charla se tratarán varios temas:
- Autenticación
- Permisos de fichero
- ACLs de ficheros
- Permisos de filesystems
- Capabilities
- LSM (SeLinux, AppArmor, etc)
- Namespaces

>Sin entrar en mucho detalle en cada uno, la idea es mostrar la amplia variedad de mecanismos que incluye GNU/Linux para controlar la seguridad del sistema.

-   Público objetivo:

>Usuarios de Linux y containers, administradores de sistemas, y todo tipo de hackers interesados en la seguridad informática. 

## Ponente:

-   Nombre: Eloy Pérez González

-   Bio:

> Interesado y profesional de la seguridad informática, la que llevo trabajando más de 5 años. Y miembro del grupo Hackliza, que aboga por el uso del software libre. Uso Emacs.<br><br>
Sería la primera vez que presento una charla en un congreso nacional.

### Info personal:

-   Web personal: <https://eloypgz.org/>
-   Mastodon (u otras redes sociales libres): <https://defcon.social/@zer1t0>
-   Twitter: <https://twitter.com/zer1t0>
-   GitLab (u otra forja) o portfolio general: <https://gitlab.com/Zer1t0>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso

