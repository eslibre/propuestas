---
layout: 2024/post
section: proposals
category: talks
author: María del Carmen Gálvez de la Cuesta
title: Ecosistema de entornos de conocimiento abierto (ECO2)
---

# Ecosistema de entornos de conocimiento abierto (ECO2)

ECO2 es un sistema de entornos de conocimiento abierto, que tiene como finalidad impulsar la alfabetización mediática e informacional, mejorando las habilidades de los estudiantes como ciudadanos comprometidos con la sostenibilidad. El proyecto extiende el alcance del "Entorno de Conocimiento y Prácticas Educativas Abiertas", generando un Ecosistema de Entornos de Publicación de contenidos en abierto, orientado a fomentar el pensamiento crítico sobre la sostenibilidad y la acción por el clima, en la línea de la propuesta que se realiza en el Real Decreto 822/2021.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla corta / Remoto
-   Idioma: Español

-   Descripción:

> El proyecto se inserta dentro de la línea estratégica del GID COMTEDEA de la Universidad Rey Juan Carlos, de establecer espacios de aprendizaje abierto que puedan implementarse en diferentes tipos de asignaturas, dentro del Plan de Innovación Educativa de la Universidad Rey Juan Carlos. La desinformación científica es un problema creciente entre los más jóvenes, incluidos los estudiantes universitarios, y estos contenidos, vinculados con los ODS 11, 12 y 13, se integrarán en el desarrollo curricular de las asignaturas de los docentes del Grupo de Innovación Docente a través de actividades de innovación, centradas en metodologías activas, que promoverán la creación de contenido en abierto por parte de los propios estudiantes.<br><br>
La acción busca:
- Ampliar las posibilidades del CA como herramienta para la mejora de los procesos de enseñanza-aprendizaje.
- Implicar a los estudiantes con un proceso de aprendizaje significativo.
- Mejorar su percepción sobre la propia titulación y el valor de transferencia social del conocimiento adquirido.
- Generar un compendio de recursos, materiales y fichas de diseño instructivo temáticas sobre sostenibilidad y acción por el clima, bajo licencia CC4-Atribución, para que puedan usarse y reutilizarse libremente por ciudadanos, docentes y estudiantes de cualquier nivel educativo en el marco de la iniciativa ClimateWarriors.
- Contribuir, con ello, a implicar a los estudiantes en el cambio social hacia la economía circular.
- Por una parte, los docentes generan contenidos en abierto, que pueden visualizarse en Classroom Ciberimaginario (https://www.learn.ciberimaginario.es/course/view.php?id=9), y por otro, los estudiantes generan a su vez Recursos Educativos Abiertos, en cada área de conocimiento, que se ponen a disposición pública en "Climate Warriors" https://climatewarriors.eu/, un entorno construido para dar visibilidad a estos contenidos libres y abiertos.
- El proyecto está desarrollado por el Grupo de Innovación Docente Comtedea (Comunicación, Tecnologías Digitales y Educación Abierta), y participan todos sus miembros con una intensa labor de creación de contenidos e impulso de la actividad de creación de REA por parte de los estudiantes en sus propias asignaturas.<br><br>
Los miembros que desarrollan la actividad son: Mª Carmen Gálvez, Manuel Gertrudix, Mario Rajas, Mª Carmen Gertrudis, Ernesto Taborda, Hernando Gómez, José Luis Rubio, Luis Matosas, Miguel Baños, Rubén Arcos, Valeria Levratto, Alejandro Carbonell, Jennifer García, Begoña Rivas, Juan Romero y Sergio Álvarez.

-   Web del proyecto: <https://ciberimaginario.es/project/proyecto-innovacion-2023-ecosistemas-de-conocimiento-abierto-eco2/>

-   Público objetivo:

>El público objetivo, es por una parte, el profesorado universitario interesado en los contenidos abiertos que se ponen a su disposición para reutilización en el aula; por otra, los estudiantes universitarios que también pueden estar interesados en ellos para ampliar su formación; e igualmente tanto docentes comos estudiantes de Educación Primaria y Educación Secundaria, que pueden utilizar los Recursos Abiertos creados por los estudiantes universitarios, para su propia formación en alfabetización mediática e informacional.

## Ponente:

-   Nombre: María del Carmen Gálvez de la Cuesta

-   Bio:

> Doctora en Ciencias de la Información por la Universidad Complutense. DEA (Diploma de Estudios Avanzados) en Comunicación Audiovisual y Publicidad (UCM). Licenciada en Geografía e Historia (UCM). Premio Extraordinario de Doctorado 2015-16 (Universidad Complutense - Facultad de CC. de la Información). Profesora Contratada Doctora. Profesora del Máster Oficial en Periodismo Digital y en el Máster Oficial en Competencia Digital y Pensamiento Computacional, ambos en la URJC. Evaluadora Experta en la Agencia Estatal de Investigación. Ha sido Coordinadora Académica de Innovación en el Centro de Innovación Docente y Educación Digital; Personal Investigador del Grupo CIBERIMAGINARIO, Investigadora del Centro Español de Subtitulado y Audiodescripción de la Universidad Carlos III;  Coordinadora de Proyectos del Centro Nacional de Información y Comunicación Educativa del Ministerio de Educación y Ciencia (actual INTEF). Es miembro del grupo de investigación CIBERIMAGINARIO y del Grupo de Innovación Docente COMTEDEA.  En la actualidad sus intereses y producción académica se centran en el ámbito de la Comunicación Digital, los Contenidos en Abierto, la Comunicación Científica, la Alfabetización Mediática y la Competencia Digital en el uso de las tecnologías en los procesos de enseñanza-aprendizaje. Es Editora Adjunta de la revista científica ICONO14.

### Info personal:

-   Web personal: <https://climatewarriors.eu/>
-   Twitter: <https://twitter.com/@ciberimaginario>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso

