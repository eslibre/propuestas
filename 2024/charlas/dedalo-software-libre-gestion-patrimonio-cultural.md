---
layout: 2024/post
section: proposals
category: talks
author: Alejandro Peña Carbonell, Juan Francisco Onielfa Veneros
title: Dédalo&#58; software libre para la gestión de Patrimonio Cultural
---

# Dédalo: software libre para la gestión de Patrimonio Cultural

>Dédalo es un proyecto tecnológico centrado en el campo de las humanidades digitales, en la necesidad de analizar y salvaguardar el Patrimonio Cultural con herramientas digitales, permitiendo a las máquinas comprender los procesos culturales, sociales e históricos asociados al Patrimonio Cultural y la Memoria.<br><br>
Cada institución ha creado sus propias soluciones a medida, que normalmente no son reutilizables en otras instituciones y, sólo en muy pocos casos son de uso libre, por lo que las instituciones modestas o los investigadores no tienen acceso a ellas.<br><br>
Pensamos que la importancia de nuestro pasado y de nuestro legado cultural y memoria es tan grande que no debe dejarse en manos de empresas privadas con intereses exclusivamente económicos.
Creemos que es tarea de todos conservarlo y difundirlo, tanto para las generaciones actuales como para las futuras.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla larga / Presencial
-   Idioma: Español

-   Descripción:

> La charla versará sobre la historia del proyecto, que nace en Valencia en 1998, su filosofía y su evolución para abordar las problemáticas en la gestión del Patrimonio Cultural y la Memoria. También se tratará la importancia del uso del software libre para la gestión del Patrimonio Cultural en las instituciones públicas y el panorama actual en los museos (desalentador...)<br><br>
Además de comentar las soluciones tecnológicas que hemos estado desarrollando e implementando a lo largo de los 25 años de historia del proyecto.<br><br>
También se abordará la situación actual de Dédalo, su comunidad y los retos y necesidades que nos plantea la incorporación de tecnologías como la inteligencia artificial a los catálogos y archivos o la interconexión de datos entre proyectos de temática cultural y memorialista.

-   Web del proyecto: <https://dedalo.dev>

-   Público objetivo:

>- Desarolladores.
- Público general.
- Gestores de Patrimonio Cultural o de humanidades.
- Cualquier persona interesada en el Patrimonio Cultural y la memoria.

## Ponente:

-   Nombre: Alejandro Peña Carbonell, Juan Francisco Onielfa Veneros

-   Bio:

> Alejandro Peña Carbonell y Juan Francisco Onielfa Veneros son desarrolladores de recursos tecnológicos para la gestión del Patrimonio Cultural y la Memoria Oral. Durante los últimos veinte cinco años, han estado desarrollando un software libre de gestión de Patrimonio Cultural de código abierto denominado Dédalo utilizado en proyectos como el Memorial Democrático de Cataluña, el archivo de Memoria de la Diputación de Valencia, el Instituto de Memoria de Navarra, el archivo de la invasión nazi en Grecia de la Universidad Freie de Berlín, Dédalo también se usa en el Museu de Prehistòria de València, el Museu Virtual de Quart de Poblet, el Museu de la Vida Rural, el Museu Etnológic de Barcelona o l'Etno, así mismo han participado en la Conceptualización del Censo Estatal de Víctimas de la Guerra y la Dictadura para el Dirección General de Memoria Democrática del Ministerio de la Presidencia, Relaciones con las Cortes y Memoria Democrática, o en el proyecto Moneda Ibérica, dentro del proyecto ARCH en colaboración con la Universidad de Oxford y la Biblioteca Nacional de Francia.

### Info personal:

-   GitLab (u otra forja) o portfolio general: <https://github.com/renderpci/dedalo>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso

