---
layout: 2024/post
section: proposals
category: talks
author: José Miguel Castillo García
title: Como dar una segunda vida a hardware "para desguace" con software libre en 2024
---

# Como dar una segunda vida a hardware "para desguace" con software libre en 2024

>Desde la Oficina de Software Libre de la Universidad de Granada realizamos una labor de recogida, puesta en marcha y reutilización de hardware anticuado, descartado y prácticamente "para desguace" de toda la Universidad. Con esta labor, logramos dar una segunda vida a equipos informáticos y los donamos a asociaciones sin ánimo de lucro, colegios e institutos, para su uso diario en tareas de ofimática o docencia.<br><br>
La charla que propongo tiene como objetivo dar a conocer las diferentes alternativas en materia de Software Libre (sistemas operativos y software generalista) que actualmente hay disponibles para este tipo de equipos, que pueden ir desde un Pentium 4 a un i3 de 3ª o 4ª generación, qué usar y qué problemas nos podemos encontrar con hardware tan antiguo.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla corta / Presencial
-   Idioma: Español

-   Descripción:

> Desde la Oficina de Software Libre de la Universidad de Granada realizamos una labor de recogida, puesta en marcha y reutilización de hardware anticuado, descartado y prácticamente "para desguace" de toda la Universidad. Con esta labor, logramos dar una segunda vida a equipos informáticos y los donamos a asociaciones sin ánimo de lucro, colegios e institutos, para su uso diario en tareas de ofimática o docencia.<br><br>
La charla que propongo tiene como objetivo dar a conocer las diferentes alternativas en materia de Software Libre (sistemas operativos y software generalista) que actualmente hay disponibles para este tipo de equipos, que pueden ir desde un Pentium 4 a un i3 de 3ª o 4ª generación, qué usar y qué problemas nos podemos encontrar con hardware tan antiguo.

-   Web del proyecto: <https://osl.ugr.es>

-   Público objetivo:

>Personal del IT con pocos recursos, administradores de centros públicos o cualquier persona interesada en el proceso de reciclado y reutilización de hardware en su entorno.

## Ponente:

-   Nombre: José Miguel Castillo García

-   Bio:

> Técnico de apoyo en la Oficina de Software Libre de la Universidad de Granada, informático por vocación y formación, maker, recopilador de hardware extraño y aprietatornillos por afición.<br><br>
Soy el técnico de la OSL-UGR desde 2019, encargándome de las tareas de reciclaje, formación, reutilización, y dando apoyo no solo a temas de hardware sino también a la instalación, configuración y puesta en marcha de Software Libre a toda la comunidad universitaria.

### Info personal:

-   Web personal:
-   Mastodon (u otras redes sociales libres): <https://mastodon.online/@josemiguelcastillo>
-   Twitter: <https://twitter.com/josemcastilloGR>
-   GitLab (u otra forja) o portfolio general:

## Comentarios

>Me haría mucha ilusión compartir el proyecto de reciclado y reutilización que llevamos a cabo en la Oficina de Software Libre de la Universidad de Granada. Pablo García Sánchez, el director de la OSL también ha solicitado realizar una charla de otro tema diferente.

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso

