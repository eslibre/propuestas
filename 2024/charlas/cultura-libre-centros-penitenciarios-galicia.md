---
layout: 2024/post
section: proposals
category: talks
author: Rafael Rodríguez Gayoso
title: Cultura libre en los centros penitenciarios de Galicia
---

# Cultura libre en los centros penitenciarios de Galicia

>La asociación MeLiSA participa en el programa «Voluntariado dixital», que es una iniactiva puesta en marcha por la Axencia para a Modernización Tecnolóxica de Galicia (AMTEGA). Y dentro de este programa se incluye el proyecto «Software libre en los centros penitenciarios de Galicia», que tiene como objetivo principal la formación de los internos de dichos centros en todo lo relacionado con tecnologías libres.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla corta / Presencial
-   Idioma: Español

-   Descripción:

>En esta charla se hará un breve resumen de todas las actividades que se han realizado durante los últimos años en los centros penitenciarios de Galicia.<br><br>
Desde un punto de vista técnico, hemos realizado instalaciones de Wikipedia en local, de servidores de ficheros (NAS), y de repositorios de Ubuntu en la red local para poder realizar instalaciones de software. Es importante destacar que en el interior de los centros penitencarios no disponemos de conexión a Internet, por lo que todo el software necesario debe estar disponible en nuestra red local.<br><br>
También se han realizado talleres de 'podcasting', a través de los cuales se ha formado a los internos en las diferentes tareas de puesta en marcha de una sala de grabaciones, gestión de dispositivos, como micrófonos y mesas de mezcla, y su posterior edición y publicación en los medios internos disponibles.

-   Web del proyecto: <https://ceibe.melisa.gal>

-   Público objetivo:

>Toda persona interesada en conocer este proyecto social basado en la cultura y tecnologías libres.

## Ponente:

-   Nombre: Rafael Rodríguez Gayoso

-   Bio:

>Soy uno de los voluntarios de la asociación MeLiSA, que forma parte del ecosistema de software libre que tenemos en Galicia.<br><br>
Desde el año 2012 he desarrollado diferentes actividades en la Oficina de Software Libre de las Universidades Galegas, y he colaborado en la organización de diferentes congresos y charlas en nuestra comunidad.

### Info personal:

-   Twitter: <https://twitter.com/@MelisaGal1>
-   GitLab (u otra forja) o portfolio general: <https://gitlab.com/oslcixug>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso

