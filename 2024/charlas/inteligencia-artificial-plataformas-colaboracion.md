---
layout: 2024/post
section: proposals
category: talks
author: Pietro Marini
title: Inteligencia artificial en plataformas de colaboración&#58; una visión ética
---

# Inteligencia artificial en plataformas de colaboración: una visión ética

>Esta charla pretende ser una reflexión sobre algunas cuestiones relativas al desarrollo de la inteligencia artificial, que reciben en general menos atención de la opinión pública: ¿Cuanta energía necesita el entrenamiento de un modelo de IA? ¿Como se obtienen los datos de entrenamiento? ¿Las predicciones de un algoritmo son verdaderamente de buena calidad y fiables? Como ejemplo de un proyecto de software que intenta responder a las derivas del sistema puesto en marcha por las grandes tecnológicas, presentaremos las últimas innovaciones relacionadas con la IA en Nextcloud Hub, su iniciativa de calificación ética de la IA y la estrategia de desarrollo del producto que permite posicionarlo como una plataforma colaborativa que garantiza la autonomía digital.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla larga / Presencial
-   Idioma: Español

-   Descripción:

> En los últimos años hemos asistido a un crecimiento exponencial del desarrollo y el uso de las tecnologías de IA/aprendizaje automático, que ya son lo suficientemente maduras como para influir en nuestras experiencias digitales en todos los ámbitos. Una suite de colaboración representa un vasto campo de aplicación de estas técnicas: pensemos, por ejemplo, en la traducción automática de textos, los sistemas de voz a texto, el reconocimiento de objetos y/o personas en fotografías, así como los algoritmos para controlar la calidad de las videollamadas.<br><br>
En cuanto estas nuevas funcionalidades aparecen en el mercado, los usuarios empiezan a demandarlas y utilizarlas, llegando a veces a un punto en que no pueden prescindir de ellas porque las consideran esenciales para aumentar la productividad. El ejemplo que viene a la mente de todos es la IA generativa, que permite generar textos de aparente buena calidad sobre todos los dominios del conocimiento disponibles en Internet.<br><br>
También son conocidos, por otro lado, los lados oscuros de la IA que a menudo se tiende a pasar por alto: la voracidad energética de los centros de datos que realizan el entrenamiento y la inferencia; la imposibilidad para los humanos de interpretar y comprender las predicciones de las redes neuronales; la opacidad del proceso de recogida de datos de entrenamiento, a menudo antagónico con las prácticas más básicas de protección de la privacidad.<br><br>
En este contexto, Nextcloud también se ha subido a la ola de la IA respetando ciertos principios fundamentales que siempre han estado en el centro de las estrategias de desarrollo del producto, a saber, la devolución del control de los datos y los procesos al usuario, la modularidad del producto y su transparencia, garantizada por su licencia de distribución de código abierto.<br><br>
Esta charla pretende ser una reflexión sobre estas cuestiones: presentaremos las últimas innovaciones relacionadas con la IA en Nextcloud Hub, la iniciativa de calificación ética de la IA y la estrategia de desarrollo del producto que permite posicionarlo como una plataforma colaborativa que garantiza la autonomía digital.<br><br>

## Ponente:

-   Nombre: Pietro Marini

-   Bio:

> Tras licenciarse en Física en 2011 por la Universidad de Pavía, Pietro ha trabajado como desarrollador y consultor en Business Intelligence y Data Analytics para numerosas empresas en varios países europeos. En el 2021 se incorpora al equipo de ventas de Nextcloud GmbH y tiene la oportunidad de participar en su expansión comercial en los países del Sur de Europa y Latinoamerica, liderando las actividades de preventa y consultoría en la región. Desde Octubre 2023 ofrece sus servicios cómo consultor independiente, con un particular enfoque en la plataforma Nextcloud.

### Info personal:

-   Web personal: <https://rcasys.com>
-   Mastodon (u otras redes sociales libres): <https://mastodon.social/@rcasys>
-   GitLab (u otra forja) o portfolio general: <https://codeberg.org/pmarini>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso

