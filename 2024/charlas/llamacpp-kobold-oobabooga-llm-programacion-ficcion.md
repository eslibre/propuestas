---
layout: 2024/post
section: proposals
category: talks
author: jm
title: Llama.cpp, Kobold y Oobabooga&#58; modelos de lenguaje grandes locales para programacion y escribir ficcion
---

# Llama.cpp, Kobold y Oobabooga: modelos de lenguaje grandes locales para programacion y escribir ficcion
 
>Este charla explica como tener en su computadora un modelo de lenguaje grandes (LLM) localmente.
>Y lo que se puede hacer con este LLM (y lo que no se puede hacer) para la ayuda a la programacion y tambien para escribir ficcion (historias o rol).
>También se hablará de lo que significa "libre" para los LLM.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla larga / Presencial
-   Idioma: Español

-   Descripción:

> Introducción a Llama.cpp
+ Inferencia con CPU vs GPU de CUDA
+ Qué es el contexto para el token y tokenización
+ HuggingFace para encontrar los modelos
+ Cuantificación y GGUF 
+ Modelos como mixtral o Phixtral o cualquiera que sea un buen modelo una semana antes de la charla (Llama 3?)
+ ¿Qué es un "prompt"? ¿Qué es "RAG"?
+ Si es mejor hablar inglés o español a tu LLM<br><br>

>Después de la parte sobre la programación:
+ Lo que funciona y lo que no funciona (con Python, C, Go y Rust)
+ Gramática formal para restringir el resultado
+ Si es mejor escribir con lenguaje fuertemente tipado o no
+ Escribir con un LLM usando Kobold, Oobabooga y SillyTavern
+ Y por supuesto pequeñas demostraciones (lentas, la GPU de mi portátil no es muy buena).

-   Público objetivo:

>Quien tenga una GPU y curiosidad por LLMs

## Ponente:

-   Nombre: jm

-   Bio:

> Trabajo con ordenadores para intentar que hagaN lo que yo quiero que hagaN.

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso

