---
layout: 2024/post
section: proposals
category: talks
author: Eduardo Romero
title: Migasfree&#58; hazte la vida facil con Linux
---

# Migasfree&#58; hazte la vida facil con Linux

>Es un software libre que gestiona despliegues de software, manteniendo y asegurando la integridad de los sistemas informáticos. Describiremos qué hace y cómo funciona con ejemplos prácticos.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla larga / Presencial
-   Idioma: Español

-   Descripción:

>- Inventariar tus sistemas (hardware y software)
- Automatizar el despliegue de software en el momento indicado y a los ordenadores necesarios
- Detección de fallas y errores centralizados
- Configuración 0

-   Web del proyecto: <https://migasfree.org>

-   Público objetivo:

>Administradores de sistemas interesados en gestionar un parque de Escritorios y/o Servidores Linux

## Ponente:

-   Nombre: Eduardo Romero

-   Bio:

> 20 y tantos años como Administrador del sistemas Linux y Windows de escritorio y servidores en el Ayuntamiento de Zaragoza.


### Info personal:

-   Twitter: <https://twitter.com/@eduromo>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso

