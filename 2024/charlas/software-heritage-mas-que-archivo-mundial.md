---
layout: 2024/post
section: proposals
category: talks
author: Agustín Benito Bethencourt
title: Software Heritage&#58; mucho más que el gran archivo mundial de software
---

# Software Heritage&#58; mucho más que el gran archivo mundial de software

>Software Heritage es un proyecto auspiciado por la UNESCO que ya es el mayor archivo de software del mundo. Pero el proyecto va mucho más allá de una iniciativa de interés público para la conservación del software para nuestras futuras generaciones. La charla pretende describir otros aspectos menos conocidos pero igualmente relevantes de este ambicioso proyecto. 

## Detalles de la propuesta:

-   Tipo de propuesta: Charla corta / Presencial
-   Idioma: Español

-   Descripción:

> El software en forma de código fuente es un componente fundamental del patrimonio cultural de la humanidad, imprescindible para preservar nuestra herencia. Su conservación es crucial para mantener accesibles otras partes de nuestro legado cultural. Software Heritage nació como instrumento esencial para alcanzar este objetivo.<br><br>
Esta charla introductoria a Software Heritage realizará una breve descripción de este proyecto libre así como de la organización que le da cobertura legal con el fin de poner a la audiencia en contexto. Posteriormente, la charla tratará de mostrar de un modo práctico cómo utilizar este gran archivo y describirá algunos aspectos relevantes para el desarrollo del proyecto, tanto desde un punto de vista técnico, de comunidad como de ecosistema. Finalmente se hará un repaso de las últimas novedades  de Software Heritage así como de los próximos pasos. <br><br>
En definitiva, se trata de una charla introductoria a Software Heritage, muy enfocada a aspectos prácticos del proyecto.

-   Web del proyecto: <https://www.softwareheritage.org/>

-   Público objetivo:

>La charla está enfocada al público asistente a esLibre en general, especialmente a desarrolladores de software interesados en conservar su software para la posteridad o en acceder al archivo y aprovecharse así de sus innumerables ventajas, con fines científicos, industriales, educativos, culturales, etc. En una charla fundamentalmente divulgativa.

## Ponente:

-   Nombre: Agustín Benito Bethencourt

-   Bio:

> Agustín Benito Bethencourt descubrió el Software Libre durante su etapa universitaria, pero en 2003 decidió centrar su carrera como emprendedor en el Software de Código Abierto. Desde entonces, ha liderado equipos y proyectos en el diseño, desarrollo y soporte de productos basados en esta tecnología en sectores como automotriz, IoT, Edge, industrial y escritorio. Actualmente, trabaja como consultor independiente, centrado en Delivery Performance Analytics [[1](https://toscalix.com/consulting/delivery-performance-analytics/)]. Es miembro activo de KDE y Software Heritage, de este último en calidad de Embajador.  Defensor del trabajo remoto, reside gran parte del año entre Málaga y La Palma, Islas Canarias.<br><br>
[1] <https://toscalix.com/consulting/delivery-performance-analytics/><br><br>
Para más información sobre toscalix, visita <https://toscalix.com/about/>

### Info personal:

-   Web personal: <https://toscalix.com/about/>
-   Mastodon (u otras redes sociales libres): <https://mastodon.social/@toscalix>
-   Twitter: <https://twitter.com/toscalix>
-   GitLab (u otra forja) o portfolio general: <https://gitlab.com/toscalix>

## Comentarios

>Charlas previas <https://toscalix.com/blog-resources/conferences/>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso

