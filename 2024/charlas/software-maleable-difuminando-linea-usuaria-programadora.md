---
layout: 2024/post
section: proposals
category: talks
author: Sergio Martínez Portela
title: Software maleable&#58; difuminando la línea entre usuari@ y programador@
---

# Software maleable: difuminando la línea entre usuari@ y programador@

>Un software maleable es aquel que considera las modificaciones del comportamiento del programa como parte su uso habitual Un programa maleable busca permitir tanto su extensión como la modificación de sus componentes de forma sencilla y minimizar su fricción al cambio, convirtiéndose en un entorno cuy@s usuari@s toman decisiones sobre como debería funcionar este y que funciones quieren que desempeñe.<br><br>
En esta charla veremos que sistemas han explorado este enfoque en el pasado, que programas en uso actualmente están orientados a ello y cuales son algunas propuestas en desarrollo.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla larga / Presencial
-   Idioma: Español

-   Descripción:

> Exploraremos el concepto de software maleable, a través de las propuestas de comunidades como [Malleable.Systems](https://malleable.systems/) o [FutureOfCoding.org](https://futureofcoding.org/).<br><br>
Veremos como es posible desarrollar software donde se desdibujen las barreras entre quien desarrolla un programa y quien lo utiliza con proyectos siguiendo el espíritu de HyperCard (y sus sucesores en desarrollo activo) o Emacs. Lo pasaremos a la práctica con pequeñas demos de como estos entornos permiten su propia modificación y adaptación y como esto permite construir, de manera incremental, funcionalidades que no estaban presentes en el software original y que permiten solucionar casos de uso que este no contemplaba.<br><br>
Finalmente veremos otras propuestas que están siendo desarrolladas actualmente y que perspectivas ofrecen sobre el futuro del desarrollo de software.

-   Público objetivo:

>Gente interesada en:
- Iniciarse a la programación
- Desarrollo de software pensado para empoderar usuari@s
- El futuro del software
- Soberanía tecnológica

## Ponente:

-   Nombre: Sergio Martínez Portela

-   Bio:

> Entusiasta de la programación y el software libre. Comencé mi carrera profesional desarrollando sistemas de big data y machine learning para buscar más adelante, de forma personal, algún sistema de software más humano a través de propuestas como Scratch y la programación visual.<br><br>
He realizado ponencias a pequeña escala de redes distribuidas, machine learning y programación visual (hablando de mi libro^H proyecto) pero mi experiencia como ponente es con un público que podría caber en un aula de clase.<br><br>
Sobre este tema en concreto no soy ningún tipo de experto, sino que es algo que me interesa desde hace unos años (por su conexión en empoderamiento con la programación visual) y que llevo investigando específicamente desde aproximadamente un año.

### Info personal:

-   Web personal: <https://codigoparallevar.com/>
-   Mastodon (u otras redes sociales libres): <https://social.codigoparallevar.com/@kenkeiras>
-   GitLab (u otra forja) o portfolio general: <https://gitlab.com/kenkeiras>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso

