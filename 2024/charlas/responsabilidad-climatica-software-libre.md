---
layout: 2024/post
section: proposals
category: talks
author: Alien
title: Responsabilidad climática en el software libre
---

# Responsabilidad climática en el software libre

>La situación actual en el contexto tecnológico conlleva una responsabilidad climática. La comunidad de software libre comparte tanta responsabilidad como el resto de desarrolladoras y usuarias, de modo que debería trabajar y posicionarse para una transición a tecnología más sostenible. Del mismo modo, los feminismos y la accesibilidad deberían ser pilares de la comunidad para asegurar espacios seguros en el desarrollo de nuevas tecnologías libres. ¿Habría que revisar los pilares fundamentales del software libre antes de continuar mirando hacia el futuro de las comunidades que lo sostienen?

## Detalles de la propuesta:

-   Tipo de propuesta: Charla larga / Presencial
-   Idioma: Español

-   Descripción:

> Tal y como exponen colectivos como [Tu nube seca mi rio](https://tunubesecamirio.com/), el impacto de la tecnología en el clima no es banal, especialmente con el actual panorama de IA y otras tecnologías insostenibles en términos de recursos. La comunidad de software libre ha desarrollado a lo largo de los años espacios seguros donde las personas pudieran encontrar servicios empujados por diferentes colectivos e individuos independientes, sin atender necesariamente a las imposiciones de productividad del software privativo.<br><br>
Frente a una situación de crisis climática, son las comunidades de software libre las que pueden comenzar una transición al decrecimiento tecnológico responsable, para poder ofrecer una Internet sostenible y mantener esos espacios seguros en el tiempo, accesibles a todo el mundo.<br><br>
Estas dificultades se unen a la necesidad de promover espacios de desarrollo seguros para minorías, y asegurar que las siguientes generaciones de comunidades en SL son feministas y accesibles.  Existen intersecciones entre la sostenibilidad y los cuidados dentro de las comunidades de SL, tan fundamentales para sostener los cimientos de Internet como el propio software.<br><br>
A lo largo de esta propuesta me gustaría lanzar algunas propuestas y necesidades que las actuales comunidades de software libre pueden utilizar de plantilla para trabajar hacia una transición sostenible de desarrollo, además de plantear una pregunta: ¿Deberían actualizarse los principios básicos del software libre para hablar de sostenibilidad, feminismos y accesibilidad?

-   Público objetivo:

>Cualquier persona familiar o interesade en el software libre y sus cimientos. 

## Ponente:

-   Nombre: Alien

-   Bio:

> Informática especializada en ciberseguridad de profesión, estudiante de artes en la universidad Oberta de Catalunya. Presidenta cofundadora de la asociación Interferencias y colaboradora de EsLibre desde sus inicios. Tengo experiencia en el desarrollo de códigos de conductas y educación en tecnología sostenible. Algunos de mis temas principales de investigación son la permacomputación, los feminismos y el software libre.

### Info personal:

-   Web personal: <https://www.inversealien.space/>
-   Mastodon (u otras redes sociales libres): <https://mastodon.green/@alien>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso

