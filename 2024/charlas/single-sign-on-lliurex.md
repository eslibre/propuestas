---
layout: 2024/post
section: proposals
category: talks
author: Enrique M.G.
title: Single sign on en LLiureX
---

# Single sign on en LLiureX

>En esta charla vamos a explicar como hemos implementado en LliureX un single sign on para acceder a la sesion usando un usuario de un dominio y una red wifi WPA Enterprise.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla larga / Presencial
-   Idioma: Español

-   Descripción:

> En esta charla vamos a explicar como hemos implementado en LliureX un single sign on para acceder a la sesion usando un usuario de un dominio y una red wifi WPA Enterprise.
* La red WiFI en los centros educativos de la comunidad valenciana
* Los usuarios en el centro digital colaborativo
* El reto que se plantea
* La solucion
* Modificando SDDM
* Windows se la pega!

-   Web del proyecto: <https://github.com/lliurex>

-   Público objetivo:

>Administradores o programadores buscando soluciones para centros de trabajo

## Ponente:

-   Nombre: Enrique M.G.

-   Bio:

> Entusiasta del software libre, colaborador en distintos proyectos y miembro del equipo de desarrollo de Lliurex.

### Info personal:

-   GitLab (u otra forja) o portfolio general: <https://github.com/Lt-henry>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso

