---
layout: 2024/post
section: proposals
category: talks
author: Coopdevs
title: Acoords&#58; una herramienta open source para la toma colaborativa de actas
---

# Acoords Una herramienta open source para la toma colaborativa de actas

>En Coopdevs estamos desarrollando una aplicación libre para la gestión y toma colaborativa de las actas de las reuniones: Acoords.<br><br>
Con ella se podrá, colaborativamente, elaborar los órdenes del día, tomar las actas a tiempo real y llevar un seguimiento de los puntos tratados. Además, una capa de análisis de datos nos dará información sobre un montón de variables que pueden ayudarnos a engrasar nuestra organización.<br><br>
Dada la visión no neutral de la tecnología que defendemos desde Coopdevs, el proyecto tiene una apuesta clara por la co-creación con las futuras usuarias, así que: ¡Ayúdanos a construir Acoords!

## Detalles de la propuesta:

-   Tipo de propuesta: Charla corta / Presencial
-   Idioma: Español

-   Descripción:

>Somos una cooperativa de trabajo, por lo que el papel que juegan las actas en nuestro día a día es fundamental. De esta experiencia, y de la acumulación de la participación en otros espacios asamblearios, hemos detectado la necesidad de una herramienta de gestión de órdenes del día y actas.<br><br>
Podríamos haber conectado un ElasticSearch a nuestro repositorio de actas, pero hemos visto la oportunidad de hacer algo que vaya más allá y resulte útil para la comunidad.<br><br>
Para facilitar la transparencia y la participación, para eliminar -o al menos, evidenciar- los sesgos existentes en las tareas reproductivas, para generar unas actas de mayor calidad y para poder recuperar la información tratada en el pasado de manera rápida y completa, nos hemos embarcado en el proceso de creación de la app de nuestros sueños (en su versión mínimamente viable).<br><br>
Pero no sólo de nuestros sueños vive la comunidad: por eso hemos adoptado una postura de co-diseño de la aplicación y desarrollo en abierto.

-   Web del proyecto: <https://acoords.org/>

-   Público objetivo:

>Cualquier persona que participe en algún tipo de organización en la que se realicen asambleas.

## Ponente:

-   Nombre: Coopdevs

-   Bio:

>Coopdevs Treball SCCL es una cooperativa de trabajo asociado con más de 6 años de trayectoria que promueve una visión no neutra de la tecnología.<br><br>
Nuestra misión es doble: ofrecer soluciones tecnológicas libres, éticas, respetuosas con la soberanía tecnológica y conscientes del impacto ambiental a la economía social y solidaria, a la vez que organizamos y publicamos el conocimiento que vamos adquiriendo para facilitar la replicabilidad de nuestra cooperativa.<br><br>
Aplicando la lógica del software libre a la autoorganización de las trabajadoras, queremos compartir con la humanidad cómo se puede organizar una empresa de manera horizontal: consideramos igual de importante publicar nuestro código que documentar en abierto los procesos internos, para que puedan servir a otras personas y a otros proyectos.<br><br>
La intercooperación es el otro gran pilar que sostiene la apuesta de Coopdevs: no queremos ser una cooperativa enorme que abarque mil procesos; preferimos fomentar la creación de un ecosistema cooperativo en el que cubramos todas las necesidades de forma colectiva sin sacrificar la humanidad que sólo tienen las organizaciones con una escala razonable.

### Info personal:

-   Web personal: <https://coopdevs.coop>
-   GitLab (u otra forja) o portfolio general: <https://git.coopdevs.org/coopdevs>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso

