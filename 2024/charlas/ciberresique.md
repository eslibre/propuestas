---
layout: 2024/post
section: proposals
category: talks
author: Iván Sánchez Ortega
title: ¿Ciberresiqué?
---

# ¿Ciberresiqué?

>La Unión Europea está tramitando la "Ley de Ciberresiliencia", con el objetivo de la seguridad informática. Esta propuesta de ley ha sido ampliamente criticada desde múltiples sectores del Software Libre.<br><br>
Esta charla explica qué intenta la ley, qué hace, y cómo (no) se ajusta a las licencias de Software Libre.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla larga / Presencial
-   Idioma: Español

-   Descripción:

> - Vista general del objetivo de la CRA (Cyber Resilience Act)
- Imposiciones prácticas de la CRA
- Definición de "uso comercial"
- Respuestas (cartas abiertas, manifiestos, etc)
- "Libro Azul" de la UE (sobre seguridad en productos)
- Interacción entre CRA y LPI (Ley de Propiedad Intelectual)
- Cesión de la autoría; mercado vs procomún

-   Público objetivo:

>Desarrolladores en general.
InfoSec y política de licencias en particular.

## Ponente:

-   Nombre: Iván Sánchez Ortega

-   Bio:

> Yo antiguamente escribía en es.comp.os.linux y asistía a las HispaLinux. Luego me metí en OpenStreetMap. Ahora soy miembro de OSGeo y programo cosas con demasiado javascript.

### Info personal:

-   Web personal: <https://ivan.sanchezortega.es>
-   Mastodon (u otras redes sociales libres): <https://mastodon.social/@IvanSanchez>
-   GitLab (u otra forja) o portfolio general: <https://gitlab.com/IvanSanchez/>

## Comentarios

>Esto se basará en el análisis que hice en <https://ivan.sanchezortega.es/politics/2023/08/30/recomendaciones-ley-ciberresiliencia.html>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso

