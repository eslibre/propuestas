---
layout: 2024/post
section: proposals
category: talks
author: Jose Manuel Ortega
title: Herramientas de benchmarks para evaluar el rendimiento en máquinas y aplicaciones
---

# Herramientas de benchmarks para evaluar el rendimiento en máquinas y aplicaciones

>Los benchmarks son programas que permiten evaluar el rendimiento de un sistema, componente o proceso en comparación con otros sistemas similares.  Son herramientas esenciales para medir y comparar el rendimiento de hardware, software y sistemas en diferentes áreas. El objetivo es dar a conocer las principales herramientas de benchmark que disponemos hoy en día para medir el rendimiento.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla larga / Presencial
-   Idioma: Español

-   Descripción:

>Entre los puntos a tratar podemos destacar:
1. Introducción a Benchmarks: Definición y propósito de los benchmarks en la medición del rendimiento
2. Tipos de Benchmarks: Benchmarks sintéticos vs. Benchmarks del mundo real.Benchmarks específicos para CPU, memoria, almacenamiento, y gráficos
3. Selección de Benchmarks: Consideraciones al elegir benchmarks según el tipo de aplicación y los objetivos de evaluación

-   Público objetivo:

>Aquellas personas interesadas en conocer como evaluar el rendimiento de máquinas y sistemas.

## Ponente:

-   Nombre: Jose Manuel Ortega

-   Bio:

>José Manuel Ortega es ingeniero de software e investigador con interés en nuevas tecnologías, open source, seguridad y testing. En los últimos años ha mostrado interés en proyectos de innovación utilizando tecnologías Big Data utilizando lenguajes de programación como Python. Actualmente se encuentra trabajando como ingeniero de software en proyectos de investigación relacionados con Big Data, Ciberseguridad y Blockchain. Ha impartido docencia a nivel universitario y colaborado con el colegio oficial de ingenieros informáticos. También ha sido ponente en varias conferencias orientadas a desarrolladores a nivel nacional e internacional.<br><br>
Más información acerca de las conferencias impartidas y otros trabajos publicados se pueden consultar en su sitio personal: <https://josemanuelortegablog.com>

### Info personal:

-   Web personal: <https://josemanuelortegablog.com>
-   Twitter: <https://twitter.com/jmortegac>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso

