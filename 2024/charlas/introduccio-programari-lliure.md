---
layout: 2024/post
section: proposals
category: talks
author: Alex Climent
title: Introducció al Programari Lliure
---

# Introducció al Programari Lliure

>Per a una societat lliure necessitem una tecnologia que no estiga controlada pel poder. 
Si valorem la nostra llibertat, podem mantindre-la i defendre-la. Explicarem la importància del programari lliure en una comunitat amb democràcia real

## Detalles de la propuesta:

-   Tipo de propuesta: Charla larga / Presencial
-   Idioma: Valencià

-   Descripción:

>De vegades a l'hora de parlar de les tecnologies lliures es passa desapercebut el programari lliure, obviant que aquests aspectes filosòfics estan més que assolits. Per desgràcia és estrany veure gent que realment comprenga la vertadera importància d'aquesta qüestió, més enllà d'un mer avantatge vehicular o una simple manera més pràctica de compartir codi.<br><br>
La introducció al programari lliure és una exploració filosòfica del concepte de llibertat en l'entorn digital. S'intenta abordar la manera en què la dependència del programari privatiu impacta la societat limitant les llibertats individuals i col·lectives. Explorarem la importància de la comunitat en l'aprenentatge i l'organització, destacant la necessitat de col·laboració horitzontal per a un desenvolupament lliure i divers. Explicant els conceptes bàsics per poder arribar a tot el món independentment del nivell tècnic en el qual es trobe l'audiència amb un llenguatge pla i senzill.<br><br>
El concepte de llibertat és central en aquesta exposició, subratllant cóm la seua absència coarta les possibilitats de desenvolupament personal i comunitari. Posarem en relleu la importància de les quatre llibertats fonamentals associades al programari lliure: l'ús lliure, l'accés al codi font, la redistribució de còpies i la possibilitat de fer modificacions.<br><br>
A més, analitzem les pràctiques comunes associades al programari privatiu a conseqüència de la falta d'aquesta llibertat; com la censura, l'addicció generada per les característiques de disseny persuasives i l'espionatge massiu de les dades dels usuaris. Apuntem que aquestes pràctiques busquen mantenir un control sobre els individus, reduint la seua autonomia i fomentant la divisió social.<br><br>
La secció educativa destaca la importància de les escoles en promoure una cultura de compartir i col·laborar. Subratllem que el programari lliure afavoreix l'educació, ja que permet l'accés lliure al coneixement i fomenta la participació en comunitats d'aprenentatge.

-   Web del proyecto: <https://t.me/programarilliure>

-   Público objetivo:

>Gent que no coneix el concepte.

## Ponente:

-   Nombre: Alex Climent

-   Bio:

>- Activista pel programari lliure, la cultura lliure i tot allò relacionat desde els 15 anys
- Creador de contingut multimedia Creative Commons
- Cofundador del Col·lectiu Emancipació Comunitària
- Membre de l'Asociació d'usuaris de GNU/Linux de València
- Realitzant estudis superiors de Administració de Sistemes en Xarxa Valencià

### Info personal:

-   Mastodon (u otras redes sociales libres): <https://awoo.fai.st/celoman>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso

