---
layout: 2024/post
section: proposals
category: talks
author: Alexander Sander
title: CRA & PLD&#58; Liability rules and software freedom
---

# CRA & PLD Liability rules and Software Freedom

>With CRA and PLD liability rules for software have been introduced with a broad exception for Free Software. After long and intense debates individual developers and non for profit work are safeguarded. I will shed light on those new rules.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla larga / Presencial
-   Idioma: English

-   Descripción:

>On Tuesday, March 12, the two votes in the plenary of the European Parliament on the Cyber Resilience Act (CRA) and the Product Liability Directive (PLD) marked the provisional end of a long debate on the introduction of liability rules for software - with a broad exemption for Free Software.<br><br>
Already at an early stage, the FSFE argued in a hearing in the EU Parliament, for the inclusion of clear and precise exemptions for Free Software development in the legislation and for liability to be transferred to those who significantly financially benefit from it on the market.<br><br>
In the future, individual developers and non-profit development of Free Software will be exempt from the CRA and the PLD. Nevertheless, the wording in both the regulations are different and a standardisation processes and guidelines are still being drawn up.<br><br>
In this talk I will discuss what this the new regulation means for software freedom in future.

-   Web del proyecto: <http://www.fsfe.org>

-   Público objetivo:

>Everyone around Free Software.

## Ponente:

-   Nombre: Alexander Sander

-   Bio:

>Alexander has studied politics in Marburg, Germany. He has been an MEP Assistant in Brussels and the General Manager of Digitale Gesellschaft e.V. in Berlin. Furthermore he is the founder of NoPNR!, a campaign against the retention of travel data. He is an expert on digital rights and this regard also a Member of the Advisory Board of the ZMI of the Univeristy of Gießen and the Initiative gegen Totalüberwachung. He is also a member of several other associations that advocate for digital rights. Alexander consults national and European institutions, administrations and organisations, is available as an expert to the media and gives lectures and workshops at international conferences and events. 

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso

