---
layout: 2024/post
section: proposals
category: talks
author: Juanjo Salvador
title: Lemmy es el siguiente eslabón de la evolución... de las comunidades de usuarios
---

# Lemmy es el siguiente eslabón de la evolución... de las comunidades de usuarios

>Primero eran listas de correo, donde era difícil seguir el hilo, pero nos apañábamos. Luego llegaron los foros, y empezamos a enunciar leyes universales al respecto. Salas de chat, grupos de Telegram, listas de RSS... hasta que ActivityPub lo cambió todo.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla larga / Presencial
-   Idioma: Español

-   Descripción:

> Lemmy es una alternativa a Reddit, ubicada en el vasto fediverso, completamente libre y de código abierto, donde se reúnen cada día más personas para hablar sobre temas en común, en forma de agregador de noticias. Proporciona a los usuarios una forma de acceder a contenido de múltiples fuentes de forma ordenada, eficaz, y atractiva. Todo lo bueno de Reddit, pero con la posibilidad de utilizar apps de terceros.

-   Web del proyecto:

-   Público objetivo:

>Cualquier usuario de Internet con interés especial en un campo. En general, se trata de una charla abierta a todo el mundo, sin mínimo conocimiento técnico necesario.

## Ponente:

-   Nombre: Juanjo Salvador

-   Bio:

> Soy programador y usuario de Linux desde hace más de 10 años, ligado casi desde mis orígenes en este mundillo al uso de software libre. He hecho charlas anteriores sobre uso de tecnologías libres, tanto en grupos locales como en eventos más grandes.

### Info personal:

-   Web personal: <https://jsalvador.me>
-   Mastodon (u otras redes sociales libres): <https://mastodon.social/@jsalvador>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso

