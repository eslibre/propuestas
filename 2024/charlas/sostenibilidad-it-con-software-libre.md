---
layout: 2024/post
section: proposals
category: talks
author: Pau Garcia Quiles
title: Sostenibilidad IT con software libre
---

# Sostenibilidad IT con software libre

>Monitorizar y corregir el desperdicio de recursos nos hace más verdes y ahorra dinero.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla larga / Presencial
-   Idioma: Español

-   Descripción:

>Dimensionar los que requieren nuestras aplicaciones en producción es complicado, así que es típico curarse en salud reservando más de lo necesario. Este desperdicio de capacidad es evitable... si se sabe cómo.<br><br>
Kruize es una herramienta de software libre que monitoriza cargas de trabajo y hace recomendaciones concretas en base a observaciones de uso de recursos durante periodos de tiempo definidos por usuario. KEPLER realiza una función parecidad, pero para consumo de energía. La combinación de ambas forma un círculo virtuoso para aprovechar al máximo la capacidad, coste y uso de energía.

-   Web del proyecto: <https://sustainable-computing.io/> / <https://github.com/kruize/>

-   Público objetivo:

>Desarrolladores, administradores de sistemas, directores de informática, directores de sostenibilidad

## Ponente:

-   Nombre: Pau Garcia Quiles

-   Bio:

>Pau es Principal Product Manager de Red Hat Insights cost management y Red Hat Insights resource optimization en Red Hat. Ha sido Product Owner de SUSE Manager, consultor IT en banca, desarrollador y administrador de sistemas. Fue desarrollador de Debian, KDE y Uyuni, entre otros proyectos libres. Cuando no está delante del ordenador, está cuidando su plantación de hortalizas o cortando leña en el campo.

### Info personal:

-   Twitter: <https://twitter.com/pgquiles>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso

