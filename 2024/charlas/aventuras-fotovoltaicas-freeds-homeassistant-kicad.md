---
layout: 2024/post
section: proposals
category: talks
author: Iván Sánchez Ortega, César Sánchez Ortega
title: Aventuras fotovoltaicas con FreeDS, HomeAssistant y KiCad
---

# Aventuras fotovoltaicas con FreeDS, HomeAssistant y KiCad

>FreeDS es un proyecto de gestor de excedentes fotovoltaicos, que se [presentó en esLibre en 2022](https://commons.wikimedia.org/wiki/File:EsLibre_2022_P50_-_Lolo,_Pablo_ZG_-_FreeDs.webm).<br><br>
Pero esta charla es desde el punto de vista no del autor del proyecto, sino del usuario. Desde una visión general de sistemas domésticos de energía solar, pasando por la motivación de FreeDS, lo que funciona, lo que no funciona, integración con otros automatismos via HomeAssistant, hasta llegar a diseñar circuitos impresos.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla larga / Presencial
-   Idioma: Español

-   Descripción:

>El guión sería más o menos:<br>
- Placas solares fotovoltaicas
- "Aprovechamiento" de la energía, rendimiento
  - Nota sobre termosifones
- Idea: almacenar energía "excedente" como energía térmica
  - Escenarios viables para FreeDS (¿dónde es útil?)
- Funcionamiento de FreeDS
- Experiencia de "onboarding" 
  - Varias alternativas de hardware
  - Documentación
  - Adquisición de hardware
- Experiencia de uso
  - Interfaz FreeDS - inversor/contador
    - shelly, modbus, etc etc
  - Una "app" para cada cosa (shelly, inversores, freeds, etc etc)
- HomeAssistant
  - Integraciones de HomeAssistant
  - homeassistant-freeds (desarrollo, mDNS, HACS, necesidad de GitHub, etc)
    - I/O: lee de freeds y controla encendido y modo manual
  - homeassistant-raspberrypi-tm1637
  - homeassistant-sensorthings
- Deshacer el lío del manual
- Hacer "bien" la placa de circuito
  - KiCad
- Experimentos con Rust

-   Público objetivo:

>Especialmente para gente a la que le gusten las chispas:<br>
- Considerando fotovoltaico
- Conocimientos de electrónica ("nivel arduino")
- "IoT"

## Ponentes:

-   Nombre: Iván Sánchez Ortega

-   Bio:

>Yo antiguamente escribía en es.comp.os.linux y asistía a las HispaLinux. Luego me metí en OpenStreetMap. Ahora soy miembro de OSGeo y programo cosas con demasiado javascript.

-   Nombre: César Sánchez Ortega

-   Bio:

>Me hice Ingeniero de Telecomunicaciones y nunca he dejado de soldar mis propios circuitos. De día miro hojas de cálculo, y de noche sueño con automatizar las persianas.

### Info personal:

-   Web personal: <https://ivan.sanchezortega.es>
-   Mastodon (u otras redes sociales libres): <https://mastodon.social/@IvanSanchez>
-   GitLab (u otra forja) o portfolio general: <https://gitlab.com/IvanSanchez/>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso

