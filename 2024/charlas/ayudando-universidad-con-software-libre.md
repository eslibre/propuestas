---
layout: 2024/post
section: proposals
category: talks
author: Pablo García Sánchez
title: Ayudando a la universidad con el Software Libre&#58; mi experiencia en la OSL de la UGR
---

# Ayudando a la universidad con el Software Libre: mi experiencia en la OSL de la UGR

>La misión de la Oficina de Software Libre es hacer de la Universidad de Granada una universidad mejor utilizando el software libre y el conocimiento abierto.<br><br>
En esta charla os contaré el estado actual de la OSL, a qué nos dedicamos, y a qué aspiramos en un futuro.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla larga / Presencial
-   Idioma: Español

-   Descripción:

>La OSL es una entidad dependiente del Vicerrectorado de Transformación Digital de la Universidad de Granada. Desde su creación en 2008 he estado vinculado de una forma u otra a sus actividades, pero desde 2021 soy director de la misma, y he podido ver desde dentro cómo funciona la universidad y las oportunidades que puede brindar la oficina, pero también sus retos y dificultades.<br><br>
En esta charla impartiré una pequeña perspectiva histórica de la OSL, los servicios que ofrece, sus proyectos, historias de éxito y sus desafíos, esperando que sirva también como inspiración para otras universidades, pero también para debatir y encontrar puntos en las que podría mejorar.

-   Web del proyecto: <http://osl.ugr.es>

-   Público objetivo:

>Cualquier persona interesada el conocimiento abierto (no es charla técnica).

## Ponente:

-   Nombre: Pablo García Sánchez

-   Bio:

>Soy Profesor Titular de Universidad del Departamento de Ingeniería de Computadores, Automática y Robótica de la Universidad de Granada y el actual director de la Oficina de Software Libre del Vicerrectorado de Transformación Digital. He sido uno de los organizadores de la conferencia conjunta Evostar sobre Computación Evolutiva del 2014 al 2019 y de la PyConES 2022 que se celebró en Granada. Mis intereses incluyen la computación orientada a servicios, computación evolutiva, inteligencia computacional en videojuegos, los algoritmos distribuidos, el software libre y la ciencia abierta. También me gusta el cine y dibujar al aire libre. ¡Ah! y tengo una gata naranja que se llama Daisy.

### Info personal:

-   Web personal: <http://www.ugr.es/~pablogarcia>
-   Mastodon (u otras redes sociales libres): <https://mastodon.online/@fergunet>
-   Twitter: <https://twitter.com/fergunet>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso

