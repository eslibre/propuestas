---
layout: 2024/post
section: proposals
category: tables
author: Slimbook
title: Slimbook
---

## Slimbook

>Slimbook, empresa Valenciana de ordenadores con GNU/Linux, colabora desde sus inicios en 2015, con organizaciones vinculadas al software libre, como no hacían ni hacen, otras empresas.<br><br>
Y un año más, es un  placer y un honor, sumar para que la difusión del conocimiento libre, apoyando la realización de esLibre en Valencia, en el backstage, pero también exponiendo nuestros ordenadores, deleite de muchos.

-   Web: <https://slimbook.com/>
-   Mastodon (u otras redes sociales libres): <https://linuxrocks.online/@slimbook>
-   Twitter: <https://twitter.com/@slimbook>
-   GitLab (u otra forja) o portfolio general: <https://github.com/slimbook-team>

## Comentarios

>La mesa es una de las muchas cosas con las que nos gustaría estar en el evento :)

## Condiciones aceptadas

-   [x]  Aceptamos seguir el código de conducta (<https://eslib.re/conducta>) durante nuestra participación en el congreso

