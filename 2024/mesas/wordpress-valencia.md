---
layout: 2024/post
section: proposals
category: tables
author: WordPress Valencia
title: WordPress Valencia
---

## WordPress Valencia

>Un lugar de encuentro en Valencia para entusiastas de WordPress, ya sean aficionados, usuarios, desarrolladores, diseñadores, empresas, freelancers... Un lugar donde compartir y aprender.

-   Web: <https://www.meetup.com/es-ES/wordpress-valencia-meetup/>
-   Twitter: <https://twitter.com/_wpvalencia>

## Condiciones aceptadas

-   [x]  Aceptamos seguir el código de conducta (<https://eslib.re/conducta>) durante nuestra participación en el congreso

