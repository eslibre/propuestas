---
layout: 2024/post
section: proposals
category: tables
author: openSUSE
title: openSUSE
---

## openSUSE

>Linux everywhere. openSUSE creates one of the world's best Linux distributions, as well as a variety of tools, such as OBS, OpenQA, Kiwi, YaST, OSEM, working together in an open, transparent and friendly manner as part of the worldwide Free and Open Source Software community.<br><br>
The project is controlled by its community and relies on the contributions of individuals, working as testers, writers, translators, usability experts, artists and ambassadors or developers. The project embraces a wide variety of technology, people with different levels of expertise, speaking different languages and having different cultural backgrounds. 

-   Web: <http://opensuse.org>
-   Info: The openSUSE project is a worldwide effort that promotes the use of 
-   Mastodon (u otras redes sociales libres): <https://fosstodon.org/@opensuse>
-   Twitter: <https://twitter.com/opensuse>
-   GitLab (u otra forja) o portfolio general: <https://github.com/openSUSE>

## Comentarios

>We will bring stickers and some swag and be there to explain anything openSUSE to all visitors.

## Condiciones aceptadas

-   [x]  Aceptamos seguir el código de conducta (<https://eslib.re/conducta>) durante nuestra participación en el congreso

