---
layout: 2024/post
section: proposals
category: tables
author: KDE España
title: KDE España
---

## KDE España

>KDE España está formado por un grupo de desarrolladores y contribuidores al proyecto KDE con la intención de dinamizar el desarrollo y uso del entorno de escritorio KDE.<br><br>
Entre ellos podemos encontrar a gran variedad de colaboradores de KDE, incluyendo programadores, empaquetadores, documentadores, promotores, traductores, artistas, etc.<br><br>
En la actualidad lo componen unos 40 socios.

-   Web: <https://www.kde-espana.org/>
-   Mastodon (u otras redes sociales libres): <https://floss.social/@kde_espana>
-   Twitter: <https://twitter.com/KDE_Espana>
-   GitLab (u otra forja) o portfolio general: <https://invent.kde.org>

## Comentarios

>El objetivo de la mesa es mostrar el hardware que utiliza el software KDE por defecto así como las bondades de éste. De esta forma presentamos portátiles, móviles, la Steam Deck y una pantalla táctil con la aplicación Krita para que los visitantes desarrollen su parte creativa.<br><br>
Tendremos pegatinas y organizaremos interacciones con el público (por ejemplo, un concurso de dibujo con Krita)<br><br>
Además, solemos vender camisetas relacionadas con KDE de la marca Freewear.

## Condiciones aceptadas

-   [x]  Aceptamos seguir el código de conducta (<https://eslib.re/conducta>) durante nuestra participación en el congreso

