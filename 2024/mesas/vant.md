---
layout: 2024/post
section: proposals
category: tables
author: VANT
title: VANT
---

## VANT

>VANT nace hace unos años (en 2011, concretamente) después de unos años en que nuestro proyecto no tenía una identidad de marca propia.<br><br>
Con una trayectoria de años en el mundo del PC, especializados en hardware, y al mismo tiempo como usuarios de Linux a nivel personal, nos dimos cuenta de la falta de una oferta seria de ordenadores que incluyesen un sistema operativo GNU/Linux disponible nada más poner en marcha el ordenador.<br><br>
Conscientes de lo interesante de Linux en entornos de educacíón y aprendizaje, decidimos iniciar nuestra actividad en 2008 ensamblando y comercializando modelos de prestaciones modestas, precio ajustado y Ubuntu 8.04 preinstalado, configurado y con aplicaciones extra instaladas para que un usuario recién llegado a Linux pudiera descubrir todo lo que este sistema operativo ofrecía.<br><br>
Poco a poco comenzamos a aumentar nuestra gama de equipos ante solicitudes de máquinas más potentes, y es en 2011 cuando nuestra oferta ya tiene entidad de catálogo y creamos VANT.<br><br>
Es también a finales de ese mismo 2011 cuando llega nuestro primer portátil, con procesador i3 y 15.6”.<br><br>
A partir de ahí, seguimos creciendo, para inaugurar,en 2014, nuestra tienda online.<br><br>
Actualmente nuestra oferta es amplia, tanto en modelos desktop como en portátiles y con una gama de configuraciones enorme para que cada uno pueda decidir exactamente cómo desea su ordenador VANT.<br><br>
Y es que cada uno tiene unas necesidades, unas preferencias… . Por eso pretendemos poner al alcance de cualquier usuario de GNU/Linux o de cualquier persona interesada en entrar a formar parte de esta gran familia linuxera, la mayor oferta de equipos, de forma que pueda encontrar aquel que se ajuste mejor a lo que busca.<br><br>
Es la forma que hemos encontrado para formar parte de esta comunidad. Lo nuestro es el hardware, y nos encargamos de proporcionar aquél que funciona bien con diferentes distribuciones Linux, para que cada cliente disfrute de una experiencia linuxera lo más completa posible.<br><br>
Con una trayectoria de años en el mundo del PC, especializados en hardware, y al mismo tiempo como usuarios de Linux a nivel personal, nos dimos cuenta de la falta de una oferta seria de ordenadores que incluyesen un sistema operativo GNU/Linux disponible nada más poner en marcha el ordenador.<br><br>
Conscientes de lo interesante de Linux en entornos de educacíón y aprendizaje, decidimos iniciar nuestra actividad en 2008 ensamblando y comercializando modelos de prestaciones modestas, precio ajustado y Ubuntu 8.04 preinstalado, configurado y con aplicaciones extra instaladas para que un usuario recién llegado a Linux pudiera descubrir todo lo que este sistema operativo ofrecía.<br><br>
Poco a poco comenzamos a aumentar nuestra gama de equipos ante solicitudes de máquinas más potentes, y es en 2011 cuando nuestra oferta ya tiene entidad de catálogo y creamos VANT.<br><br>
Es también a finales de ese mismo 2011 cuando llega nuestro primer portátil, con procesador i3 y 15.6”.<br><br>
A partir de ahí, seguimos creciendo, para inaugurar,en 2014, nuestra tienda online.<br><br>
Actualmente nuestra oferta es amplia, tanto en modelos desktop como en portátiles y con una gama de configuraciones enorme para que cada uno pueda decidir exactamente cómo desea su ordenador VANT.<br><br>
Y es que cada uno tiene unas necesidades, unas preferencias... por eso pretendemos poner al alcance de cualquier usuario de GNU/Linux o de cualquier persona interesada en entrar a formar parte de esta gran familia linuxera, la mayor oferta de equipos, de forma que pueda encontrar aquel que se ajuste mejor a lo que busca.<br><br>
Es la forma que hemos encontrado para formar parte de esta comunidad. Lo nuestro es el hardware, y nos encargamos de proporcionar aquél que funciona bien con diferentes distribuciones Linux, para que cada cliente disfrute de una experiencia linuxera lo más completa posible.

-   Web: <https://www.vantpc.es/>
-   Twitter: <https://twitter.com/@vantpc>

## Condiciones aceptadas

-   [x]  Aceptamos seguir el código de conducta (<https://eslib.re/conducta>) durante nuestra participación en el congreso

