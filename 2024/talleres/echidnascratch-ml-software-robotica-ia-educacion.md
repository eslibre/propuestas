---
layout: 2024/post
section: proposals
category: workshops
author: Jorge Lobo, Xabier Rosas, José Pujol, Juan David Rodríguez 
title: EchidnaScratch ML&#58; Software de escritorio para robótica e IA en educación
---

# EchidnaScratch ML:Software de escritorio para robótica e IA en educación

>En este taller presentaremos la versión de escritorio de EchidnaScratch ML, un entorno de programación por bloques para trabajar la robótica educativa y el aprendizaje automático en Educación Primaria y Secundaria.<br><br>
Los participantes podrán explorar la conexión entre el mundo físico y el digital, a través de distintas las diferentes prácticas propuestas.

## Detalles de la propuesta:

-   Tipo de propuesta: Taller / Presencial
-   Idioma: Español

-   Descripción:

> En este taller exploraremos la conexión entre el mundo físico y el digital mediante EchidnaScratch ML, un entorno de programación por bloques para trabajar Programación, robótica e inteligencia artificial y la placa microcontroladora Echidna Black. <br><br>
Este taller será la presentación de la versión de escritorio de EchidnaScratch ML, una versión más accesible al poder instalarse en tu equipo y no depender de la conexión a internet. Y, por supuesto, con licencia libre :-)<br><br>
A través de varias actividades conoceremos el funcionamiento de algunos de los sensores y actuadores de la placa EchidnaBlack, controlaremos una torreta impresa en 3D con dos servomotores y crearemos un proyecto de machine learning, una de las disciplinas de la inteligencia artificial.

-   Web del proyecto: <https://echidna.es/>

-   Público objetivo:

>Cualquier persona interesada en la programación y la robótica, puede resultar especialmente interesante a docentes. No se requiere ningún conocimiento previo, pero resultará más fácil si se tiene alguna experiencia con entornos gráficos de programación, como Scratch o Snap. Serán bienvenidas niñas y niños a partir de 8 años acompañados por un adulto. 


## Ponente:

-   Nombre: Jorge Lobo

-   Bio:

>Maestro de Educación Primaria. Lleva años introduciendo programación robótica educativa, IA e impresión 3D en Primaria. Imparte cursos y formación sobre robótica educativa e IA a docentes. Participa en los proyectos Open Hardware Escornabot y Echidna STEAM.

-   Nombre: Xabier Rosas

-   Bio:

>Profesor de Ciclos Formativos Electrónica. Xabier cuenta con un amplio bagaje en el mundo del open source ha desarrollado el proyecto Escornabot y es el responsable del desarrollo electrónico en Echidna. Imparte formación sobre robótica educativa.

-   Nombre: Jose Pujol

-   Bio:

>Profesor de Tecnología Educación Secundaria.  Jose lleva introduciendo programación y robótica en sus clases usando hardware libre desde el 2006. Desarrolló el proyecto Kiwibot. Imparte formación sobre robótica educativa.

-   Nombre: Juan David Rodríguez

-   Bio:

>Asesor Técnico Docente en el Instituto Nacional de Tecnologías Educativas y Formación del Profesorado donde desempeña tareas de administración de sistemas y control del ciclo de vida del software. Ha desarrollado herramientas como Learning ML y EchidnaScratch ML para trabajar el pensamiento computacional a través de actividades prácticas con inteligencia artificial.


### Info personal:

-   Twitter: <https://twitter.com/@echidnaSteam>
-   GitLab (u otra forja) o portfolio general: <https://github.com/EchidnaShield>

## Comentarios

>- Requerimientos técnicos: proyector, tomas de corriente para ordenadores (nuestros y de asistentes), conexión a Internet
- Material necesario: los asistentes deben llevar al menos un ordenador por pareja con el IDE de Arduino y EchidnaScratch ML instalados
- Recursos:
    + Arduino: <https://www.arduino.cc/en/Main/Software>
    + EchidnaScratch: (se facilitará cuando el software esté disponible en la web de Echidna)

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso

