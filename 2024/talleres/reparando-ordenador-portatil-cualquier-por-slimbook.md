---
layout: 2024/post
section: proposals
category: workshops
author: Alejandro López
title: Reparando tu ordenador portátil de cualquier marca por Slimbook
---

# Reparando tu ordenador portátil de cualquier marca por Slimbook

>Cómo reparar tu ordenador portátil, hacerle un mantenimiento, o cambiarle la pasta térmica es algo básico, que debería hacerse como se hace en los coches, y ayuda a alargar alargar la vida de nuestros ordenadores.

## Detalles de la propuesta:

-   Tipo de propuesta: Taller / Presencial
-   Idioma: Español

-   Descripción:

> En este taller aprenderemos a abrir y desmontar un ordenador portátil. Usaremos algunas unidades de SLIMBOOK, pero la teoría es muy similar en otras marcas, por lo que, si lo deseas, traete tu ordenador y te ayudaremos.<br><br>
Inicialmente, te contaremos que tipo de incidencias suele tramitar nuestro servicio técnico, etc.<br><br>
Y además, llevaremos equipamiento, como destornilladores, paletillas, pinzas, y pasta térmica, para el que se atreva a cambiarla!

-   Web del proyecto: <https://slimbook.com/>

-   Público objetivo:

>Cualquiera

## Ponente:

-   Nombre: Alejandro López

-   Bio:

> Fundador en 2015 de Slimbook, aunque usuario de Debian desde 2003, Alejandro es un programador que se adentró en los caminos del pingûino, y apostó por proveer de hardware compatible y de alta calidad, a nuestras queridas distribuciones, olvidadas por las grandes marcas.

### Info personal:

-   Mastodon (u otras redes sociales libres): <https://linuxrocks.online/@slimbook>
-   Twitter: <https://twitter.com/@slimbook>
-   GitLab (u otra forja) o portfolio general: <https://github.com/Slimbook-Team>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso

