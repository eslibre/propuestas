---
layout: 2024/post
section: proposals
category: workshops
author: Thais Pousada
title: Recursos tecnológicos para la comunicación y participación de las personas con discapacidad 
---

# Recursos tecnológicos para la comunicación y participación de las personas con discapacidad 

>Este taller se plantea para acercar el conocimiento de diferentes recursos de tecnología libre para facilitar los procesos de comunicación de personas con discapacidad. Durante el mismo, se mostrarán diferentes herramientas gratuitas con las que generar soluciones válidas y adecuadas para mejorar la comunicación y participación de este colectivo.

## Detalles de la propuesta:

-   Tipo de propuesta: Taller / Presencial
-   Idioma: Español

-   Descripción:

>En este taller se abordarán diferentes aplicaciones y programas libres que permiten mejorar las posibilidades de comunicación de aquellas personas que, por su situación de salud, encuentran algunas dificultades para ello. La tecnología debe apoyar y ayudar a todas las personas, siendo, en este caso un recurso facilitador de los procesos de comunicación.<br><br>
Las asistentes conocerán diferentes herramientas gratuitas con las que confeccionar tableros de comunicación, agendas virtuales, secuencias de actividades y con las que mejorar el acceso a diferentes dispositivos. Todo ello, con el objetivo de crear recursos válidos y adecuados para mejorar la participación y la inclusión de las personas con discapacidad, empleando la tecnología por el bien común.


-   Público objetivo:

>Personas interesadas en el tema. Profesionales de la educación y del ámbito sociosanitario que quieran actualizar sus conocimientos sobre la propuesta.

## Ponente:

-   Nombre: Thais Pousada
-   Bio: 

>Doctora en ciencias de la salud, terapeuta ocupacional y enfermera.<br><br>
Profesora Titular de Universidad del Departamento de Ciencias de la Salud, de la Universidade da Coruña (UDC). Miembro del grupo de investigación TALIONIS (https://talionis.citic.udc.es/) y del Centro de Investigación en las TIC (CITIC).<br><br>
Es colaboradora con diferentes organizaciones sin ánimo de lucro, entre las que destacan la Federación ASEM y la Federación Española de Enfermedades Raras. Sus líneas de investigación están relacionadas con la tecnología y productos de apoyo, especialmente con el diseño de soluciones en 3D y aplicación de realidad virtual.

### Info personal:

-   Web personal: <https://www.linkedin.com/in/thais-pousada-garc%C3%ADa-12092724/>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso

