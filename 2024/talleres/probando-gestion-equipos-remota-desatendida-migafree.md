---
layout: 2024/post
section: proposals
category: workshops
author: Equipo Vitalinux
title: Probando la gestión de equipos y servidores de forma remota y desatendida con Migasfree
---

# Probando la gestión de equipos y servidores de forma remota y desatendida con Migasfree

>Taller que pretende presentar la herramienta de Migasfree de forma práctica.  Probaremos las funcionalidades más interesantes en directo como la instalación y desinstalación de software, inventario de software y hardware, reporte y solución de errores, políticas...dentro de un contexto real en producción basado en Vitalinux (distribución Linux oficial para educación del Gobierno de Aragón).<br><br>
¿Necesitas las funcionalidades de Active Directory para Linux? Tienes algo mejor y libre: Migasfree.

## Detalles de la propuesta:

-   Tipo de propuesta: Taller / Presencial
-   Idioma: Español

-   Descripción:

>Durante el taller se llevarán al cabo las siguientes actuaciones:
1. Desplegaremos un servidor migasfree en producción (con datos reales), explicando su contexto como ejemplo de uso aplicable a otras realidades
2. Configuración de Dominios de gestión para cada participante
3. Instalación y configuración de clientes
4. Paquetizar soluciones a implementar en formato deb para configuración de los clientes. 
5. Prueba de las diferentes funcionalidades y particularidades en cliente y servidor: despliegue de software y configuraciones basado en políticas, informes, gestión de errores...

-   Web del proyecto: <https://docs.vitalinux.educa.aragon.es>

-   Público objetivo:

>Administradores o personas interesadas en la gestión de equipos informáticos

## Ponente:

-   Nombre: Equipo Vitalinux

-   Bio:

>Técnicos informáticos del Proyecto Vitalinux dentro de CATEDU (Centro Aragonés de Tecnologías para la Educación) del Gobieno de Aragón. Comunidad Anfitriona de esLibre 2023.

### Info personal:

-   Web personal: <https://docs.vitalinux.educa.aragon.es>
-   GitLab (u otra forja) o portfolio general: <https://gitlab.vitalinux.educa.aragon.es>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso

