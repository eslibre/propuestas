---
layout: 2024/post
section: proposals
category: workshops
author: Pablo Ridolfi
title: Obtén el máximo rendimiento de tu ESP32 desarrolla aplicaciones en lenguaje C con el framework ESP-IDF
---

# Obtén el máximo rendimiento de tu ESP32 desarrolla aplicaciones en lenguaje C con el framework ESP-IDF

>La familia de microcontroladores ESP32 se ha vuelto un estándar de facto para el desarrollo de hardware IoT gracias a sus prestaciones de conectividad y su compatibilidad con plataformas como Arduino y Micropython. En esta presentación abordaremos aspectos básicos del Espressif IoT Development Framework (ESP-IDF) que nos permitirá desarrollar aplicaciones en lenguaje C buscando obtener el mayor rendimiento de esta plataforma de hardware. Finalmente realizaremos un ejercicio de conectividad SSL desde un dispositivo basado en ESP32 hacia un servidor en la nube.

## Detalles de la propuesta:

-   Tipo de propuesta: Taller / Presencial
-   Idioma: Español

-   Descripción:

>Este taller propone introducir a los asistentes al software y hardware libre que es posible utilizar hoy en día para implementar sistemas embebidos. Dentro del abanico de plataformas disponibles hoy en día, encuentro a ESP32 particularmente interesante debido a que basan sus herramientas mayoritariamente en software libre, así como sus últimos modelos de microcontroladores basados en la arquitectura abierta RISC-V.

-   Web del proyecto: <https://gitlab.com/pridolfi/esp-idf-tutorial>

-   Público objetivo:

>Personas con conocimientos básicos de programación y sistemas GNU/Linux podrán seguir el taller sin problemas. Para sacar máximo provecho al taller se recomienda asistir con una PC con sistema operativo Ubuntu o similar instalado.

## Ponente:

-   Nombre: Pablo Ridolfi

-   Bio:

>Pablo Ridolfi es ingeniero en electrónica (Universidad Tecnológica Nacional) y magíster en sistemas embebidos (Universidad de Buenos Aires). Diseña, implementa y pone en marcha sistemas electrónicos para la industria desde hace más de dieciocho años. Actualmente trabaja como Senior Software Quality Engineer en Red Hat, donde es responsable por el aseguramiento de la calidad y la seguridad funcional de los componentes de arquitectura de hardware del kernel Linux para aplicaciones Real-Time. Fue cofundador del proyecto de hardware y software libre Computadora Industrial Abierta Argentina (<www.proyecto-ciaa.com.ar>).<br><br>
Ha sido ponente de diversas charlas sobre hardware y software libre en conferencias como FLISoL (Festival Latinoamericano de Instalación de Software Libre), SASE (Simposio Argentino de Sistemas Embebidos) y PyCon.

### Info personal:

-   Twitter: <https://twitter.com/pabloridolfi>
-   GitLab (u otra forja) o portfolio general: <https://www.github.com/pridolfi>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso

