---
layout: 2024/post
section: proposals
category: workshops
author: Gabriele Ponzo
title: Crear plantillas de documentos profesionales con LibreOffice Writer
---

# Crear plantillas de documentos profesionales con LibreOffice Writer

>Las plantillas de documentos son esenciales para una buena organización y una imagen uniforme de una empresa o entidad.

## Detalles de la propuesta:

-   Tipo de propuesta: Taller / Presencial
-   Idioma: Español

-   Descripción:

>Veremos cómo crear plantillas profesionales, automatizadas y protegidas frente a modificaciones inexpertas. Enseñaré lo que NO hay que hacer con Writer y lo que, en cambio, hace que un documento esté bien formado y estructurado.

-   Web del proyecto: <https://es.libreoffice.org>

-   Público objetivo:

>Difundir nociones importantes sobre cómo utilizar correctamente el tratamiento de textos y evitar errores generalizados.

## Ponente:

-   Nombre: Gabriele Ponzo

-   Bio:

>Un usuario pionero desde los días de StarOffice, se unió a Progetto Linguistico Italiano OpenOffice durante la Conferencia de 2009 en Orvieto, y ha formado parte de la comunidad OpenOffice.org hasta el nacimiento de LibreOffice. La formación y el soporte son sus actividades más habituales y ha estado trabajando en los proyectos LibreUmbria y LibreDifesa como profesor.<br><br>
Es uno de los fundadores de LibreItalia, donde también es parte de la Junta Directiva. Sus contribuciones son sobre todo hablar en conferencias, algunas traducciones, y el apoyo a los usuarios en las listas de correo italiano e internacional, Formador certificado y experto en migración desde febrero de 2015.

### Info personal:

-   Mastodon (u otras redes sociales libres): <https://mastodon.uno/@gippy>
-   Twitter: <https://twitter.com/@PonzoGabriele>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso

