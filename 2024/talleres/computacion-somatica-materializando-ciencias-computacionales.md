---
layout: 2024/post
section: proposals
category: workshops
author: compudanzas (sejo y mel*)
title: Computación somática&#58; materializando ciencias computacionales en cuerpxs humanxs y coreografía
---

# computación somática: materializando ciencias computacionales en cuerpxs humanxs y coreografía

>¿Qué es una computadora? ¿Cómo es tan poderosa y hace lo que hace, si consiste en piezas de minerales ordenados?<br><br>
¿Qué pasa si la alentamos? ¿Y si incrementamos su tamaño? ¿Y si le quitamos todo rastro de eficiencia para que la podamos bailar?<br><br>
En este taller nos insertaremos entre el mundo abstracto de las ciencias computacionales y el mundo de su materialización industrial, para darle cuerpx humanx, social, coreográfico, a algunos de sus conceptos fundamentales.<br><br>
Lo que queremos es construir y programar computadoras relativamente lentas, grandes e ineficientes, a partir de personas en movimiento siguiendo y jugando instrucciones. Así, nos podremos reapropiar de las maneras computacionales de entender el mundo, para usarlas a nuestro gusto con el fin de proponer otras posibles computadoras, danzas, y/o formas de vida.

## Detalles de la propuesta:

-   Tipo de propuesta: Taller / Presencial
-   Idioma: Español

-   Descripción:

> El taller consiste en una sesión guiada de movimiento y juego en la que pasaremos por varias etapas para conectar con nuestrxs cuerpxs y visibilizar procesos e ideas que rigen las realidades digitales actuales. El propósito es instigar e investigar la reapropriación de dichos conceptos computacionales con el objetivo de imaginar y realizar mundos donde estos sirven a intereses alternos, como los de la danza, la vida y/o la liberación.<br><br>
Dado lo tradicionalmente abstracto y “cerebral” de las ciencias computacionales, ponemos mucho énfasis en (re)visitarlas como cuerpxs multidimensionales que somos.<br><br>
Las etapas del taller son: sensibilización inicial y calentamiento, movimiento discreto, introducción a la técnica qiudanz de manipulación computacional de secuencias de movimiento, exploración de conceptos computacionales a través de actividades y juegos basados en la técnica, y un espacio para probar ideas de quienes participen.<br><br>
Los conceptos computacionales a cuerpear: estados finitos y tiempos discontinuos, operaciones lógicas, memoria digital, datos e instrucciones, y algunas máquinas computacionales abstractas.<br><br>
La duración del taller se puede adaptar entre 90 y 120 minutos, de acuerdo a las necesidades del comité organizador.

-   Web del proyecto: <https://compudanzas.net/>

-   Público objetivo:

>Dadas las crisis climáticas, ecológicas y sociales actuales, consideramos que el taller es un importante ejercicio de imaginación donde nos preguntamos: ¿Qué pasaría si las computadoras fueran bailes y no cajas de metal y semiconductor?<br><br>
El taller está dirigido a quienes les llame la atención esta pregunta, ya sean profesionales de la informática, practicantes de disciplinas de movimiento, artistas, filósofxs o público en general.<br><br>
No es necesario tener experiencia previa con actividades basadas en movimiento. Además, todo lo que hagamos está diseñado para adaptarse a diferentes grados de movilidad.

## Ponente:

-   Nombre: compudanzas (sejo y mel*)

-   Bio:

> compudanzas es un proyecto creativo de investigación que explora formas alternativas de aprender y hacer cómputo.<br><br>
Tratamos de transicionar desde una lógica de productividad y eficiencia, y circuitos que destruyen vida, hacia danzas, rituales, y otros tipos de computadoras aparentemente inútiles.<br><br>
Nos movemos con calma, paciencia y curiosidad.<br><br> 
Originaries de la Ciudad de México, ahora residimos en la Comunidad de Madrid.

### Info personal:

-   Web personal: <https://compudanzas.net/>
-   Mastodon (u otras redes sociales libres): <https://post.lurk.org/@compudanzas>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso

