---
layout: 2024/post
section: proposals
category: workshops
author: Reynaldo Cordero Corro
title: Taller de kataficación de código 100% software libre
---

# Taller de kataficación de código 100% software libre

>Aprender a programar sin clases ni manuales, como se aprende a hablar sin ir a la escuela: simplemente por exposición, gracias a la elegancia, riqueza expresiva y sencillez del lenguaje de programación Haskell. 

## Detalles de la propuesta:

-   Tipo de propuesta: Taller / Presencial
-   Idioma: Español

-   Descripción:

> Los participantes necesitan unicamente un portátil con VirtualBox instalado para arrancar una máquina virtual de libre copia GNU/Trisquel (una distribución de Linux gallega) donde vendrá ya preinstalado el entorno de programación HaskellKatas.<br><br>
Estará inspirado en este primer taller que se dio en la Universidad de Alcalá el 15 de enero: <https://www.linkedin.com/posts/reynaldo-cordero-7723031_haskellkatasen-activity-7155599067203461120-x39i>

-   Web del proyecto: <https://gitlab.com/HaskellKatas/katas--proof-of-concept>

-   Público objetivo:

>El taller es para todos los públicos y niveles. A este tipo de taller le viene especialmente bien la diversidad, ya que se apoya en la observación y en la edición expresiva y artística del código fuente.

## Ponente:

-   Nombre: Reynaldo Cordero Corro

-   Bio:

> Coordinador en el Meetup HaskellMAD
Analista/Programador en la Universidad de Alcalá

### Info personal:

-   Mastodon (u otras redes sociales libres): <https://floss.social/@naldoco>
-   Twitter: <https://twitter.com/@naldoco>
-   GitLab (u otra forja) o portfolio general: <https://gitlab.com/HaskellKatas>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso

