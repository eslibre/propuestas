---
layout: 2024/post
section: proposals
category: workshops
author: Adriana Fuentes, Loli Iborra
title: Taller de pensamiento computacional
---

# Taller de pensamiento computacional

>Consiste en una actividad mentorizada para trabajar el pensamiento computacional que se realiza sin contar con dispositivos electrónicos; por eso, este tipo de actividades suelen llamarse, por tanto, desconectadas o desenchufadas.

## Detalles de la propuesta:

-   Tipo de propuesta: Taller / Presencial
-   Idioma: Español

-   Descripción:

>Mediante la participación en el juego, se realiza un proceso de pensamiento que permite formular o resolver problemas del mundo que nos rodea haciendo uso de habilidades y técnicas, como las secuencias e instrucciones ordenadas (algoritmos), para llegar a la solución, siguiendo la Rutina “Pienso – Programo – Pruebo”.

-   Web del proyecto: <https://bylinedu.org/>

-   Público objetivo: Sobre todo, niños y niñas que estén cursando educación primaria, para iniciarse en el mundo de la programación.

## Ponente:

-   Nombre: Adriana Fuentes, Loli Iborra

-   Bio:

>Adriana es UX/UI product designer y miembro del voluntariado de la asociación Bylinedu como mentora en el club CoderDojo Valencia.<br><br>
Loli es cofundadora y Responsable de proyectos en ByLInEdu, además de coordinar el club CoderDojo Valencia.

### Info personal:

-   Web personal: <https://www.linkedin.com/in/adriana-fuentes-garc%C3%ADa-92756b68/> / <https://www.linkedin.com/in/loli-iborra/>
-   Twitter: <https://twitter.com/@bylinedu>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso

