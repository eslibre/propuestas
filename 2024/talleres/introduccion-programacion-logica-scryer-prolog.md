---
layout: 2024/post
section: proposals
category: workshops
author: Adrián Arroyo Calle
title: Introducción a la programación lógica con Scryer Prolog
---

# Introducción a la programación lógica con Scryer Prolog

>La programación lógica es una forma de programar basada en la lógica formal. En vez de pensar en los pasos a seguir, pensamos en las relaciones lógicas entre las diferentes partes de un problema. En muchos problemas obtendremos la solución "como por arte de magia". El lenguaje más conocido de este paradigma es Prolog, nacido en 1972 y estándar ISO desde 1995.<br><br>
Scryer Prolog es un proyecto relativamente reciente, 100% software libre que trata de implementar ISO Prolog así como algunas innovaciones posteriores. En el taller usaremos Scryer Prolog para adentrarnos en este mundo y descubrir qué lo hace tan diferente y especial respecto a la programación imperativa y a la funcional y cómo te abre la mente pensar de esta forma tan diferente.

## Detalles de la propuesta:

-   Tipo de propuesta: Taller / Presencial
-   Idioma: Español

-   Descripción:

> El taller será un taller de introducción a la programación lógica usando Scryer Prolog. Scryer Prolog es un sistema software libre que trata de implementar ISO Prolog y algunas innovaciones posteriores. El paradigma de la programación lógica no es muy conocido y aunque algunas universidades en España todavía lo explican, muchas usan materiales anticuados. Aun con todos estos problemas, Prolog se sigue usando. Este mismo año pudimos ver como unos matemáticos usaron Prolog para determinar cuántos números de lotería había que comprar para ganar algo (https://www.europapress.es/ciencia/laboratorio/noticia-cuanta-loteria-hay-comprar-garantizar-gane-algo-20230804102326.html) y cómo muchas pruebas de nuevos medicamentos contra el cáncer no siguen la metodología adecuada (https://arxiv.org/abs/2012.05301). También se ha usado Prolog para diseñar el software de los trenes de metro automáticos, como la línea 14 de París.<br><br>
En el taller, se verán los fundamentos de la programación lógica y de Prolog en particular, centrándonos en pequeños problemas y optando por las soluciones más declarativas posibles. De esta manera obtendremos soluciones totalmente diferentes a las equivalentes en lenguajes como Python o Java. Una buena referencia de este estilo es el libro online The Power of Prolog (https://www.metalevel.at/prolog)

-   Web del proyecto: <https://www.scryer.pl/>

-   Público objetivo:

>Gente con algo de experiencia en algún otro lenguaje de programación.

## Ponente:

-   Nombre: Adrián Arroyo Calle

-   Bio:

> Graduado en Ingeniería Informática por la Universidad de Valladolid. Trabajo actualmente en Telefónica Innovación Digital desde 2019 desarrollando backend. Desde finales de 2020, principios de 2021 colaboro con el proyecto Scryer Prolog, en tareas tanto de código, como de documentación. En 2022 di una charla sobre cómo resolver puzzles usando Scryer Prolog (https://www.youtube.com/watch?v=c_yP_kr7DxI). Aparte tengo otras librerías software libre ajenas pero diseñadas para ser usadas con Scryer Prolog, como renders de Markdown, Djot, Jinja o conexiones con PostgreSQL.

### Info personal:

-   Web personal: <https://adrianistan.eu>
-   Mastodon (u otras redes sociales libres): <https://castilla.social/@aarroyoc>
-   Twitter: <https://twitter.com/@aarroyoca>
-   GitLab (u otra forja) o portfolio general: <https://github.com/aarroyoc/>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso

