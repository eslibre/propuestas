---
layout: 2024/post
section: proposals
category: workshops
author: Miguel Ángel Rodríguez Muiños, Alejandro Rodríguez Antolín 
title: Hydra o el arte programado&#58; taller de codificación y experimentación audiovisual en vivo con software libre
---

# Hydra o el arte programado: taller de codificación y experimentación audiovisual en vivo con software libre

>El taller se centrará en el conocimiento y manejo de Hydra, un poderoso y versátil sintetizador de vídeo y audio y entorno de codificación en vivo (live coding), que funciona a través de un navegador. Se presentará la herramienta y se procederá a conocer sus aspectos fundamentales de cara a la creación y experimentación audiovisual. No es necesario tener conocimientos previos de programación.<br><br>
El taller se puede seguir desde cualquier dispositivo que tenga un navegador actualizado y conexión a internet. Se recomienda a las personas asistentes que traigan auriculares que puedan conectar a sus dispositivos (aunque no es imprescindible).

## Detalles de la propuesta:

-   Tipo de propuesta: Taller / Presencial
-   Idioma: Español

-   Descripción:

>La codificación y composición algorítmica han revolucionado por completo el ámbito de la creación de proyectos audiovisuales y musicales. Basado en la programación de código y su manipulación en vivo con el objetivo de generar imágenes, vídeos y sonidos, esta forma de acercamiento al ámbito de la creación supone una posibilidad de reflexión para programadores, una vía alternativa para artistas y una herramienta poderosa para docentes, de cara a su integración en entornos académicos. Conocer el software libre que ofrece esta posibilidad es tan lúdico como útil, abriendo la puerta a experimentaciones de todo tipo.<br><br>
Una de estas herramientas es Hydra, un sintetizador de vídeo y audio en tiempo real y entorno de codificación en vivo (live coding). Funciona de manera similar a un sintetizador modular analógico y su sintaxis se inspira en ellos para generar contenidos a través de la interconexión de señales con distintas transformaciones. Gracias a Hydra, se facilita la conexión entre múltiples fuentes visuales (osciladores, cámaras, audio, vídeo) y creación de composiciones visuales, enrutando y transformando esas fuentes. Además, incluye una amplia gama de funciones para crear proyectos visuales complejos, tales como síntesis de video digital, efectos, filtros, composición...<br><br>
Por su equilibrio entre sencillez y efectividad, sin necesidad de instalación y con un entorno gráfico cómodo e intuitivo, Hydra se postula como una opción ideal para introducirse en la materia, así como para integrar en los ámbitos artístico, pedagógico o personal.<br><br>
En este taller conoceremos las principales características y posibilidades de Hydra, tanto en lo referente a la codificación de visuales (vídeo, imagen) como de audio. Así, exploraremos los parámetros y funciones básicas para introducir y comprender el manejo de la herramienta, con el objetivo de que los asistentes puedan integrarla en sus actividades como programadores, artistas o docentes.

-   Web del proyecto: <https://hydra.ojack.xyz/>

-   Público objetivo:

>Personas interesadas en la creación y edición audiovisual por medio de software libre, interesados en la composición algorítmica y codificación en vivo (live coding), programadores, artistas y docentes o expertos en las áreas de la educación y pedagogía. Si te apasiona la creatividad, la tecnología, la programación, el live coding o la experimentación, Hydra te ofrece un mundo de posibilidades para explorar... y este es tu taller!

## Ponente:

-   Nombre: Miguel Ángel Rodríguez Muiños, Alejandro Rodríguez Antolín

-   Bio:

>Miguel Ángel Rodríguez Muiños: Técnico Informático de profesión. Caballero andante del Software Libre por vocación y músico amateur por afición. Miembro de la Asociación MeLiSA.<br><br>
Alejandro Rodríguez Antolín: Musicólogo por la Universidad Autónoma de Madrid. Máster en Estudios Artísticos, Literarios y de la Cultura en la especialidad de Música y Artes Escénicas. Doctor con la especialidad en Musicología, especializado en música electroacústica, arte sonoro y experimentaciones sonoro-tecnológicas en España. Melómano, cinéfilo, videojugador, aficionado al ajedrez y friki de las tecnologías sonoras en general.

### Info personal:

-   Web personal: <https://audiofloss.melisa.gal/>
-   Twitter: <https://twitter.com/@mianromu>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso

