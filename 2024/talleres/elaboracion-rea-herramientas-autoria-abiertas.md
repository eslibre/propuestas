---
layout: 2024/post
section: proposals
category: workshops
author: Ismail Ali Gago
title: Elaboración de REA (Recursos Educativos Abiertos) con herramientas de autoría abiertas y su publicación en plataformas web
---

# Elaboración de REA (Recursos Educativos Abiertos) con herramientas de autoría abiertas y su publicación en plataformas Web

>Elaboración de Recursos Educativos Abiertos en formato digital e interactivos utilizando eXeLearning (https://exelearning.net/), aplicación integrada en MAX (https://www.educa2.madrid.org/web/max) y su complementación con las aulas virtuales (basadas en Moodle), incluyendo la nueva versión de eXeLearning Online y espacios de alojamiento web (Liferay) en Educamadrid (https://www.educa2.madrid.org/educamadrid/). Todo un ecosistema basado en software libre para la generación, publicación y difusión de contendidos educativos abiertos e interactivos en formato digital. Aunque es el mismo taller presentado en la edición de esLibre 2023 en Zaragoza, considero que la temática sigue siendo de actualidad.<br><br>
Se adjunta ejemplo de REA elaborado con eXeLearning y publicado en mi espacio web de Educamadrid:<br>
- Trabajando los ODS en un Instituto de Enseñanza Secundaria: <https://www.educa2.madrid.org/web/ismail.ali/trabajando-los-ods>

## Detalles de la propuesta:

-   Tipo de propuesta: Taller / Presencial
-   Idioma: Español

-   Descripción:

> Taller sobre elaboración de Recursos Educativos Abiertos en formato digital e interactivos utilizando eXeLearning (https://exelearning.net/), aplicación integrada en MAX (https://www.educa2.madrid.org/web/max) y su complementación con las aulas virtuales (basadas en Moodle) y espacios de alojamiento web (Liferay) en Educamadrid (https://www.educa2.madrid.org/educamadrid/).<br><br>
Durante el taller se mostrar a los participantes las diferentes opciones de creación y de publicación de REA, incluyendo las nuevas funcionalidades de la versión de eXeLearning Online integradas en las aulas virtuales de Educamadrid. También se mostrarán diversas opciones de publicación de REA en diferentes espacios de alojamiento web y de plataformas LMS.<br><br>
Todo un ecosistema basado en software libre para la generación, publicación y difusión de contendidos educativos abiertos e interactivos en formato digital.<br><br>
Se muestra ejemplo de REA elaborado con eXeLearning y publicado en mi espacio web de Educamadrid.

-   Web del proyecto:
    + <https://www.educa2.madrid.org/web/max>
    + <https://exelearning.net/>
    + <https://www.educa2.madrid.org/educamadrid/>
    + <https://www.educa2.madrid.org/web/ismail.ali/trabajando-los-ods><br><br>

-   Público objetivo:

>- Profesorado y alumnado de todos los niveles y etapas educativas. 
- Desarrolladores de software libre aplicado a la educación. 
- Cualquier persona interesada en las aplicaciones de las Tecnologías de la Información y la Comunicación en la docencia. 
- Cualquier persona interesada en el software libre y en el conocimiento libre y abierto. 
- STEAM, TIC, OpenScience. 

## Ponente:

-   Nombre: Ismail Ali Gago

-   Bio:

> Profesor de Biología y Geología a nivel de Enseñanza Secundaria. También he sido profesor Asociado de Nuevas Tecnologías aplicadas a la Educación en la Facultad de Formación de Profesorado de la Universidad Autónoma de Madrid.
- Promotor y primer coordinador del Grupo de Desarrollo MAX MAdrid_linuX: <https://www.educa2.madrid.org/web/max>
- Promotor y primer coordinador del Grupo de Desarrollo eXeLearning: <https://exelearning.net/>
- Actualmente sigo colaborando con ambos grupos y como formador de formadores en las áreas TIC y STEAM
- Scientix Ambassador: <https://www.scientix.eu/in-your-country/scientix-4-teacher-panel##ES>

### Info personal:

-   Web personal:
    + <https://www.educa2.madrid.org/web/ismail.ali>
    + <https://es.linkedin.com/in/ismail-ali-gago-3aa5862b>
-   Twitter: <https://twitter.com/@ismagago>

## Comentarios

>Aunque es el mismo taller presentado en la edición de esLibre 2023 en Zaragoza, considero que la temática sigue siendo de actualidad, y en esta edición se incluyen las novedades desarrolladas durante el presente curso académico tanto en MAX, como en Educamadrid y en eXeLearning.

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso

