---
layout: 2024/post
section: proposals
category: workshops
author: Mario Vázquez
title: Introducción a seguridad en contenedores
---

# Introducción a seguridad en contenedores

>La seguridad en contenedores es uno de los grandes desafíos a los que se enfrentan las compañías al desplegar Kubernetes en producción. En esta presentación, hablaremos sobre algunas de las tecnologías que utilizamos para proteger nuestros contenedores en producción.

## Detalles de la propuesta:

-   Tipo de propuesta: Taller / Presencial
-   Idioma: Español

-   Descripción:

> La seguridad en contenedores es uno de los grandes desafíos a los que se enfrentan las compañías al desplegar Kubernetes en producción. En esta presentación, hablaremos sobre algunas de las tecnologías que utilizamos para proteger nuestros contenedores, como las _capabilities_ de Linux y los perfiles Seccomp.<br><br>
>También exploraremos cómo estas tecnologías pueden ser empleadas en entornos Kubernetes.<br><br>
Los asistentes a este taller obtendrán:
- Conocimientos sobre qué son las _capabilities_ de Linux
- Cómo gestionar las _capabilities_ en contenedores
- Conocimientos sobre qué son los Secure Compute Profiles (seccomp)
- Cómo crear tus propios perfiles Seccomp
- Cómo trabajar con _capabilities_ y perfiles Seccomp en Kubernetes

-   Público objetivo:

>- Administradores de sistemas trabajando con Kubernetes
- Desarrolladores de software trabajando con Kubernetes
- Entusiastas de Kubernetes en general

## Ponente:

-   Nombre: Mario Vázquez

-   Bio:

> Ingeniero de soluciones en Red Hat, apasionado de la automatización, contenedores y cloud hybrida!

### Info personal:

-   Web personal: <https://linuxera.org>
-   Twitter: <https://twitter.com/mvazce>
-   GitLab (u otra forja) o portfolio general: <https://github.com/mvazquezc>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso

