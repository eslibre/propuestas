---
layout: 2024/post
section: proposals
category: workshops
author: inventadero
title: Creación de realidad extendida con FLOSS | OpenLAB - La Mutant XR
---

# Creación de realidad extendida con FLOSS | OpenLAB - La Mutant XR

>(Pro)puesta en común por parte de las universidades Rey Juan Carlos y Complutense de Madrid sobre flujos de trabajo con FLOSS para Realidad Extendida (XR).<br><br>
Amalgamados por el Software Libre, diversos departamentos de múltiples disciplinas (Arqueología, Computación y Diseño de URJC, junto a Dibujo y Escultura de UCM) proponen este taller práctico y abierto a cualquier persona con curiosidad por la XR.

## Detalles de la propuesta:

-   Tipo de propuesta: Taller / Presencial
-   Idioma: Español

-   Descripción:

>A partir del espacio real de LA MUTANT se trabajará y reflexionará sobre los siguientes aspectos:<br>
1. Fotogrametría: Edición 2→3D con Meshroom y otros FOSS. [Datos de entrada: Nubes de puntos LIDAR, fotos o vídeos, etc...]
2. Espacios XR: Creación con A-Frame, etc... [Softwares de conversión/edición: a malla y otros formatos 3D]
3. Visualización y consumo energético: glTF, splats, etc... [Creación/visualización espacios XR: A-frame y otros FLOSSXR]

>- Material necesario: si puedes tráete tu portátil, trabajaremos con el siguiente software; Meshroom, Blender, A-Frame (opción browser), Godot VR y otros . . .<br><br>
- Objetivos:
    * Puesta en común de procesos.
    * Opciones de optimización de software.
    * Crear espacio(s) XR LA MUTANT.
    * Índice para Recurso Educativo Abierto.

-   Web del proyecto: <https://8d2.es/course/view.php?id=86>

-   Público objetivo:

>Cualquier persona interesada en la XR y sus flujos de trabajo libres.<br><br>
El rango de campos relacionados en muy amplio; desde la arqueología o el diseño a la visualización de datos o la creación artística ~ ~ ~


## Ponente:

-   Nombre: inventadero (en representación de varios departamentos de las universidades Rey Juan Carlos y Complutense de Madrid)

-   Bio:

>Propuesta motivada gracias a la atención e interés de Jesús G. Barahona, coordinador/a de la Oficina de Conocimiento y Cultura Libres de la URJ, Carmen Pérez, directora del GI "Dibujo y conocimiento" y Ricardo Espinosa, coordinador del PID: HACKLAB3D de BBAA UCM.


### Info personal:

-   Web personal: <https://8d2.es/>
-   Mastodon (u otras redes sociales libres): <https://qoto.org/@inventadero>
-   Twitter: <https://twitter.com/@inventadero>

## Comentarios

>Buenas!!<br><br>
Creo que no dio tiempo a presentar ""dev-room" con este tema de flujos de creación de realidad extendida con FOSS, entre Jesús G. Barahona y otros departamentos de computación URJC y yo mismo, en nombre del departamento de dibujo BBAA UCM.<br><br>
Así que propongo este taller en el que ( mientras hacemos la parte práctica: fotogrametría para editar e implantar en Realidad Aumentada en La Mutant [podéis ver una práctica similar en BBAA UCM aquí <https://8d2.es/course/view.php?id=85>]) reflexionemos y compartamos abiertamente las posibilidades de creación XR con FOSS ( ya nos juntamos en URJC un día . . . ) Creo que pueden salir cosas interesantes de esta conjunción . . .<br><br> 
Un saludo y enhorabuena por el trabajote!!

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso

