---
layout: 2024/post
section: proposals
category: misc
author: Mariano Flores
title: RETO (Recursos Educatius de Tecnología Oberta) per la Justicia Global
---

# RETO (Recursos Educatius de Tecnología Oberta) per la Justicia Global

>Queremos compartir la web y la plataforma que permite crear y compartir propuestas educativas de niveles no universitarios (primaria y secundaria, principalmente) La plataforma permite la.colaboracion en el.diseño y concreción de la propuestas didácticas. También permite publicar en licencia abierta para que otros docentes repliquen, amplíen o mejoren las unidades didáctica Se integran criterios para fomentar enfoques pedagógicos desde la.sobernia digital, la interculturalidad y el apoderamiento feminista.

## Detalles de la propuesta:

-   Tipo de propuesta: Póster

-   Descripción:

> Recursos Educatius de tecnologia oberta per a la Justícia Global
Necessites inspiració per incorporar l’educació crítica i transformadora a l’aula? Descobreix els darrers materials d’Educació per a la Justícia Global compartits en aquesta plataforma. Hi trobaràs exemples de recursos que pots aplicar directament a l’aula o que et poden servir de punt de partida per dissenyar noves propostes didàctiques adaptades als grups d’infants i joves amb qui treballes.<br><br>
T’animem a que utilitzis i facis teus els recursos disponibles, publicant les teves pròpies activitats, i posant a disposició de la comunitat de docents i entitats d’EpJG les adaptacions, ampliacions, modificacions, traduccions... que facis dels materials compartits a la plataforma RETO.

-   Web del proyecto: <https://reto.edualter.org>

-   Público objetivo:

>Docentes de primaria y secundaria
Personal de ONG's elaborando propuestas educativas de Justicia Global, Solidaridad Internacional.

## Propuesta enviada por:

-   Nombre: Mariano Flores

### Info personal:

-   Mastodon (u otras redes sociales libres): <https://mastodon.social/@edualter>
-   Twitter: <https://twitter.com/Edualter>


## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso

