---
layout: 2024/post
section: proposals
category: misc
author: Rafael Conde Melguizo
title: Lecciones de la comunidad Coronavirus Makers en España&#58; impresión 3D contra la COVID-19
---

# Lecciones de la comunidad Coronavirus Makers en España: impresión 3D contra la COVID-19

>Esta investigación presenta al colectivo Coronavirus Makers o Coronamakers, quienes durante los peores momentos de la pandemia por COVID-19 diseñaron e imprimieron materiales para ayudar a combatir la pandemia.<br><br>
El artículo describe un caso único e irrepetible de creación de una comunidad de conocimiento abierto que creaba y compartía diseños libres de impresión 3D, ayudaba a su distribución de manera colaborativa y generó canales de comunicación colectivos aprovechando herramientas abiertas como Telegram.

## Detalles de la propuesta:

-   Tipo de propuesta: Artículo

-   Descripción:

>El artículo "LESSONS FROM CORONAVIRUSMAKERS COMMUNITY IN SPAIN: 3D PRINTING SHIELD AGAINST COVID-19" recoge un estudio realizado en plena pandemia mediante una encuesta dentro del canal de Telegram del colectivo Coronavirus Makers. Este colectivo estaba imprimiendo esos días materiales en 3D para ayudar a combatir la pandemia de manera altruista.<br><br>
Es un estudio que refleja una situación real e irrepetible sobre como se organiza una comunidad de conocimiento abierto que comparte diseños, procedimientos y tecnologías a través de un modelo de organización horizontal apoyado en el uso de tecnologías de comunicación abiertas.<br><br>
Es curioso que durante los confinamientos de 2020 los call for papers sobre COVID y pandemias se multiplicaron. Desde el punto de vista científico, es cuestionable, pues ninguno éramos expertos en lo que no había pasado nunca. Sin embargo, miles de papers se publicaron con experimentos express. Ahora, sin embargo, ya deberíamos poder publicar estudios fundamentados como este que presentamos aquí.  Pero en 2024 ya no abundan calls sobre COVID-19. Parece que también la comunidad científica quiere olvidar pronto la pandemia.<br><br>
Siendo conscientes del valor de nuestra aportación para el estudio de comunidades horizontales de conocimiento abierto -más aun teniendo en cuenta el contexto- y de la dificultad para ser publicada, los autores hemos liberamos nuestro paper en formato PrePrint en la plataforma Figshare, donde cualquiera puede leerlo e incluso descargar libremente los datos originales, por si quiere reutilizarlos. Artículo y datos se encuentran con licencia CC-BY 4.0 y cuentan con DOI para su cita.

-   Web del proyecto: <https://figshare.com/articles/preprint/_b_LESSONS_FROM_CORONAVIRUSMAKERS_COMMUNITY_IN_SPAIN_b_3D_PRINTING_SHIELD_AGAINST_COVID-19/25411984>

-   Público objetivo:

>Cualquier persona o investigador interesado en comunidades de conocimiento abierto. Investigadores sociales: sociólogos, antropólogos, etc. Colectivos maker. Investigadores en salud pública.

## Propuesta enviada por:

-   Nombre: Rafael Conde Melguizo

-   Bio:

>Doctor en Sociología. Actualmente, profesor e investigador en la universidad UDIT. IP del proyecto DEED (Digitalización de Espacios para Educación y Divulgación científica y cultural)<br><br>
Previamente, director del grado en Artes Digitales de la UCJC de 2018 a 2023. Profesor en ESNE en los grados de Diseño de Videojuegos, Diseño Multimedia y Gráfico, Diseño de Producto y Master UX de 2010 a 2019. Investigador en UPM de 2007 a 2012 en grupo T>SIC.<br><br>
En el esLibre de 2021 propuse y coordiné la mesa EWOK (Education with Open Knowledge): https://propuestas.eslib.re/2021/salas/ewok-education-with-open-knowledge 

### Info personal:

-   Twitter: <https://twitter.com/rcondemelguizo>

## Comentarios

>El artículo está coescrito con el profesor <Jonathan Z. Bar-Magen Numhauser>(https://www.linkedin.com/in/4yony4/), quien comparte la publicación y su envío a este congreso.

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso

