---
layout: 2020/post
section: proposals
category: workshops
title: Configura un entorno bash geek y personalizado al gusto | Bash y Tmux
---

Taller en el que los asistentes crearán un usuario en su sistema operativo al cuál será el usuario que reciba todas estas configuraciones, si les gustan, se lo podrán poner a su usuario personal. Los temas se presentarán de forma progresiva para que los asistentes adquieran unos conocimientos base para comprender el resultado final. Toda la presentación irá acompañada de diapositivas y explicaciones. Se ofrecerán archivos de configuración de complejidad progresiva con el código de ejemplo que que estaré trabajando sus explicaciones en comentarios para que los asistentes no se pierdas y puedan seguir el taller aún habiéndose perdido. Los asistentes se llevarán a casa unos ficheros para hacer de bash una terminal bonita y personalizada.

## Objetivos a cubrir en el taller

El objetivo de este taller es llevar a cabo un taller didáctico, divertido y útil que nos enseñará los fundamentos de la terminal GNU presente en gran cantidad de Sistemas Operativos basados en UNIX como GNU/Linux, además de cómo configurar un entorno de terminal cómodo, personalizado, bonito e interactivo con el objetivo de tener otra visión de la terminal de GNU/Linux, simplificar procesos o impresionar a tus colegas Geek con tu terminal llena de colores y comandos personalizados, ¡hasta animaciones! (la terminal no es el demonio feo que pensamos). Los asistentes requieren de un mínimo de experiencia con la terminal (moverse por los directorios, copiar, mover y editar archivos de texto), además de su distribución UNIX, GNU/Linux o SO que use Bash como intérprete de comandos. En el taller se va a dar el material software libre necesario para seguir el taller con ejemplos explicados en el documento y en el taller presencial.

Temas que se van a exponer y poner en práctica:

1.  Introducción a la misma Bash (Bourne-again shell): ¿Qué es? ¿En qué sistemas se encuentra presente? Características de un intérprete de comandos. Estructura y uso de un intérprete de comandos: `PROMPT> nombrecomando argumento /rutaobjetivo /rutadestino`.

2.  Los ficheros `~/.bashrc`, `~/bash_profile`, `~/.bash_history`, `/etc/bashrc`, `/etc/profile`: ¿Qué hacen estos ficheros? Crearemos un usuario de pruebas y le editaremos estos archivos, pudiendo hacer un entorno personalizado a cada usuario del sistema. Ej: root.

3.  Variables de entorno y opciones del shell: Veremos varias, pero especialmente `HOME`, `PATH`, `PS1`, `PWD`, `OLDPWD`. Usaremos estas variables para personalizaremos nuestro prompt y podremos ejecutar comandos personalizados. Aprenderemos a listar, establecer, modificar y exportar variables para hacer ciertas tareas más fáciles. También veremos las opciones del shell con el comando `~$ set -o`¡Para mejorar y usar opciones extra, evitar salidas accidentales o usar vi en la propia terminal!

4.  Scripting básico, definición de alias, traps, etc: Usaremos todos estos recursos para configurar características en el fichero `.bashrc`.

5.  Multiplexación de terminales (Tmux): Cómo instalar Tmux, cómo usar Tmux y ficheros de configuración de este; al igual que con el `.bashrc`, le daremos nuestro toque personal y haremos que arranque conjunto a cada bash abierta.

## Público objetivo

Este taller va orientado a pensonas interesadas en conocer en profundidad una de las terminales más usadas en el mundo, el asistente debe tener unos mínimos de conocimientos y experiencia en Bash.

## Ponente(s)

**Pablo Martínez Lázaro**

### Contacto(s)

-   **Pablo Martínez Lázaro**: @FallFur

## Prerrequisitos para los asistentes

El asistente debe haber usado la terminal alguna vez como mínimo, moverse por los directorios, editar ficheros y comandos básicos.

## Prerrequisitos para la organización

Necesitaré proyector y micrófono.

## Tiempo

El taller tendrá una duración aproximada de 1:30 horas (puede extenderse a 2 horas según el interés de los asistentes).

## Día

Preferiblemente me gustaría realizar el taller el sábado 6 de Junio, pero no me importa hacerlo el viernes 5 de Junio.

## Comentarios

Ninguno.

## Condiciones

-   [x]  Acepto seguir el [código de conducta](https://eslib.re/conducta/).
-   [x]  Al menos una persona entre los que proponen el taller estará presente el día programado para el mismo.
-   [x]  Acepto coordinarme con la organización de esLibre.
