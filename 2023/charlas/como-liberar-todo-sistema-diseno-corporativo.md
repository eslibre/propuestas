---
layout: 2023/post
section: proposals
category: talks
author: tripu
title: Cómo liberar todo un sistema de diseño corporativo
---

# Cómo liberar todo un sistema de diseño corporativo

>Ya sabemos que una empresa de software necesita de un sistema de diseño: ese catálogo unificado y coherente de patrones, elementos y buenas prácticas que contribuyen a proporcionar una experiencia de usuario suave como la mantequilla y consistente como el granito. Lo que quizá no se te había ocurrido es que tu sistema de diseño también es software, debe tratarse como tal y… también puede ser software libre.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla larga / Presencial
-   Idioma: Español

-   Descripción:

>En esta charla contaremos cómo nos embarcamos en la odisea de compartir con el mundo nuestro incipiente y ambicioso sistema de diseño. Los protagonistas: un equipo de diseñadores y programadores con más buenas intenciones que conocimiento de lo que se les venía encima. Spoiler alert: después de meses de trabajo, ideas felices y muchas tazas de café (y varios cambios de enfoque por el camino), todo el sistema de diseño vio la luz y está hoy disponible en GitHub. Esto incluye design tokens, paletas de iconos, guías sobre usabilidad e imagen corporativa, empaquetadores de estilos, demos y documentación… todo libre y en abierto, y sin dependencias apuntando a proyectos internos.<br><br>
Compartiremos algunos escollos que nos sorprendieron, consejos prácticos, enseñanzas derivadas de este proyecto, y herramientas útiles. Y defenderemos la utilidad de publicar un sistema de diseño; tanto para la comunidad de profesionales como para la institución que libera el trabajo y lidera su evolución en abierto… o no.

-   Web del proyecto: <https://github.com/orgs/DevoInc/repositories?q=genesys>

-   Público objetivo:

>Programadores web y diseñadores web, en general

<p style="padding-top: 5px; text-align: center; font-size: 1.20rem;"><strong>La asistencia al evento es libre y gratuita, pero por cuestiones de aforo<br> necesitamos que te registres si tienes pensado asistir a las actividades.</strong></p>

-   Horario: viernes 12 mayo 16:30-17:00
-   Registro de asistentes al congreso: <https://eventos.librelabgrx.cc/events/798b67b2-21d2-4811-8201-7e1e14c69458>

## Ponente:

-   Nombre: tripu

-   Bio:

>Nacido en Madrid en 1980. Ingeniero en informática por la Universidad de Granada y MA en Japanese Cultural Studies por University of London.<br><br>
Durante los últimos 19 años ha trabajado como programador, analista y jefe de proyectos; tanto en start-ups como en empresas multinacionales; en Sevilla, en Madrid, en Londres y en Tokio. Sus principales intereses son el software libre, GNU/Linux, los sistemas abiertos, la informática gráfica, interfaces y usabilidad, la tecnología de la web y JavaScript.<br><br>
Fue programador web dentro del equipo de sistemas del World Wide Web Consortium (W3C) durante cinco años, donde se dedicó a desarrollar herramientas y aplicaciones del W3C, públicas o internas; y a participar en eventos relacionados con los estándares web.<br><br>
Actualmente vive en Madrid, y trabaja para Devo como ingeniero senior de software en proyectos de front-end.

### Info personal:

-   Web personal: <https://tripu.info/>
-   Mastodon (u otras redes sociales libres): <https://qoto.org/@tripu>
-   Twitter: <https://twitter.com/tripu>
-   GitLab (u otra forja) o portfolio general: <https://gitlab.com/tripu.info>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
