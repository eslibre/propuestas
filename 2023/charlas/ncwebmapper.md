---
layout: 2023/post
section: proposals
category: talks
author: Fergus Reig Gracia
title: NcWebMapper
---

### Redirigiendo a la página de la sala...

[Acceder directamente](https://propuestas-libregeo.eslib.re/2023/charlas/ncwebmapper)

<script>
  document.body.onload = function() {
    window.location = "https://propuestas-libregeo.eslib.re/2023/charlas/ncwebmapper";
  }
</script>
