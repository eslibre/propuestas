---
layout: 2023/post
section: proposals
category: talks
author: Jaime Crespo (jynus)
title: Wikipedia&#58; infraestructura libre para un conocimiento libre
---

# Wikipedia: infraestructura libre para un conocimiento libre

>Wikipedia, y otros proyectos colaborativos libres de Wikimedia no podrían funcionar sin los millones de colaboradores voluntarios a lo largo de todo el mundo. Sin embargo, en general la gente desconoce en su mayoría cómo funcionan por dentro los servidores de Wikimedia para poder servir 22 mil millones de visitas y 45 millones de ediciones al mes y - completamente basados en software y tecnologías libres. Yo te lo cuento para demostrar que esta forma de trabajar no sólo es posible, sino deseable, así como para animarte a colaborar!

## Detalles de la propuesta:

-   Tipo de propuesta: Charla larga / Presencial
-   Idioma: Español

-   Descripción:

>Imagina un mundo en el que todas las personas tienen acceso a la totalidad del conocimiento humano. Este es el objetivo de la organización sin ánimo de lucro Wikimedia Foundation, la cual proporciona soporte informático y legal a los casi 1000 proyectos en más de 300 idiomas que aloja, incluidos la Wikipedia, WikiCommons y Wikidata.<br><br>
Para que esto sea posible, y de forma diferencial, Wikimedia usa exclusivamente sistemas operativos libres (Debian Linux) y software libre (Apache, PHP, MariaDB) en sus más de 4000 servidores, y no usa CDNs o nubes de terceros para maximizar la privacidad de nuestros usuarios.<br><br>
Y no sólo "consumimos" software libre, también lo creamos -empleados y voluntarios colaboramos juntos en repositorios abiertos de software. Por transparencia, también exponemos nuestras métricas y gestionamos nuestra infraestructura completamente en abierto, por lo que todos nuestros servidores también aceptan parches y reportes de cualquier persona.<br><br>
Os contaremos cómo funciona la Wikipedia por dentro y cómo colaborar con nosotros para seguir compartiendo información y cultura de manera libre- y convenceros que cualquier otra administración o empresa podría hacer lo mismo.

-   Web del proyecto: <https://wikitech.wikimedia.org>

-   Público objetivo:

>La charla va dirigida principalmente a gente con conocimientos técnicos (e.g. programación o sistemas) de cualquier nivel (de estudiantes a gerentes), especialmente a aquellas/os que desean aplicar un modelo abierto a su trabajo, o querrían aprender a colaborar con un proyecto libre como Wikimedia. También a miembros de la comunidad Wikimedia que quieren aprender cómo funciona por dentro.

<p style="padding-top: 5px; text-align: center; font-size: 1.20rem;"><strong>La asistencia al evento es libre y gratuita, pero por cuestiones de aforo<br> necesitamos que te registres si tienes pensado asistir a las actividades.</strong></p>

-   Horario: viernes 12 mayo 16:00-16:30
-   Registro de asistentes al congreso: <https://eventos.librelabgrx.cc/events/798b67b2-21d2-4811-8201-7e1e14c69458>

## Ponente:

-   Nombre: Jaime Crespo (jynus)

-   Bio:

>He trabajado desde hace 8 años como "Site Reliability Engineer" en el departamento de Tecnología de la Fundación Wikimedia, la fundación sin ánimo de lucro basada en EE.UU., encargada de apoyar proyectos de conocimiento libre como Wikipedia. Mi trabajo consiste en asegurarme que la Wiki esté funcionando en todo momento, y mi especialización es en Datos (backups y bases de datos).<br><br>
He presentado en conferencias pasadas sobre este tema (siempre con enfoques distintos, adecuados a cada audiencia), lo cual ha sido reflejado en la prensa internacional: <https://www.linux-magazine.com/Issues/2020/232/The-Wikimedia-Foundation> / <https://twitter.com/el_pais/status/1454936616418611204><br><br>
Fui alumno de la Universidad de Zaragoza, ex-secretario y (ex-)miembro fundador de Púlsar y uno de los primeros trabajadores de la Oficina de Software Libre de la Universidad de Zaragoza.

### Info personal:

-   Web personal: <https://jynus.com>
-   Twitter: <https://twitter.com/@jynus>
-   GitLab (u otra forja) o portfolio general: <https://gerrit.wikimedia.org/r/admin/repos/q/filter:operations>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
