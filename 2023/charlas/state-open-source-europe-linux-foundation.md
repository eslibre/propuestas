---
layout: 2023/post
section: proposals
category: talks
author: Cailean Osborne
title: What’s the state of Open Source in Europe? Insights from the Linux Foundation’s “World of Open Source Europe spotlight 2022” research report
---

# What’s the state of Open Source in Europe? Insights from the Linux Foundation’s “World of Open Source Europe spotlight 2022” research report

>This talk introduces insights on open source software trends and priorities in Europe from our recent research report, “World of Open Source: Europe Spotlight 2022,” conducted by Linux Foundation Research and Scott Logic, a UK-based software consultancy. The result of quantitative surveys and qualitative interviews, the report describes the “state of open source” across the European continent and builds a comprehensive picture by examining open source consumption, contributions, challenges, motivators, and opportunities. While the report covers a wide range of topics relating to open source in Europe, in this talk we will focus on some of the most compelling conclusions from our findings. We hope that these conclusions will provide individuals, organisations, and governments in Spain, Europe, and beyond with tangible advice that allows them to better unlock the growing value of open source software.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla larga / Presencial
-   Idioma: English

-   Descripción:

>This talk will introduce insights on open source software trends and priorities in Europe from our recent report, “World of Open Source: Europe Spotlight 2022,” conducted by Linux Foundation Research and Scott Logic, a UK-based software consultancy. Combining insights from quantitative surveys and qualitative interviews, the report describes the “state of open source” across the European continent and builds a comprehensive picture by examining open source consumption, contributions, challenges, motivators, and opportunities. While the report covers a wide range of topics relating to open source in Europe, in this talk we will focus on four of the most compelling conclusions from our findings.<br><br>
Firstly, our research finds an imbalance between consumption of and contributions to open source by organisations, which challenges the sustainability of open source projects. A significant proportion of our respondents indicated a lack of a clear policy at their organisation or that they simply did not know what the policy was. In contrast, very few saw the same challenges when it came to consuming open source. This gap widened further in some sectors, such as telecommunications, public, finance, and insurance. The result: organisations tend to ‘take’ more than they ‘give’. Consequently, many open source projects are suffering from growing sustainability challenges. The damaging effects of this are experienced most visibly through high-profile security incidents due to a lack of open source maintenance. However, less visible effects include a growing unease within many open source communities.<br><br>
Second, clear leadership pays dividends, with micro- and enterprise-scale organisations leading the way on open source. The route to unlocking value from open source goes far beyond simply creating the right policies. We found that organisations with a structured approach to open source via an OSPO, or simply visible leaders, tend to have organisational culture that encourages and empowers employees to contribute to open source. We also found that organisations at the two extremes of the scale (<10 or >10,000 employees) tend to have an OSPO or visible leader, while mid-sized organisations tend to lack both. There is clear potential for these organisations to follow in the footsteps of very small or very large organisations, which creates an open source leadership structure that empowers and supports their employees.<br><br>
Thirdly, the public sector is failing to fully capitalise on open source. We are increasingly seeing open source consumption being formally prescribed by national and international government bodies across Europe. Much of the code that the public sector produces is now shared in the open, which is primarily for reasons of transparency. However, despite the consumption policies and increasing number of public sector-founded projects, this sector remains an outlier across many aspects of our research. There is limited inner source activity, which indicates a lack of collaboration between public sector organisations, and a lack of a clear contribution policy, which potentially suggests an overly narrow appreciation of open source’s value and that it is simply a mechanism for the transparency of work rather than for collaboration and collective value creation. With the public sector having so much to gain from open source, much needs to be done to create a cultural shift. Policies that simply mandate consumption and that ‘code must be shared’ miss out on much of the value that open source has to offer.<br><br>
Finally, open source can be an apolitical key to fostering digital sovereignty. Digital sovereignty is high on political agendas across Europe: from the European Commission to national governments. North America drives and owns much of our digital world in terms of the products, services, and infrastructure that we depend on. There is a clear need to create and sustain the mechanisms that will enable Europe to plot its own course in the digital world. Our survey responses reinforce the notion that open source is a powerful mechanism for innovation, collective value creation, and ultimately bringing the vision of the ‘digital commons’ to life. There are strong beliefs that industry standards and interoperability benefit the most from open source and that there should be further investment in open source alternatives to technology monopolies. Open source exists and operates beyond politics, which inclusively drives value for all; it breeds digital products and services that anyone can use; it ensures space for constraint-free innovation and collaboration; and it creates rich environments for skills and capability development.<br><br>
We hope that these conclusions will provide individuals, organisations, and governments in Spain, Europe, and beyond with tangible advice that allows them to better unlock the growing value of open source software.

-   Web del proyecto: <https://www.linuxfoundation.org/research/world-of-open-source-europe-spotlight>

-   Público objetivo:

>European OSS developers, European OSPOs, European governments, European industry

<p style="padding-top: 5px; text-align: center; font-size: 1.20rem;"><strong>La asistencia al evento es libre y gratuita, pero por cuestiones de aforo<br> necesitamos que te registres si tienes pensado asistir a las actividades.</strong></p>

-   Horario: viernes 12 mayo 18:45-19:10
-   Registro de asistentes al congreso: <https://eventos.librelabgrx.cc/events/798b67b2-21d2-4811-8201-7e1e14c69458>

## Ponente:

-   Nombre: Cailean Osborne

-   Bio:

>Cailean Osborne is a Researcher at the Linux Foundation (LF) and a PhD Candidate in Social Data Science at the University of Oxford. At the LF, Cailean contributes to research projects led by LF Research and supports the LF Europe team with regional growth. Previously, Cailean worked in technology policy at the UK Government.


### Info personal:

-   Web personal: <https://www.oii.ox.ac.uk/people/profiles/cailean-osborne/>
-   Mastodon (u otras redes sociales libres): <https://fosstodon.org/@cosborne>
-   Twitter: <https://twitter.com/@cailean_osborne>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
