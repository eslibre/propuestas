---
layout: 2023/post
section: proposals
category: talks
author: Antonio Montes (inventadero)
title: Malagueira XR&#58; los bocetos de Álvaro Siza Vieira en VR
---

# Malagueira XR: los bocetos de Álvaro Siza Vieira en VR

>Emergen nuevos paisajes y entornos de creación con la Realidad Extendida; esta mar donde confluyen la Virtual, Aumentada y Mixta.<br><br>
La coexistencia de lo imaginado junto a lo ya construido; es una pequeña colaboración con la escuela de Arquitectura de la Universidad de Évora en un gran proyecto europeo “MALAGUEIRA; Patrimonio de Todos”, que contribuye a la nominación de Évora 2027 como ciudad Patrimonio Mundial de la UNESCO, poniendo en valor la obra más querida del arquitecto ASV.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla larga / Presencial
-   Idioma: Español

-   Descripción:

>Narraré, a modo de cuaderno de bitácora, el caso de los bocetos arquitectónicos de Álvaro Siza Vieira transcritos a la XR del barrio de Malagueira y los procedimientos que lo posibilitaron.<br><br>
El desafío: Respetar tres premisas de la obra de Siza en Malagueira; Accesibilidad , Colaboración y Economía de medios. ¿Cómo? Con sus homólogas digitales; Multiplataforma, con cualquier dispositivo y basada en el navegador. Software Libre, donde son fabulosas las sinergias arte~programación. “Low poly”, geometrías ligeras y consumos mínimos de energía en la navegación Virtual.<br><br>
Esta charla es abierta y aunque no pretende sumergirse en un conocimiento profundo del software, sino navegar en la superficie con tripulación multidisciplinar, se agradece colaboración de geoinquietos y otros animales.<br><br>
Se propone taller aparte para poner en práctica lo expuesto aquí en el paisaje de Zaragoza.

-   Web del proyecto: <https://b8d.es/>

-   Público objetivo:

>Interesados en XR, topografía, dibujo, edición 3D, arquitectura, avatares, naturaleza ~ ~ ~

<p style="padding-top: 5px; text-align: center; font-size: 1.20rem;"><strong>La asistencia al evento es libre y gratuita, pero por cuestiones de aforo<br> necesitamos que te registres si tienes pensado asistir a las actividades.</strong></p>

-   Horario: viernes 12 mayo 11:00-11:30
-   Registro de asistentes al congreso: <https://eventos.librelabgrx.cc/events/798b67b2-21d2-4811-8201-7e1e14c69458>

## Ponente:

-   Nombre: Antonio Montes

-   Bio:

>Dibujante, "empantallao" a veces, Blenderita , enseñando~aprendiendo.


### Info personal:

-   Web personal: <https://8d2.es/>
-   Mastodon (u otras redes sociales libres): <https://qoto.org/@inventadero>
-   Twitter: <https://twitter.com/@inventadero>
-   GitLab (u otra forja) o portfolio general: <https://github.com/inventadero>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
