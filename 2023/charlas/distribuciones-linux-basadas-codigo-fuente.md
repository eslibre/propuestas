---
layout: 2023/post
section: proposals
category: talks
author: Guillermo Roche Zarralanga
title: Distribuciones linux basadas en código fuente (Gentoo y compañia)
---

# Distribuciones linux basadas en código fuente (Gentoo y compañia)

>Explicar las ventajas de este tipo de distribuciones, y contar un poco la realidad de esta forma de gestionar los paquetes así como sus posibles usos más allá de un ordenador de personal

## Detalles de la propuesta:

-   Tipo de propuesta: Charla larga / Presencial
-   Idioma: Español

-   Descripción:

>Las distribuciones basadas en código fuentes están mitificadas por ser difíciles de instalar, y tienen fama de ser las más rápidas. La idea es contar un poco como funcionan, que ventajas reales tienen, tratando un poco ese mito de que son difíciles de entender y manejar. También exponer por encima las distintas distribuciones que hay y algunas diferencias, y como encajan en la mentalidad tan plural que hay en la comunidad linuxera.<br><br>
Otro punto a abordar será el uso real y productivo que tiene el usar un gestor de paquetes basado en código fuente, por que muchas distribuciones binarias tienen uno, y sobre todo que sentido tiene el compilar un programa para ti en lugar de usar un binario

-   Público objetivo:

>Cualquier usuario de sistemas libres que tenga curiosidad sobre este tipo de distribuciones o que haya escuchado hablar sobre ellas

<p style="padding-top: 5px; text-align: center; font-size: 1.20rem;"><strong>La asistencia al evento es libre y gratuita, pero por cuestiones de aforo<br> necesitamos que te registres si tienes pensado asistir a las actividades.</strong></p>

-   Horario: viernes 12 mayo 19:15-19:40
-   Registro de asistentes al congreso: <https://eventos.librelabgrx.cc/events/798b67b2-21d2-4811-8201-7e1e14c69458>

## Ponente:

-   Nombre: Guillermo Roche Zarralanga

-   Bio:

>Apasionado de la informática y usuario de gnu/linux durante años. Actualmente trabajo como QA en el proveedor de servicios de hosting Arsys. Desde hace años he probado y jugado con distintos tipos de sistemas operativos para conocerlos y profundizar con ellos, ya sea para un uso domestico o industrial para proveer mis propios servicios de forma comunitaria, aunque casi siempre he usado como sistema principal Gentoo, distribución sobre la que se centrará la charla.<br><br>
Como miembro activo de la comunidad de gentoo en españa, en la primera convención que hicimos de gentoo di una charla sobre como tenía configurado uno de mis sistemas para compilar en otras arquitecturas distintas al host: https://www.youtube.com/watch?v=uhMgMQ01Z84

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
