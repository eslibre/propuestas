---
layout: 2023/post
section: proposals
category: talks
state: canceled
author: Miguel Ángel Fernández Sánchez
title: 7 métricas para complementar la evaluación de riesgos de tu proyecto de Software Libre
---

# 7 métricas para complementar la evaluación de riesgos de tu proyecto de Software Libre

>Para evaluar el riesgo de un proyecto es común usar herramientas como los modelos de inventario, pero estas aproximaciones suelen dejar de lado otras facetas que son clave a la hora de realizar esta evaluación. En esta charla proponemos 7 métricas basadas en la actividad del proyecto y sus dependencias para poder complementar dicha evaluación; tales como el número de organizaciones involucradas en el mantenimiento, el riesgo de cada proyecto de perder contribuidores activos y medidas de eficiencia a la hora de resolver errores y proposiciones de cambios. Mediante el uso de estas métricas, los desarrolladores de software y los expertos en seguridad pueden tomar decisiones mejor informadas sobre qué bibliotecas de código abierto utilizar y cómo gestionar mejor los posibles riesgos que puedan surgir.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla larga / Presencial
-   Idioma: Español

-   Descripción:

>Las dependencias son uno de los mayores factores a analizar a la hora de evaluar el riesgo en un proyecto. Para evaluar dicho riesgo, se suelen elaborar listas como el SBOM (Software Build of Materials), describiendo cada componente usado en el proyecto. No obstante, este tipo de herramientas otorgan un punto de vista muy limitado del problema y de las causas que pueden llevar a que un proyecto sufra vulnerabilidades o que se reduzca su viabilidad.<br><br>
En esta charla se propone una manera menos común para complementar esta información: definimos 7 métricas que sirven para evaluar el riesgo de nuestro proyecto de software libre y de sus dependencias desde perspectivas diferentes, tales como el número de organizaciones involucradas en el mantenimiento, el riesgo de cada proyecto de perder contribuidores activos y medidas de eficiencia a la hora de resolver errores y proposiciones de cambios. Mediante el uso de estas métricas, los desarrolladores de software y los expertos en seguridad pueden tomar decisiones mejor informadas sobre qué bibliotecas de código abierto utilizar y cómo gestionar mejor los posibles riesgos que puedan surgir.<br><br>
Para obtener estas métricas, usamos GrimoireLab, una herramienta libre dedicada a analizar procesos de desarrollo de software y que forma parte del proyecto CHAOSS de la Fundación Linux.

-   Web del proyecto: <https://chaoss.github.io/grimoirelab/>

-   Público objetivo:

>Cualquier persona involucrada en proyectos de Software Libre que esté interesada en el mantenimiento y la seguridad en dichos proyectos.

## Ponente:

-   Nombre: Miguel Ángel Fernández Sánchez

-   Bio:

>Soy consultor y analista de datos en Bitergia, y tengo más de 5 años de experiencia trabajando con proyectos de software libre y código abierto. Actualmente, mi trabajo se centra en ayudar a estos proyectos a elaborar una estrategia de métricas que les ayude a completar sus objetivos dentro de los mismos y de sus comunidades.<br><br>
Recientemente he terminado un Máster en ciencia de datos, y también participo en GrimoireLab, parte del proyecto CHAOSS y anteriormente en el departamento de LibreSoft de la Universidad Rey Juan Carlos.

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
