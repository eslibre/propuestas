---
layout: 2023/post
section: proposals
category: talks
author: Pablo Garaizar Sagarminaga
title: La importancia del Software Libre en el ámbito escolar
---

### Redirigiendo a la página de la sala...

[Acceder directamente](https://propuestas-vitalinux.eslib.re/2023/charlas/importancia-software-libre-ambito-escolar)

<script>
  document.body.onload = function() {
    window.location = "https://propuestas-vitalinux.eslib.re/2023/charlas/importancia-software-libre-ambito-escolar";
  }
</script>
