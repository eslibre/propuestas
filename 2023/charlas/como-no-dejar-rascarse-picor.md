---
layout: 2023/post
section: proposals
category: talks
author: JJ Merelo
title: Cómo no dejar de rascarse el picor en 10 cómodos pasos
---

# Cómo no dejar de rascarse el picor en 10 cómodos pasos

>El proceso que lleva a liberar una aplicación o biblioteca se ha descrito muchas veces como "rascarse un picor" (*scratch an itch*). Pero el software libre empieza cuando ya ha dejado de picar porque ya has resuelto el problema y has decidido publicar la biblioteca o módulo o distribución en algún repositorio para el lenguaje de programación (como CRAN, CPAN, pypi, npm, REA o "marketplace" (como Docker hub o GitHub Action Marketplace). Sigue cuando al final resulta que hay más de una persona que tenía el mismo picor y empiezas a tener usuarios de verdad. ¿Cómo nos las podemos apañar entonces?

## Detalles de la propuesta:

-   Tipo de propuesta: Charla larga / Presencial
-   Idioma: Español

-   Descripción:

>En esta charla trataremos, a través de la experiencia, de contar cómo organizarse para mantener algunas docenas de proyectos de software libre publicados en diferentes repositorios y no perder la vida y la salud mental en el intento; a través de una serie de buenas prácticas, herramientas y trucos que son exactamente 10 (más o menos 2 o tres, según lo vea cuando dé la charla) veremos cómo intentar mantener a la comunidad contenta y bien servida, cómo mantener el código a punto y seguro, y cómo conseguir, finalmente, que el picor que ha dejado de picar no se convierta en muchos otros picores que al final acaben picando más de la cuenta. Porque comer y rascar, todo es empezar.

-   Web del proyecto: <https://github.com/JJ>

-   Público objetivo:

>Gente que desarrolle de cualquier nivel; especialmente nivel intermedio.

<p style="padding-top: 5px; text-align: center; font-size: 1.20rem;"><strong>La asistencia al evento es libre y gratuita, pero por cuestiones de aforo<br> necesitamos que te registres si tienes pensado asistir a las actividades.</strong></p>

-   Horario: sábado 13 mayo 16:30-17:00
-   Registro de asistentes al congreso: <https://eventos.librelabgrx.cc/events/798b67b2-21d2-4811-8201-7e1e14c69458>

## Ponente:

-   Nombre: JJ Merelo

-   Bio:

>JJ Merelo es desarrollador de software, especialmente en Raku y profe en la UGR. También venetófilo.

### Info personal:

-   Web personal: <https://jj.github.io>
-   Twitter: <https://twitter.com/jjmerelo>
-   GitLab (u otra forja) o portfolio general: <https://github.com/JJ>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
