---
layout: 2023/post
section: proposals
category: talks
author: Álvaro Peña
title: Un tour Linux embebido con Yocto y algunas buenas prácticas
---

# Un tour Linux embebido con Yocto y algunas buenas prácticas

>Ni Linux en embebidos ni Yocto son proyectos nuevos, pero trataré de hacer un recorrido introductorio e intermedio sobre qué es Yocto y la fórmula que desde mi punto de vista le está dando una relevancia bastante masiva entre desarrolladores y fabricantes.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla larga / Presencial
-   Idioma: Español

-   Descripción:

>   - Introducción al proyecto Yocto y OpenEmbedded<br>
   - Cómo y dónde podemos aplicar este sistema de construcción<br>
   - Integración continua desde el minuto cero<br>
   - Gestión de capas de fabricantes<br>
   - Control de build reproducibles<br>
   - Información sobre la cadena de suministro para cumplir con la legislación<br>
   - Informes de seguridad

-   Web del proyecto: <https://www.yoctoproject.org/>

-   Público objetivo:

>Integradores y desarrolladores de software de Linux embebido y público en general interesado en estos temas.

<p style="padding-top: 5px; text-align: center; font-size: 1.20rem;"><strong>La asistencia al evento es libre y gratuita, pero por cuestiones de aforo<br> necesitamos que te registres si tienes pensado asistir a las actividades.</strong></p>

-   Horario: sábado 13 mayo 17:00-17:30
-   Registro de asistentes al congreso: <https://eventos.librelabgrx.cc/events/798b67b2-21d2-4811-8201-7e1e14c69458>

## Ponente:

-   Nombre: Álvaro Peña

-   Bio:

>Álvaro Peña es desarrollar de software embebidos desde hace más de diez años y en el entorno de Linux y Software Libre desde hace más de 20 años. Actualmente trabaja en Telefónica en la fabricación de los STB de Movistar, pero antes ha formado parte de empresas del entorno Linux como ESware Linux u OpenShine, entre otras.<br><br>
Aunque actualmente mi interés es Linux en dispositivos embebidos, tabletas y móviles, siempre he sido fan y colaborador de GNOME y miembro de GNOME Hispano, además de otras asociaciones de usuarios de Linux.

### Info personal:

-   Web personal: <https://alvaropg.org/>
-   Mastodon (u otras redes sociales libres): <https://fosstodon.org/@alvaropg>
-   Twitter: <https://twitter.com/@alvaropg>
-   GitLab (u otra forja) o portfolio general: <https://github.com/alvaropg/>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
