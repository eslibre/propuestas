---
layout: 2023/post
section: proposals
category: talks
author: Laura M. Castro
title: David vs. Goliat&#58; lecciones aprendidas de una experiencia fallida de adopción de Cryptpad
---

# David vs. Goliat: lecciones aprendidas de una experiencia fallida de adopción de Cryptpad

>Cryptpad (<https://cryptpad.org>) es una alternativa a plataformas privativas de almacenamiento y edición colaborativa en la nube de documentos.<br><br>
En 2022, en una pequeña asociación cultural gallega (Semente Corunha) decidimos adoptar Cryptpad como herramienta. Lamentablemente, meses después tuvimos que reconocer nuestro fracaso.<br><br>
Esta charla pretende ilustrar el proceso seguido y las razones que condujeron al desenlace, desde un punto de vista constructivo, que pueda servir de inspiración y aprendizaje para la comunidad.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla larga / Presencial
-   Idioma: Español

-   Descripción:

>La creación y lanzamiento de productos libres llamados a competir con software privativo de gran presencia es un esfuerzo tan ingente como necesario, en ese camino imprescindible hacia la soberanía tecnológica y el empoderamiento de la ciudadanía.<br><br>
Es por ello que conocer experiencias de adopción, especialmente cuando no resultan exitosas pese a la voluntad de las personas usuarias, es imprescindible para detectar los problemas que se interponen entre el esfuerzo de las personas desarrolladoras y el éxito entre las usuarias.<br><br>
Dicho de otro modo, para que el tiempo y la dedicación (siempre limitados) que las personas con conocimientos técnicos invierten en la creación y mantenimiento de alternativas libres no caigan en saco roto, debemos estudiar por qué el software libre no consigue consolidar su uso, incluso entre grupos con voluntad de dar el paso hacia su adopción.<br><br>
En esta charla presentamos el fracaso del proceso de adopción de Cryptpad como herramienta de almacenamiento y edición de documentos en la nube por parte de la coordinación de una asociación cultural de pequeño tamaño.<br><br>
La asociación en cuestión, Semente Corunha, dedicada a la promoción y reconocimiento de la lengua gallega, con foco especialmente en la creación de espacios para su uso y conservación durante la primera infancia, cuenta con una directiva de 5 miembros y una base social de alrededor de 60 socias. Entre sus actividades se organizan excursiones, celebraciones tradicionales, talleres... para los que habitualmente se crean carteles que se difunden por redes sociales, formularios de inscripción para que las personas anticipen su interés, etc. Además de la "gestión externa", otras necesidades pasan por el mantenimiento de libros de socios, libros de cuentas, etc.<br><br>
Para estas necesidades, y en un contexto de creciente consciencia y preocupación por la seguridad y privacidad de los datos, Cryptpad aparece como una alternativa perfecta, que garantiza cifrado de las comunicaciones y su almacenamiento dentro de la Unión Europea.<br><br>
No obstante, la experiencia durante varios meses revela reveló diferentes problemáticas que condujeron, tristemente, al desestimiento en favor de una alternativa privativa de presencia casi monopólica. Algunas de ellas fueron, por ejemplo, la dificultad de uso en dispositivos móviles, que en muchos casos son la plataforma habitual de navegación de la población, que no dispone de portátil u ordenador de sobremesa, o lo comparte con otros miembros de su familia. También encontramos restricciones bien intencionadas, como por ejemplo la limitación de cumplimentación de formularios a uno por IP, que impide que una persona se registre a sí misma y a una conocida en una actividad de interés, o que puedan hacerlo personas accediendo desde una biblioteca o centro cívico.<br><br>
En resumen, esta charla pretende ser una reivindicación y visibilización de los esfuerzos de la población consciente por empoderarse y utilizar herramientas libres, y una reflexión constructiva para la comunidad de desarrollo sobre las necesidades y las circunstancias que rodean y condicionan el proceso último de adopción del software libre.

-   Web del proyecto: <https://sementecorunha.gal>

-   Público objetivo:

>Cualquier persona interesada en el software libre y su adopción, desde usuarias a desarrolladoras, y también gestoras y divulgadoras.

<p style="padding-top: 5px; text-align: center; font-size: 1.20rem;"><strong>La asistencia al evento es libre y gratuita, pero por cuestiones de aforo<br> necesitamos que te registres si tienes pensado asistir a las actividades.</strong></p>

-   Horario: viernes 12 mayo 19:45-20:10
-   Registro de asistentes al congreso: <https://eventos.librelabgrx.cc/events/798b67b2-21d2-4811-8201-7e1e14c69458>

## Ponente:

-   Nombre: Laura M. Castro

-   Bio:

>Ingeniera y doctora en informática, profesora e investigadora en la Universidade da Coruña, directora de la Cátedra CICAS por el Impulso a la Ciencia Abierta a través del Software, divulgadora en favor del software libre. Miembro de GPUL (Grupo de Usuarios y Programadores Linux) de la Facultad de Informática de A Coruña.

### Info personal:

-   Web personal: <https://lauramcastro.github.io>
-   Mastodon (u otras redes sociales libres): <https://fosstodon.org/@lauramcastro>
-   Twitter: <https://twitter.com/@lauramcastro>
-   GitLab (u otra forja) o portfolio general: <https://gitlab.com/lauramcastro>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
