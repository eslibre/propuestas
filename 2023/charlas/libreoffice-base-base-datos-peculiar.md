---
layout: 2023/post
section: proposals
category: talks
author: Juan C. Sanz
title: LibreOffice Base, una base de datos peculiar
---

# LibreOffice Base, una base de datos peculiar

>LibreOffice Base es una base de datos diferente de la mayoría de los gestores de bases de datos normales porque, por un lado admite bases de datos embebidas, bases de datos de archivo y bases de datos de servidor. Por otro lado, no está sujeta a un motor de bases de datos específico, sino que puede manejar diversos motores de bases de datos (HSQLDB, Firebird, MySQL, PostgreSQL, MariaDb...) y además, a muchos de esos motores de bases de datos se puede conectar de varias formas

## Detalles de la propuesta:

-   Tipo de propuesta: Charla larga / Presencial
-   Idioma: Español

-   Descripción:

>Se explicarán los diferentes tipos de bases de datos, en cuanto a localización que puede manejar Base (bases de datos internas, bases de datos de archivo y bases de datos de servido) explicando las relaciones de base con cada una de esas bases de datos.<br><br>
Se explica cada uno de los tipos y como se puede conectar LibreOffice Base con ellos y las ventajas e inconvenientes de cada uno de ellos. Y al finas se explican las ventajas de usar Base en la conexión con las bases de datos de servidor, frente a usar esas bases de datos directamente.

-   Público objetivo:

>Cualquiera interesado en bases de datos y en LibreOffice

<p style="padding-top: 5px; text-align: center; font-size: 1.20rem;"><strong>La asistencia al evento es libre y gratuita, pero por cuestiones de aforo<br> necesitamos que te registres si tienes pensado asistir a las actividades.</strong></p>

-   Horario: viernes 12 mayo 12:00-12:30
-   Registro de asistentes al congreso: <https://eventos.librelabgrx.cc/events/798b67b2-21d2-4811-8201-7e1e14c69458>

## Ponente:

-   Nombre: Juan C. Sanz

-   Bio:

>- Técnico especialista en informática de gestión (Titulación MEC).
- Certified Professional Trainer de LibreOffice
- Colaborador de LibreOffice desde los inicios y miembro de The Document Foundation, antes colaborador de OpenOffice
- He trabajado como programador de aplicaciones en las varias empresas y como responsable de informática (más de 40 PC y servidores) en un organismo público

### Info personal:

-   GitLab (u otra forja) o portfolio general: <https://easymariadb.github.io/>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
