---
layout: 2023/post
section: proposals
category: talks
author: Sergio López Pascual
title: Un pingüino en tu coche
---

# Un pingüino en tu coche

>Tras conquistar el mercado de los servidores y los smartphones, ha llegado el momento de que Tux emprenda nueva aventura colándose en el salpicadero de tu (futuro) coche.<br><br>
En esta charla veremos los retos que plantea el uso de Linux en un entorno tan crítico y exigente como son los ordenadores de abordo de un automóvil, así como el trabajo que se está realizando en varios proyectos de Software Libre (podman, QEMU, CentOS...) para estar a la altura de dichos retos.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla larga / Presencial
-   Idioma: Español

-   Descripción:

>Tras conquistar el mercado de los servidores y los smartphones, ha llegado el momento de que Tux emprenda una nueva aventura colándose en el salpicadero de tu (futuro) coche.<br><br>
Todo el software que se ejecuta en un ordenador de abordo debe superar una serie de certificaciones requeridas por la industria del automóvil, garantizar la disponibilidad inmediata de los recursos para los procesos críticos que los demanden, así como disponer de mecanismos de monitorización y recuperación.<br><br>
Por otro lado, la interacción con el resto de elementos del vehículo presenta un escenario único en su naturaleza y la necesidad de ajustar algunas expectativas y flujos de trabajo del sistema.<br><br>
En esta charla veremos los retos que plantea el uso de Linux en un entorno tan crítico y exigente como son los ordenadores de abordo de un automóvil, así como el trabajo que se está realizando en varios proyectos de Software Libre (podman, QEMU, CentOS...) para estar a la altura de dichos retos.

-   Web del proyecto: <https://sigs.centos.org/automotive/>

<p style="padding-top: 5px; text-align: center; font-size: 1.20rem;"><strong>La asistencia al evento es libre y gratuita, pero por cuestiones de aforo<br> necesitamos que te registres si tienes pensado asistir a las actividades.</strong></p>

-   Horario: sábado 13 mayo 17:30-18:00
-   Registro de asistentes al congreso: <https://eventos.librelabgrx.cc/events/798b67b2-21d2-4811-8201-7e1e14c69458>

## Ponente:

-   Nombre: Sergio López Pascual

-   Bio:

>Sergio es Principal Software Engineer en Red Hat especializado en Virtualización y Confidential Computing, y actualmente forma parte del equipo de Automotive Digital Cockpit. Ha contribuido a múltiples proyectos de Software Libre y es mantenedor de libkrun, krunvm y qemu-microvm.

### Info personal:

-   Web personal: <https://sinrega.org>
-   Mastodon (u otras redes sociales libres): <https://fosstodon.org/@slp>
-   Twitter: <https://twitter.com/slpnix>
-   GitLab (u otra forja) o portfolio general: <https://github.com/slp>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
