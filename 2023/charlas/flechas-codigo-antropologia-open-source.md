---
layout: 2023/post
section: proposals
category: talks
author: David de Torres Huerta
title: De flechas a código&#58; ¿Qué nos puede enseñar la antropología de las comunidades open source?
---

# De flechas a código: ¿Qué nos puede enseñar la antropología de las comunidades open source?

>Cuando contribuimos a un proyecto de código abierto, generalmente no vemos más allá de los issues, pull requests e innumerables líneas de código y documentación. Pero detrás de la pantalla, no somos más que humanos. Los mismos humanos que la Antropología viene estudiando a lo largo de todo el mundo en diferentes contextos y sociedades.<br><br>
En esta charla, David presentará algunos estudios de Antropología social y cultural, haciendo un recorrido por diferentes comunidades humanas (desde cazadores-recolectores hasta sociedades industriales) y encontrando analogías y reflexiones sobre las comunidades open source actuales.<br><br>
¿Quieres conocer más ejemplos? Esta charla tiene como objetivo ayudarnos a comprender mejor a los primates sentados al otro lado de la pantalla y cuidar mejor a nuestras comunidades.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla larga / Presencial
-   Idioma: Español

-   Descripción:

>A los programadores nos encanta programar. Pero cuando alguien inicia un nuevo proyecto de código abierto, el acto de programar se convierte en una actividad social. Si el proyecto atrae e involucra a suficientes personas, puede surgir una nueva comunidad, y gestionarlas es algo totalmente diferente a la programación.<br><br>
No hay dos proyectos de código abierto, con sus comunidades, que sean iguales, pero podemos ver cómo aparecen patrones similares en circunstancias similares. Esto también sucede fuera del mundo del código abierto. Afortunadamente, la Antropología lleva mucho tiempo estudiando las sociedades humanas y tanto la Antropología Política como la Económica se han especializado en documentar y describir cómo los diferentes grupos humanos se organizan para resolver diferentes problemas.<br><br>
En esta charla, cualquiera que participe en comunidades de código abierto podrá ver las diferencias entre comunidades y proyectos. Comprender los roles y motivaciones de los diferentes grupos en una comunidad puede ayudarnos a crear proyectos más saludables y mejores interacciones entre los miembros de nuestras comunidades.

-   Público objetivo:

>Cualquier persona que pertenezca a un proyecto de código abierto o que esté manteniendo una comunidad open source.

<p style="padding-top: 5px; text-align: center; font-size: 1.20rem;"><strong>La asistencia al evento es libre y gratuita, pero por cuestiones de aforo<br> necesitamos que te registres si tienes pensado asistir a las actividades.</strong></p>

-   Horario: viernes 12 mayo 13:30-14:00
-   Registro de asistentes al congreso: <https://eventos.librelabgrx.cc/events/798b67b2-21d2-4811-8201-7e1e14c69458>

## Ponente:

-   Nombre: David de Torres Huerta

-   Bio:

>David tiene más de 20 años de experiencia en tecnología e informática. Actualmente, es manager de ingeniería en Sysdig, y antes fue CTO en una empresa de IoT en el ámbito de la medición de energía. Es ingeniero informático y colaborador en proyectos de código abierto, apasionado por el estudio de la antropología, el blues y la creación de videojuegos.

### Info personal:

-   Mastodon (u otras redes sociales libres): <https://mastodon.social/@maellyssa>
-   Twitter: <https://twitter.com/@maellyssa>
-   GitLab (u otra forja) o portfolio general: <https://github.com/daviddetorres>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
