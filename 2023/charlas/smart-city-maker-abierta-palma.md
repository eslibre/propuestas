---
layout: 2023/post
section: proposals
category: talks
author: Carlos Orts
title: La “smart city” maker y abierta de Palma construida enseñando IoT a los ciudadanos
---

# La “smart city” maker y abierta de Palma construida enseñando IoT a los ciudadanos

>De la iniciativa de los vecinos de un barrio de Palma de tener una red de sensores medioambientales. Nació una plataforma Smartcity abierta y autogestionada que hoy en día monitoriza, registra y alerta:  la calidad del aire, y la contaminación sonora y radiactiva.<br><br>

## Detalles de la propuesta:

-   Tipo de propuesta: Charla larga / Presencial
-   Idioma: Español

-   Descripción:

>Cuando la Asociación de Vecinos del barrio de Palma de Canamunt se puso en contacto con la Asociación de Makers de Mallorca para ayudarle en su problema de ruido nocturno. Nació un modelo colaborativo de proyectos tecnológicos donde la Ciencia Ciudadana acaba desarrollando servicios reales que suponen una mejora en el ámbito de la sociedad<br><br>
¿Y cómo funciona esto?<br><br>
Partimos de una inquietud o necesidad de los ciudadanos a la que se le busca una solución técnica y novedosa. Esto involucra directamente al movimiento Maker compuesto por excelentes profesionales en varios ámbitos tecnológicos.
Luego se ajustan los costes de materiales y equipos con una dotación mínima y la ayuda desinteresada de expertos y espacios de fabricación digital como el FabLab de Mallorca.<br><br>
Con todo esto se organizan talleres donde, además de formar en nuevas tecnologías, construimos los sensores, las aplicaciones y lo necesario para desplegar una solución que finalmente, con la ayuda de los vecinos y empresas colaboradoras, desplegamos y utilizamos. Esto es verdadera Ciencia Ciudadana para construir verdaderas Smart Cities.<br><br>
¿Para qué la utilizamos?<br><br>
Fum Al Port: nació de la preocupación de los vecinos del puerto de Palma por la contaminación producida por los grandes cruceros atracados en un puerto que forma parte de la propia ciudad. En este caso la plataforma monitoriza las condiciones meteorológicas y de contaminación. Para emitir alertas por Telegram, Twitter y e-mail para informar a la ciudadanía de riesgos a la salud en tiempo real.<br><br>
Se relaciona la información de los sensores con el tráfico marítimo para tratar de identificar los focos de la contaminación. Se hace un análisis predictivo para dar avisos de contaminación.<br><br>
Canamunt: este barrio cuenta ahora con una densa red de sensores que permiten que la plataforma envíe un mensaje matinal con un informe de la calidad ambiental de sus calles. Y avise a sus vecinos de dónde hay ruido o contaminación.<br><br>
La solución se ha documentado y diseñado con estándares profesionales para que pueda ser replicada en otros puertos y ciudades, siempre bajo licencia de código abierto tanto para software y hardware como para los datos. Por otra parte, todo el stack tecnológico, basado en MING (MQTT, InfluxDB, Node-RED y Grafana) que soporta la aplicación son herramientas libres como puedes ver en este diagrama de arquitectura: <https://github.com/McOrts/fumport/blob/main/img/FumPort_SystemArchutecture.png>.<br><br>
Las comunicaciones son soportadas por la red The Things Networks. Que es un proyecto internacional de infraestructuras de comunicación para IoT. El almacenamiento y la normalización de datos se hace gracias a la iniciativa de Sensor Community y empresas privadas como Sinay y MyShip Tracking están colaborando desinteresadamente.<br><br>

-   Web del proyecto: <https://github.com/McOrts/fumport/> / <http://fumport.de-a.org/ui> / <http://canamunt.mooo.com:48055/ui>

-   Público objetivo:

>Makers y aficionados al Do It Yourselft, interesados en SmartCity, aficionados al Internet de las Cosas (IoT), promotores del asociacionismo

<p style="padding-top: 5px; text-align: center; font-size: 1.20rem;"><strong>La asistencia al evento es libre y gratuita, pero por cuestiones de aforo<br> necesitamos que te registres si tienes pensado asistir a las actividades.</strong></p>

-   Horario: sábado 13 mayo 12:00-12:30
-   Registro de asistentes al congreso: <https://eventos.librelabgrx.cc/events/798b67b2-21d2-4811-8201-7e1e14c69458>

## Ponente:

-   Nombre: Carlos Orts

-   Bio:

>Soy un profesional senior de IT con formación de técnico superior en Telecomunicaciones y en Informática. Y en la naútica como Capitán.
Fundador e impulsor del movimiento Maker en Baleares. Realizo mensualmente talleres y charlas.

### Info personal:

-   Web personal: <https://www.linkedin.com/in/mcorts/> / <https://iot-foundations.org/>
-   Twitter: <https://twitter.com/@mcorts>
-   GitLab (u otra forja) o portfolio general: <https://github.com/mcorts>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
