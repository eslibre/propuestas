---
layout: 2023/post
section: proposals
category: talks
author: yopaseopor
title: OSMPoisMap y Tagcionario&#58; acercando las etiquetas al público general
---

### Redirigiendo a la página de la sala...

[Acceder directamente](https://propuestas-libregeo.eslib.re/2023/charlas/osmpoismap-tagcionario-acercando-etiquetas-publico-general)

<script>
  document.body.onload = function() {
    window.location = "https://propuestas-libregeo.eslib.re/2023/charlas/osmpoismap-tagcionario-acercando-etiquetas-publico-general";
  }
</script>
