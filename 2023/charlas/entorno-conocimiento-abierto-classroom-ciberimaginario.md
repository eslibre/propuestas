---
layout: 2023/post
section: proposals
category: talks
author: María del Carmen Gálvez de la Cuesta
title: Entorno de conocimiento y prácticas educativas abiertas&#58; Classroom Ciberimaginario
---

# Entorno de conocimiento y prácticas educativas abiertas: Classroom Ciberimaginario

>Classroom Ciberimaginario- Entorno de Conocimiento y Prácticas Educativas Abiertas, aloja contenidos y prácticas educativas abiertas (OEP) que permiten a los estudiantes de diversas titulaciones, beneficiarse de experiencias de aprendizaje que amplíen sus conocimientos sobre aspectos específicos de las materias, profundizando en ellos y acercándose a realidades profesionales concretas.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla corta / Remoto
-   Idioma: Español

-   Descripción:

>Partiendo de la Recomendación de la Unesco sobre REA (Recursos Educativos Abiertos), se han analizado los informes del Sistema Interno de Garantía de Calidad, y se han tomado los principales indicadores que se miden, los informes con las acciones de mejora y las encuestas de satisfacción, para seguidamente plantear el desarrollo de una metodología transversal de intervención didáctica basada en los principios del conocimiento abierto.<br><br>
Para medir el logro se utilizarán la mayoría de los indicadores del Sistema Interno de Garantía de calidad; así como el análisis extraído de los planes de mejora, en los que se observa de manera recurrente la necesidad de incrementar la satisfacción de los estudiantes con los recursos docentes.<br><br>
A lo largo del curso 2022/23 se ha iniciado la puesta en marcha del portal, aplicándolo a diversas asignaturas de Grados y Másteres de la Universidad Rey Juan Carlos, especialmente en el área de Comunicación. La propuesta pretende ahondar en la estructura del sitio, sus características, así como su configuración como espacio de conocimiento abierto disponible para los estudiantes de cualquier titulación.<br><br>
Classroom Ciberimaginario se ha desarrollado a través del entorno Open Source, Moodle, y todos sus contenidos se encuentran disponibles bajo la licencia Creative Commons Attribution 4.0 International (CC BY 4.0). Cualquier estudiante interesado puede acceder al contenido ofrecido en abierto y perteneciente a las asignaturas que se incluyen dentro del Proyecto de Innovación Docente en el que se incluye el portal, haciendo un uso libre del contenido ofrecido por los docentes.<br><br>
El proyecto se inserta dentro de la línea estratégica del Proyecto de Innovación Educativa del Grupo de Innovación Docente - COMTEDEA  de establecer espacios de aprendizaje abierto que puedan implementarse en diferentes tipos de asignatura.<br><br>
AUTORES: María del Carmen Gálvez de la Cuesta, Manuel Gértrudix Barrio, Mario Rajas Fernández, María del Carmen Gertrudis Casado, Miguel Baños, Luis Matosas López, Ernesto José Taborda Hernández, Rubén Arcos Martín, José Luis Rubio Tamayo, Juan Romero Luis y Alejandro Carbonell Alcocer.

-   Web del proyecto: <https://www.learn.ciberimaginario.es/course/view.php?id=9>

-   Público objetivo:

>Profesionales de la educación comprometidos con el conocimiento abierto y la puesta a disposición de este a la sociedad.

<p style="padding-top: 5px; text-align: center; font-size: 1.20rem;"><strong>La asistencia al evento es libre y gratuita, pero por cuestiones de aforo<br> necesitamos que te registres si tienes pensado asistir a las actividades.</strong></p>

-   Horario: viernes 12 mayo 10:30-10:45
-   Registro de asistentes al congreso: <https://eventos.librelabgrx.cc/events/798b67b2-21d2-4811-8201-7e1e14c69458>

## Ponente:

-   Nombre: María del Carmen Gálvez de la Cuesta

-   Bio:

>Profesora de Comunicación Digital en la Facultad de Ciencias de la Comunicación de la Universidad Rey Juan Carlos. Miembro del grupo de investigación CIBERIMAGINARIO y del Grupo de Innovación Docente COMTEDEA. Intereses académicos en torno al Conocimiento Abierto, la generación de Contenidos Digitales Abiertos, la Alfabetización Mediática, la Comunicación Digital en sus diversos marcos, y la Competencia Digital en el uso de las tecnologías en los procesos de enseñanza-aprendizaje.

### Info personal:

-   Web personal: <https://ciberimaginario.es/grupo-de-innovacion-docente/>
-   Mastodon (u otras redes sociales libres): <https://universeodon.com/@mcarmengalvez>
-   Twitter: <https://twitter.com/@mcarmengalvez>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
