---
layout: 2023/post
section: proposals
category: talks
author: Sergio Salgado
title: La experiencia de Xnet con el entorno virtual de aprendizaje DD para una educación digital democrática y abierta
---

### Redirigiendo a la página de la sala...

[Acceder directamente](https://propuestas-vitalinux.eslib.re/2023/charlas/experiencia-xnet-entorno-virtual-aprendizaje-dd)

<script>
  document.body.onload = function() {
    window.location = "https://propuestas-vitalinux.eslib.re/2023/charlas/experiencia-xnet-entorno-virtual-aprendizaje-dd";
  }
</script>
