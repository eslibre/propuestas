---
layout: 2023/post
section: proposals
category: talks
author: Miguel Ángel Rodríguez Muíños
title: EpiLinux&#58; una distro orientada a la epidemiología y la bioestadística
---

### Redirigiendo a la página de la sala...

[Acceder directamente](https://propuestas-distros-linux.eslib.re/2023/charlas/epilinux-distro-orientada-epidemiologia-bioestadistica)

<script>
  document.body.onload = function() {
    window.location = "https://propuestas-distros-linux.eslib.re/2023/charlas/epilinux-distro-orientada-epidemiologia-bioestadistica";
  }
</script>
