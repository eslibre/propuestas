---
layout: 2023/post
section: proposals
category: talks
author: Paula de la Hoz
title: State of the art&#58; amenazas en Linux
---

# State of the art: amenazas en Linux

>La idea de la charla es repasar los ataque más relevantes de 2022 y primeros meses de 2023 que se han realizado contra varios tipos de sistemas de Linux. La charla pretende desmentir la idea de que Linux es seguro por defecto, pero al mismo tiempo proporcionar herramientas y conocimiento para afrontar las amenazas a las que se enfrenta.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla larga / Presencial
-   Idioma: Español

-   Descripción:

>La charla se presentaría en un formato accesible, pero incluyendo también detalles técnicos. Se analizarían las amenazas más comunes por un lado y las que han supuesto más impacto por otro. Se analizarán casos específicos, incluyendo una breve explicación de campañas descubertas a través de una honeypot personal. Explicaré cómo se investigan y correlacionan esta clase de ataques para mejorar la seguridad.

-   Público objetivo:

>SysAdmins de Linux interesados en seguridad o en general usuarios con interés técnico en ataques contra Linux.

## Ponente:

-   Nombre: Paula de la Hoz

-   Bio:

>Analista de ciberinteligencia, previamente Threat Hunter, IR y Experta en ciberseguridad ofensiva. Ha trabajado de profesora de hardware hacking y pentesting en el FP de ciberseguridad Salesianos Atocha (Madrid). Presidenta cofundadora de la asociación Interferencias. Colaboradora del programa de radio social Post Apocalipsis Nau.

<p style="padding-top: 5px; text-align: center; font-size: 1.20rem;"><strong>La asistencia al evento es libre y gratuita, pero por cuestiones de aforo<br> necesitamos que te registres si tienes pensado asistir a las actividades.</strong></p>

-   Horario: sábado 13 mayo 19:00-19:30
-   Registro de asistentes al congreso: <https://eventos.librelabgrx.cc/events/798b67b2-21d2-4811-8201-7e1e14c69458>

### Info personal:

-   Web personal: <https://www.inversealien.space>
-   Mastodon (u otras redes sociales libres): <https://mastodon.green/@alien>
-   GitLab (u otra forja) o portfolio general: <https://sr.ht/~alienagain/>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
