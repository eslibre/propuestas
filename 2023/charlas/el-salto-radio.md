---
layout: 2023/post
section: proposals
category: talks
author: Laura Corcuera
title: El Salto Radio
---

# El Salto Radio

> Presentación de cómo funciona el El Salto Radio.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla larga / Remoto
-   Idioma: Español

-   Descripción:

>Charla sobre EL SALTO RADIO (ESR), el área sonora de EL SALTO.
ESR comenzó una etapa nueva el 8 de marzo de 2023. Con la cobertura Especial en directo de las movilizaciones feministas del pasado 8M y con la antesala de haber cubierto en directo las elecciones generales de Brasil, ESR abrió el 8 de marzo de 2023  una emisora en directo que ya emite un hilo musical seleccionado por las curadoras sonoras de Caipirinha Libre. Se trata de un canal abierto a coberturas sonoras de streaming, donde también se emiten todos los podcasts y programas de EL SALTO RADIO. La emisora también replica una selección de podcast y programas amigas.  ESR funciona de forma asamblearia y horizontal. Se trata de una emisora colaborativa sostenida con servidores autónomos (RadioBot, Nodo50, Dirsoot y Sindominio) y producida con herramientas abiertas, libres y seguras (AzuraCast, MediaCast, OBS, BUTT, iRaddit, VoiceMeeter, Audacity, Nextcloud, pad Código Sur).

-   Web del proyecto: <https://www.elsaltodiario.com/el-salto-radio>

-   Público objetivo:

>Todas las personas interesadas en periodismo crítico y periodismo sonoro.

<p style="padding-top: 5px; text-align: center; font-size: 1.20rem;"><strong>La asistencia al evento es libre y gratuita, pero por cuestiones de aforo<br> necesitamos que te registres si tienes pensado asistir a las actividades.</strong></p>

-   Horario: sábado 13 mayo 13:30-14:00
-   Registro de asistentes al congreso: <https://eventos.librelabgrx.cc/events/798b67b2-21d2-4811-8201-7e1e14c69458>

## Ponente:

-   Nombre: Laura Corcuera

-   Bio:

>Laura Corcuera (Zaragoza, 1979) es periodista, escritora y artista, residente entre Brasil y España. Activista transfeminista de la soberanía tecnológica e informativa, es miembro del servidor autónomo Sindominio. Fue confundadora del periódico DIAGONAL y hoy del medio de comunicación EL SALTO, donde forma parte de EL SALTO RADIO. Compagina las prácticas comunicativas con las prácticas artísticas.

### Info personal:

-   Web personal: <https://lcorcuera.noblogs.org>
-   Mastodon (u otras redes sociales libres): <https://mastodon.social/@ElSaltoDiario>
-   Twitter: <https://twitter.com/@lcorcuera>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
