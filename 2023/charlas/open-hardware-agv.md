---
layout: 2023/post
section: proposals
category: talks
author: Miguel Martínez, Miguel Gámez
title: Acercando el Hardware Libre a la industria&#58; OH AGV
---

# Acercando el Hardware Libre a la industria: OH AGV

>Desarrollo de un vehículo autónomo Open Source de cara a mejorar la logística de pequeñas industrias. Es un proyecto muy nuevo que nació como un TFG hace año y medio pero que podría ser prometedor. Podría ser también una buena punta de lanza para introducir el Open Source Hardware en la industria.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla corta / Presencial
-   Idioma: Español

-   Descripción:

>El proyecto se ha dividido en tres fases principales. Una primera de diseño del vehículo, otra de construcción del mismo, y actualmente se pretende lanzar una tercera de diseño de software. Estas tres son independientes entre sí, es decir, han sido realizadas por personas diferentes.<br><br>
Nosotros nos centramos en el diseño del vehículo (la primera fase), y el allanar todo el terreno para las siguientes fases. Claramente en las posteriores fases se han realizado correcciones sobre nuestro trabajo, pero el enfoque que buscamos dar puede ir más relacionado a cómo comenzar de cero un trabajo Open Hardware, orientado a la industria y de gran magnitud, para que cada uno pueda hacer sus desarrollos personales customizados.<br><br>
Por tanto, como prototipo de charla, explicaría un poco nuestra forma de pensar a la par que íbamos desarrollando un proyecto que considero propiamente interesante.

-   Web del proyecto: <https://github.com/hibiscus22/OpenHardwareAGV>

-   Público objetivo:

>Supongo que este evento se puede llegar a centrar más en Software, entonces con esta suposición de que me puedo salir un poco de la norma, casi invitaría a cualquier curioso (interesado en robótica, por ejemplo) a asistir. Más aún porque es un proyecto muy joven.<br><br>
Evidentemente, el público objetivo también incluye a todo aquel que tenga contacto con la industria y pueda ver potencial a esta idea para implementarla en una fábrica.

<p style="padding-top: 5px; text-align: center; font-size: 1.20rem;"><strong>La asistencia al evento es libre y gratuita, pero por cuestiones de aforo<br> necesitamos que te registres si tienes pensado asistir a las actividades.</strong></p>

-   Horario: sábado 13 mayo 13:00-13:15
-   Registro de asistentes al congreso: <https://eventos.librelabgrx.cc/events/798b67b2-21d2-4811-8201-7e1e14c69458>

## Ponente:

-   Nombre: Miguel Martínez, Miguel Gámez

-   Bio:

>Somos dos recién licenciados en ingenierías diferentes. Nos conocimos en Suecia y cuando vimos que querían lanzar este proyecto, no dudamos en unirnos para llevarlo para delante.
Por esto mismo, no tenemos demasiado rodaje en desarrollo de Open Source, aunque es algo que nos interesa enormemente y motiva siempre.

### Info personal:

-   GitLab (u otra forja) o portfolio general: <https://github.com/hibiscus22/OpenHardwareAGV>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
