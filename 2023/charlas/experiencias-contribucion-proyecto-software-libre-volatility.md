---
layout: 2023/post
section: proposals
category: talks
author: Ricardo J. Rodríguez
title: Experiencias propias en la contribución a proyectos de Software Libre Volatility
---

# Experiencias propias en la contribución a proyectos de Software Libre Volatility

>Desde el grupo de investigación DisCo, del Departamento de Informática de Ingeniería de Sistemas de la Universidad de Zaragoza, llevamos más de 5 años contribuyendo al proyecto de software libre Volatility, que proporciona una entorno aceptado como estándar en el mundo del análisis forense de memoria. En la charla detallaremos cuál ha sido nuestro aporte al proyecto a lo largo de estos años, resumiendo nuestras contribuciones (tanto las oficiales como las no oficiales) al proyecto y todo lo que hemos aprendido durante este proceso. Además, resumiremos otras contribuciones que hemos hecho a la comunidad fuera de Volatility y también bajo licencia GNU/GPLv3. Todas nuestras contribuciones están accesibles en nuestro repositorio oficial de GitHub <https://github.com/reverseame>.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla larga / Presencial
-   Idioma: Español

-   Descripción:

>Desde el grupo de investigación DisCo, del Departamento de Informática de Ingeniería de Sistemas de la Universidad de Zaragoza, llevamos más de 5 años contribuyendo al proyecto de software libre Volatility, que proporciona una entorno aceptado como estándar en el mundo del análisis forense de memoria. En la charla detallaremos cuál ha sido nuestro aporte al proyecto a lo largo de estos años, resumiendo nuestras contribuciones (tanto las oficiales como las no oficiales) al proyecto y todo lo que hemos aprendido durante este proceso. Además, resumiremos otras contribuciones que hemos hecho a la comunidad fuera de Volatility y también bajo licencia GNU/GPLv3. Todas nuestras contribuciones están accesibles en nuestro repositorio oficial de GitHub <https://github.com/reverseame>.

-   Web del proyecto: <https://github.com/reverseame>

-   Público objetivo:

>Público técnico especializado y público general.

<p style="padding-top: 5px; text-align: center; font-size: 1.20rem;"><strong>La asistencia al evento es libre y gratuita, pero por cuestiones de aforo<br> necesitamos que te registres si tienes pensado asistir a las actividades.</strong></p>

-   Horario: sábado 13 mayo 11:00-11:30
-   Registro de asistentes al congreso: <https://eventos.librelabgrx.cc/events/798b67b2-21d2-4811-8201-7e1e14c69458>

## Ponente:

-   Nombre: Ricardo J. Rodríguez

-   Bio:

>Ricardo J. Rodríguez es Doctor en Informática e Ingeniería de Sistemas por la Universidad de Zaragoza desde 2013. Actualmente, trabaja como Profesor Titular en la misma universidad. Sus intereses de investigación incluyen el análisis de sistemas complejos, con especial énfasis en el rendimiento y su seguridad, el forense digital y el análisis de aplicaciones binarias. Participa como ponente habitual y profesor de talleres técnicos en numerosas conferencias de seguridad del sector industrial, como NoConName, Hack.LU, RootedCON, Hack in Paris, MalCON, SSTIC CCN-CERT, o Hack in the Box Amsterdam, entre otras. Lidera una línea de investigación dedicada a seguridad informática en la Universidad de Zaragoza (<https://reversea.me>).

### Info personal:

-   Web personal: <https://ricardojrdez.github.io/>
-   Twitter: <https://twitter.com/RicardoJRdez>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
