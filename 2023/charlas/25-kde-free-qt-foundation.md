---
layout: 2023/post
section: proposals
category: talks
author: Albert Astals Cid
title: 25 años de The KDE Free Qt Foundation&#58; como las comunidades de Software Libre y las empresas pueden funcionar juntas
---

# 25 años de The KDE Free Qt Foundation: como las comunidades de Software Libre y las empresas pueden funcionar juntas

>En esta charla hablaré sobre los 25 años de KDE Free Qt Foundation, una fundación que se estableció hace mucho tiempo para coordinar a KDE y los desarrolladores del Qt (el toolkit más utilizado por el software de KDE).<br><br>
Los desarrolladores de Qt también tienen una versión no libre de Qt, lo que hace que esta relación sea especial y la Fundación casi única.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla larga / Presencial
-   Idioma: Español

-   Descripción:

>En esta charla hablaré sobre los 25 años de KDE Free Qt Foundation, una fundación que se estableció hace mucho tiempo para coordinar a KDE y los desarrolladores del Qt (el toolkit más utilizado por el software de KDE).<br>
Los desarrolladores de Qt también tienen una versión no libre de Qt, lo que hace que esta relación sea especial y la Fundación casi única.

-   Web del proyecto: <https://kde.org/community/whatiskde/kdefreeqtfoundation/>

-   Público objetivo:

>Aquellos interesados en la historia del Software Libre y la gestión de comunidades de software Libre

<p style="padding-top: 5px; text-align: center; font-size: 1.20rem;"><strong>La asistencia al evento es libre y gratuita, pero por cuestiones de aforo<br> necesitamos que te registres si tienes pensado asistir a las actividades.</strong></p>

-   Horario: sábado 13 mayo 16:00-16:30
-   Registro de asistentes al congreso: <https://eventos.librelabgrx.cc/events/798b67b2-21d2-4811-8201-7e1e14c69458>

## Ponente:

-   Nombre: Albert Astals Cid

-   Bio:

>Albert ha estado involucrado en KDE durante casi dos décadas y recientemente se ha unido a The KDE Free Qt Foundation como presidente.

### Info personal:

-   Mastodon (u otras redes sociales libres): <https://fosstodon.org/@tsdgeos>
-   Twitter: <https://twitter.com/tsdgeos>
-   GitLab (u otra forja) o portfolio general: <https://invent.kde.org/aacid>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
