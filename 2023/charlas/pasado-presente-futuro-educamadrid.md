---
layout: 2023/post
section: proposals
category: talks
author: Adolfo Sanz De Diego
title: Pasado, presente y futuro de EducaMadrid
---

# Pasado, presente y futuro de EducaMadrid

>EducaMadrid es la Plataforma Educativa de la Comunidad de Madrid.<br><br>
Basada en Software Libre, en un entorno seguro y sostenible, ofrece múltiples servicios interconectados y complementarios: aulas virtuales, páginas web de centros, blogs de profesores, mediateca, nube, correo, videoconferencia, MAdrid_linuX, etc.<br><br>
La Pandemia primero, y la Competencia Digital Docente después, ha catapultado su uso en el ámbito educativo no Universitario.<br><br>
En esta charla quiero contar cómo empezamos, cómo crecimos, cómo es nuestro día a día, tanto a nivel técnico como a nivel funcional, y cómo veo su futuro.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla larga / Presencial
-   Idioma: Español

-   Descripción:

>EducaMadrid es la Plataforma Educativa de la Comunidad de Madrid.<br><br>
Basada en Software Libre, en un entorno seguro y sostenible, ofrece múltiples servicios interconectados y complementarios: aulas virtuales, páginas web de centros, blogs de profesores, mediateca, nube, correo, videoconferencia, MAdrid_linuX, etc.<br><br>
La Pandemia primero, y la Competencia Digital Docente después, ha catapultado su uso en el ámbito educativo no Universitario.<br><br>
En esta charla quiero contar cómo empezamos, cómo crecimos, cómo es nuestro día a día, tanto a nivel técnico como a nivel funcional, y cómo veo su futuro.

-   Web del proyecto: <https://www.educa2.madrid.org/educamadrid/>

-   Público objetivo:

>Público general.

<p style="padding-top: 5px; text-align: center; font-size: 1.20rem;"><strong>La asistencia al evento es libre y gratuita, pero por cuestiones de aforo<br> necesitamos que te registres si tienes pensado asistir a las actividades.</strong></p>

-   Horario:
-   Registro de asistentes al congreso: <https://eventos.librelabgrx.cc/events/798b67b2-21d2-4811-8201-7e1e14c69458>

## Ponente:

-   Nombre: Adolfo Sanz De Diego

-   Bio:

>Jefe de Servicio de Plataformas Educativas de la Comunidad de Madrid ([@EducaMadrid](https://twitter.com/educamadrid) y [@MAX_MAdridlinuX](https://twitter.com/max_madridlinux)). Colaboro además como profesor universitario y formador técnico.

### Info personal:

-   Web personal: <https://www.asanzdiego.com/>
-   Twitter: <https://twitter.com/asanzdiego>
-   GitLab (u otra forja) o portfolio general: <https://github.com/asanzdiego>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
