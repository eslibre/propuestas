---
layout: 2023/post
section: proposals
category: talks
author: Juan Rodriguez Esteban
title: Open source al rescate de la privacidad
---

# Open source al rescate de la privacidad

>Las grandes empresas privadas cada vez tienen acceso a mas de nuestros datos y lo menos que hacen con ellos es mandarnos anuncios dirigidos sobre nuestros gustos. La privacidad es un derecho fundamental y el OpenSource es el camino a seguir para recuperarla. Permiteme que en esta pequeña charla te exponga a que nos enfrentamos y que armas tenemos para defendernos.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla corta / Presencial
-   Idioma: Español

-   Descripción:

>El rapido avance de la tecnologia y la digitalizacion de cada vez mas sectores ha provocado que gran parte de nuestras vidas pase por una pantalla y no nos damos cuenta de las implicaciones que tiene.<br><br>
Poca gente a dia de hoy concibe salir de casa sin su telefono movil y realmente no se le da importancia a toda la informacion que ese pequeño dispositivo que hace fotos graciosas sabe de nosotros.<br><br>
Y ni si quiera en casa nos salvamos, ¿quien a dia de hoy no tiene un altavoz inteligente tipo alexa? Dispositivos creados con el proposito de estar escuchando de manera continua a la espera de una orden, pero ¿que estan haciendo con todo ese tiempo en que no le damos una orden? ¿que hacen con esas grabaciones “con fines de mejorar el servicio”?<br><br>
Vengo a presentar unas soluciones de codigo libre que nos van a permitir retomar el control de nuestros datos, desde sistemas operativos y apliaciones hasta redes sociales, pasando por unas buenas practicas generales.

<p style="padding-top: 5px; text-align: center; font-size: 1.20rem;"><strong>La asistencia al evento es libre y gratuita, pero por cuestiones de aforo<br> necesitamos que te registres si tienes pensado asistir a las actividades.</strong></p>

-   Horario: sábado 13 mayo 13:15-13:30
-   Registro de asistentes al congreso: <https://eventos.librelabgrx.cc/events/798b67b2-21d2-4811-8201-7e1e14c69458>

## Ponente:

-   Nombre: Juan Rodriguez Esteban

-   Bio:

>Soy un estudiante de Ingenieria Electronica y Automatica en segundo año. He participado en varios voluntariados relacionados con la ingenieria, tecnologia y robotica y estoy muy interesado en el sector. Yo mismo predico con el ejemplo, uso a diario lineageOS en el movil, tengo dos servidores para abordar mi parte del "selfhost" y dualboot windows/linux en mi ordenador. Ademas de seguir a personalidades como Edward Snowden y Linus Torvald intento transmitir su mensaje a amigos y conocidos.

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
