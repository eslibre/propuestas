---
layout: 2023/post
section: proposals
category: talks
author: Antonio-Paulo Ubieto Artur
title: Sostenibilidad “4R” y enseñanza de con Software Libre en el “Grado en Información y Documentación” de la Universidad de Zaragoza ((1992-)2008-2022
---

# Sostenibilidad “4R” y enseñanza de con Software Libre en el “Grado en Información y Documentación” de la Universidad de Zaragoza ((1992-)2008-2022

>Se comparten experiencias en dos campos relacionados:<br><br>
- Sostenibilidad “4R” (“Reutilizar, Reparar, Reciclar, Reducir”) recuperando Ordenadores Personales (PCs) sin mantenimiento oficial,prolongando su vida útil y evitando compras innecesarias.<br><br>
- Enseñanza de asignaturas del Grado mediante aplicaciones de software libre en los PCs recuperados más potentes como servidores LAMP (Linux-Apache-MySQL-PHP).<br><br>
Muchas asignaturas del Grado requieren prácticas con aplicaciones del mundo laboral real. La alta calidad y propiedades didácticas son características de muchas aplicaciones de software libre. Sin costo de adquisición –y posibilidad de ejecución en PCs reciclados– estas aplicaciones son accesibles para profesorado y alumnado.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla larga / Presencial
-   Idioma: Español

-   Descripción:

>El autor pretende compartir sus experiencias en los citados Grado y Universidad como docente e investigador durante el periodo 1992-2022 en dos campos íntimamente ligados: 1) la sostenibilidad “4R” que pueden proporcionar PCs reutilizables alargando su vida útil sin soporte técnico oficial, y 2) usar los PCs más potentes como servidores GNU/Linux para la enseñanza de asignaturas del Grado con alto contenido tecnológico mediante aplicaciones documentales de software libre. Esperemos que esta Ponencia sea útil más allá de la docencia en Universidades y sus estudiantes, ya que profesionales en continua formación podrán utilizarla para los continuos y necesarios autoestudio y autoformación en aplicaciones profesionales, siempre que éstas sean de Software Libre. En todos los casos facilitará la empleabilidad de estas personas.<br><br>
- Sostenibilidad “4R”, o “Reutilizar-Reparar-Reciclar-Reducir”<br><br>
Al principio de nuestra Docencia (primeros años 90), antes de la aparición de conceptos como “sostenibilidad” y “4R”, ya aprovechábamos ordenadores descartados para ahorrar unos recursos públicos siempre escasos. Estos ordenadores no aceptaban programas comerciales (como MS Windows NT), lo que nos llevó al software libre. Con el tiempo encontramos en organizaciones y literatura ecologistas referencias desde las “3R” hasta –al menos– las “7R”. Hemos escogido las siguientes “4R”: “Reutilizar, Reparar, Reciclar, Reducir”, que se desarrollarán en la Metodología y los Resultados “4R”.
Los ordenadores recuperados más potentes se han venido utilizando como servidores con software libre GNU/Linux para la Enseñanza. Para otros ordenadores algo menos potentes, pero en buen uso, se evalúan sus posibles ampliaciones de memoria y se han ido rotando entre docentes que tuvieran ordenadores más antiguos.<br><br>
- Uso de software libre<br><br>
En nuestros estudios usamos software libre tanto de servidor GNU/Linux como de cliente, mayoritariamente sobre MS Windows. En el primer caso se consigue rendimientos óptimos con ordenadores reutilizados, en el segundo se facilita a profesorado y alumnado el uso de software gratuito que se puede descargar y usar libremente sin pago de licencias, a diferencia del software privativo, que obligaría a docentes y discentes al correspondiente desembolso económico, aumentando la brecha digital para alumnado de menores recursos económicos. Esto se hace en dos etapas:<br><br>
    - Años 1992-2008 (Diplomatura en Biblioteconomía y Documentación, etapa anterior al Grado, pero necesaria para comprender sus antecedentes): se usó exclusivamente software libre para mejorar la Docencia del autor, desde 1992 software de escritorio para acceder a Internet desde un solo ordenador del aula, y desde 1995 instalando y accediendo a un primer servidor GNU/Linux. Con ello, este profesor no arriesga la Docencia de sus colegas y adquiere la experiencia necesaria para minimizar riesgos de terceras personas. En esta primera etapa se cubrieron básicamente las antiguas “Iniciación a la Informática” y “Documentación Automatizada”. Algo más reciente es la “Edición Digital”, donde se incluye una introducción práctica a la modificación y creación de sencillas páginas web.<br><br>
   - Año 2008-2022: con el Grado crecen las necesidades docentes y se ofrece el servidor GNU/Linux a asignaturas nuevas propias y de colegas, cada vez más tecnológicas: “Sistemas Integrados para Unidades de Información” (que incluye programas de Bibliotecas y Archivos, entre otros), “Catalogación” e “Indización”, “Descripción de Documentos de Archivos”. Esta etapa se vio apoyada por un “Programa de Incentivación de la Innovación Docente” (PIIDUZ_12_1_286) de la Universidad de Zaragoza. El alumnado utiliza aplicaciones de software libre –especialmente el documental– usados en la vida profesional. Algunas de estas aplicaciones han sido instaladas, personalizadas y administradas por el alumnado en sus cuentas personales en el servidor GNU/Linux, lo que –muy probablemente– no se le permita hacer en las prácticas externas. Esta política se complementa con la de otros colegas, que usarán otras aplicaciones de software libre en el hospedaje web ofrecido por la Universidad de Zaragoza, especialmente en la asignatura “Aplicaciones para recursos de información digital”.

-   Web del proyecto: <https://doi.org/10.37467/revtechno.v11.3794>

-   Público objetivo:

>Docentes y (auto)estudiantes de materias con componente tecnológico, y no solo de Información y Documentación. Este público podrá reaprovechar PCs en desuso para instalar software libre para (auto)aprendizaje de aplicaciones varias.

<p style="padding-top: 5px; text-align: center; font-size: 1.20rem;"><strong>La asistencia al evento es libre y gratuita, pero por cuestiones de aforo<br> necesitamos que te registres si tienes pensado asistir a las actividades.</strong></p>

-   Horario: viernes 12 mayo 10:30-11:00
-   Registro de asistentes al congreso: <https://eventos.librelabgrx.cc/events/798b67b2-21d2-4811-8201-7e1e14c69458>

## Ponente:

-   Nombre: Antonio-Paulo Ubieto Artur

-   Bio:

>Soy profesor del Área de Biblioteconomía y Documentación (Departamento de Ciencias de la Documentación e Historia de la Ciencia) desde el curso 1990-91 hasta el presente. En el presente Grado en Información y Documentación imparto "Edición Digital" y "Sistemas Integrados para Unidades de Información", hasta hace unos cursos impartí "Fundamentos de Bases de Datos". En todas estas asignaturas mi alumnado recibe acceso a los servidores GNU/Linux que administro en UniZar donde usa -y en ocasiones instala- aplicaciones web 2.0 de las asignaturas, todas ellas software libre. Más información en mi CV oficial y público en: https://janovas.unizar.es/sideral/CV/antonio-paulo-ubieto-artur

### Info personal:

-   Web personal: <http://franky.unizar.es/>
-   Twitter: <https://twitter.com/@apubieto>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
