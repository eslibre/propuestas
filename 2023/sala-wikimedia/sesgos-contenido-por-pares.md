---
layout: 2023/post
section: proposals
category: talks
author: David Abián
title: ¿Sesgos del contenido por pares?
---

# Toda la verdad sobre las brechas de contenido

>¿Es cierto que hay «brechas» o vacíos de contenido por género, factores socioeconómicos, ubicación, etc. en sitios web de producción por pares como Wikipedia, Wikidata y OpenStreetMap? Se responderá con rigor a estas y otras preguntas y se presentarán los resultados de una estrategia de análisis que no te esperas.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla larga / Presencial
-   Idioma: Español

-   Descripción:

>¿Es cierto que hay «brechas» o vacíos de contenido por género, factores socioeconómicos, ubicación, etc. en sitios web de producción por pares como Wikipedia, Wikidata y OpenStreetMap? Se responderá con rigor a estas y otras preguntas y se presentarán los resultados de una estrategia de análisis que no te esperas.

-   Web del proyecto: <https://www.wikimedia.es/>

-   Público objetivo:

>Personas que se dedican a la investigación académica, agentes e instituciones culturales, creadores, participantes en la comunidad del software libre, wikimedistas y todas las personas que tengan interés en los temas a tratar.

<p style="padding-top: 5px; text-align: center; font-size: 1.20rem;"><strong>La asistencia al evento es libre y gratuita, pero por cuestiones de aforo<br> necesitamos que te registres si tienes pensado asistir a las actividades.</strong></p>

-   Horario: viernes 12 mayo 16:30-17:00
-   Registro de asistentes al congreso: <https://eventos.librelabgrx.cc/events/798b67b2-21d2-4811-8201-7e1e14c69458>

## Ponente:

-   Nombre: David Abián

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
