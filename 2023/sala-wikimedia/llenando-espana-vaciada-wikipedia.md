---
layout: 2023/post
section: proposals
category: talks
author: Rubén Ojeda
title: Llenando la 'España vaciada' en Wikipedia
---

# Llenando la 'España vaciada' en Wikipedia

>En esta sesión expondremos las distintas iniciativas que se están llevando a cabo para reducir la brecha de contenidos sobre el medio rural, especialmente en el ámbito de la documentación fotográfica.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla larga / Presencial
-   Idioma: Español

-   Descripción:

>En esta sesión expondremos las distintas iniciativas que se están llevando a cabo para reducir la brecha de contenidos sobre el medio rural, especialmente en el ámbito de la documentación fotográfica.

-   Web del proyecto: <https://www.wikimedia.es/>

-   Público objetivo:

>Personas que se dedican a la investigación académica, agentes e instituciones culturales, creadores, participantes en la comunidad del software libre, wikimedistas y todas las personas que tengan interés en los temas a tratar.

<p style="padding-top: 5px; text-align: center; font-size: 1.20rem;"><strong>La asistencia al evento es libre y gratuita, pero por cuestiones de aforo<br> necesitamos que te registres si tienes pensado asistir a las actividades.</strong></p>

-   Horario: viernes 12 mayo 17:30-18:00
-   Registro de asistentes al congreso: <https://eventos.librelabgrx.cc/events/798b67b2-21d2-4811-8201-7e1e14c69458>

## Ponente:

-   Nombre: Rubén Ojeda

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
