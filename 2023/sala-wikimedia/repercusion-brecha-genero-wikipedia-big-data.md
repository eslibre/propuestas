---
layout: 2023/post
section: proposals
category: talks
author: Florencia Claes, Ester Bonet
title: La repercusión de la brecha de género de la Wikipedia en el 'Big Data'
---

# La repercusión de la brecha de género de la Wikipedia en el 'Big Data'

>La Wikipedia es uno de los grandes exponentes del procomún digital, se anuncia a sí misma como una enciclopedia basada en la neutralidad, la autoridad y el consenso. Pero ¿qué ocurre si esto no es cierto, si los datos que aporta la Wikipedia presentan brechas y sesgos de género?

## Detalles de la propuesta:

-   Tipo de propuesta: Charla larga / Presencial
-   Idioma: Español

-   Descripción:

>La Wikipedia es uno de los grandes exponentes del procomún digital, se anuncia a sí misma como una enciclopedia basada en la neutralidad, la autoridad y el consenso. Pero ¿qué ocurre si esto no es cierto, si los datos que aporta la Wikipedia presentan brechas y sesgos de género?

-   Web del proyecto: <https://www.wikimedia.es/>

-   Público objetivo:

>Personas que se dedican a la investigación académica, agentes e instituciones culturales, creadores, participantes en la comunidad del software libre, wikimedistas y todas las personas que tengan interés en los temas a tratar.

<p style="padding-top: 5px; text-align: center; font-size: 1.20rem;"><strong>La asistencia al evento es libre y gratuita, pero por cuestiones de aforo<br> necesitamos que te registres si tienes pensado asistir a las actividades.</strong></p>

-   Horario: viernes 12 mayo 17:00-17:30
-   Registro de asistentes al congreso: <https://eventos.librelabgrx.cc/events/798b67b2-21d2-4811-8201-7e1e14c69458>

## Ponente:

-   Nombre: Florencia Claes, Ester Bonet

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
