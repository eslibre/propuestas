---
layout: 2023/post
section: proposals
category: workshops
author: Wikimedia España
title: Wiki-salida por el recinto de la Expo 2008
---

# Wiki-salida por el recinto de la Expo 2008

>Salida por el recinto de la Expo 2008 para documentar el legado de la exposición 15 años después y subir posteriormente el material a Wikimedia Commons.

## Detalles de la propuesta:

-   Tipo de propuesta: Salida guiada / 90 minutos

-   Descripción:

>Salida por el recinto de la Expo 2008 para documentar el legado de la exposición 15 años después y subir posteriormente el material a Wikimedia Commons.

-   Web del proyecto: <https://www.wikimedia.es/>

-   Público objetivo:

>Personas que se dedican a la investigación académica, agentes e instituciones culturales, creadores, participantes en la comunidad del software libre, wikimedistas y todas las personas que tengan interés en los temas a tratar.

<p style="padding-top: 5px; text-align: center; font-size: 1.20rem;"><strong>Por favor, si tienes interés en esta salida apúntate para asegurar tu plaza y facilitarnos la gestión de su realización<br>(además de registrarte también en el congreso si tienes interés en participar en otras actividades).</strong></p>

-   Horario: Viernes 12 de mayo de 18:45 a 20:15.
-   Formulario de inscripción para la salida: <https://eventos.librelabgrx.cc/events/d70c1de6-da92-46a0-be0a-6ea52b221690>
-   Registro de asistentes al congreso: <https://eventos.librelabgrx.cc/events/798b67b2-21d2-4811-8201-7e1e14c69458>
## Organiza:

-   Nombre: Wikimedia España

-   Bio:

>Wikimedia España es el capítulo reconocido por la Fundación Wikimedia para operar en España. Somos una asociación sin ánimo de lucro y reconocida como entidad de utilidad pública que promueve el conocimiento libre y los proyectos Wikimedia, siendo Wikipedia el más conocido. Nos constituimos en 2011 y llevamos todos estos años trabajando para mejorar la accesibilidad, el uso y la participación en Wikipedia y sus proyectos hermanos tanto a nivel social como institucional.<br><br>
La visión del movimiento Wikimedia es conseguir un mundo en el que todas las personas tengan acceso libre a la suma del conocimiento y puedan participar en su construcción colectiva. Seguimos las filosofías del conocimiento y la cultura libres, que defienden el derecho fundamental de acceso a estos, y todos nuestros proyectos operan sobre plataformas de software libre.<br><br>
Todo el mundo tiene conocimiento para compartir. Desde escribir o mejorar un artículo en Wikipedia, a documentar manifestaciones culturales y subir las imágenes a Wikimedia Commons. Personas individuales, colectivos u organizaciones: todas pueden formar parte del ecosistema Wikimedia.

-   Web: <https://www.wikimedia.es>
-   Twitter: <https://twitter.com/@wikimedia_es>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
