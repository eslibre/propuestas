---
layout: 2023/post
section: proposals
category: talks
author: David Abián
title: ¿Sesgos del contenido por pares?
---

### Redirigiendo a la versión actualizada de la charla...

[Acceder directamente](https://propuestas.eslib.re/2023/sala-wikimedia/sesgos-contenido-por-pares)

<script>
  document.body.onload = function() {
    window.location = "https://propuestas.eslib.re/2023/sala-wikimedia/sesgos-contenido-por-pares";
  }
</script>
