---
layout: 2023/post
section: proposals
category: tables
author: VANT
title: VANT
---

## VANT

-   Web: <https://www.vantpc.es/>

>VANT es una marca de ordenadores personales especializados en GNU/Linux. Comprende una extensa gama de equipos, ofreciendo diferentes configuraciones y prestaciones, adaptadas al usuario.

-   Twitter: <https://twitter.com/vantpc>

## Condiciones aceptadas

-   [x]  Aceptamos seguir el código de conducta (<https://eslib.re/conducta>) durante nuestra participación en el congreso
