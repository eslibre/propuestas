---
layout: 2023/post
section: proposals
category: devrooms
author: Oficina de Software Libre de la Universidad de Zaragoza
title: Concurso TFG relacionados con Tecnología Libre
---

# Concurso TFG relacionados con Tecnología Libre

## Detalles de la propuesta:

>Esta sala está dedicada al Iº Concurso de TFG relacionados con Tecnología Libre que hemos puesto en marcha en la Universidad de Zaragoza. Principalmente será para que los estudiantes ganadores presenten sus TFG y se les haga entrega del premio correspondiente. Este premio consiste en 3 ordenadores portátiles para los 3 mejores TFG relacionados con Tecnología Libre (hardware y software).

-   Formato:

>La idea es comenzar con una presentación del concurso, el jurado y el patrocinador. Una visión general de los trabajos presentados y las normas de valoración.<br><br>
Seguidamente los estudiantes ganadores harán una breve presentación del TFG con exposición de las ideas principales y sus resultados, así como la aportación al mundo de la tecnología libre. Por fin, se haría la entrega de premios y diplomas.<br><br>
La duración será de un máximo de 2 horas. El día no lo tenemos pensado, quizás sea mejor el sábado porque facilita los desplazamientos. Lo podemos concretar con la organización.

-   Público objetivo:

>Será interesante para los estudiantes, universitarios y no universitarios, así como técnicos interesados en las últimas tendencias tecnológicas (por lo menos dentro del ámbito docente).

## Comunidad que propone la sala:

### Oficina de Software Libre de la Universidad de Zaragoza

>OSLUZ es el organismo de la Universidad de Zaragoza encargado de promover el uso de software libre en el ámbito universitario.<br><br>
Al cargo de la sala y de OSLUZ está Angel Bailo García. Contará con el apoyo del resto del Servicio de Informática y del Departamento de Informática y de Ingeniería de Sistemas que participa en la organización del Concurso.

-   Web: <https://osluz.unizar.es>
-   Twitter: <https://twitter.com/@Osluz_unizar>

## Condiciones aceptadas

-   [x]  Aceptamos seguir el código de conducta (<https://eslib.re/conducta>) durante nuestra participación en el congreso
