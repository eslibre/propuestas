---
layout: 2023/post
section: proposals
category: workshops
author: Carlos López Quintanilla
title: Introducción a QGIS
---

### Redirigiendo a la página de la sala...

[Acceder directamente](https://propuestas-libregeo.eslib.re/2023/talleres/introduccion-qgis)

<script>
  document.body.onload = function() {
    window.location = "https://propuestas-libregeo.eslib.re/2023/talleres/introduccion-qgis";
  }
</script>
