---
layout: 2023/post
section: proposals
category: workshops
author: Alejandro López
title: Reparando mi propio ordenador portátil con Slimbook
---

# Reparando mi propio ordenador portátil con Slimbook

>En este taller de 2 horas, aprenderemos a abrir y desmontar un ordenador portátil. Usaremos algunas unidades de SLIMBOOK, pero la teoría es muy similar en otras marcas, por lo que, si lo deseas, traete tu ordenador y te ayudaremos. El mantenimiento de un ordenador portátil, pasa pro una limpieza correcta, y es algo tan importante, como las revisiones de un vehículo.

## Detalles de la propuesta:

-   Tipo de propuesta: Taller / Presencial
-   Idioma: Español

-   Descripción:

>Además, llevaremos equipamiento, como destornilladores, paletillas, pinzas, y pasta térmica, para el que se atreva a cambiarla!

-   Web del proyecto: <https://www.slimbook.es>

## Ponente:

-   Nombre: Alejandro López

-   Bio:

>Fundador en 2015 de Slimbook, aunque usuario de Debian desde 2003, Alejandro es un programador que se adentró en los caminos del pingûino, y apostó por proveer de hardware compatible y de alta calidad, a nuestras queridas distribuciones, olvidadas por las grandes marcas.

<p style="padding-top: 5px; text-align: center; font-size: 1.20rem;"><strong>Por favor, si tienes interés en este taller apúntate para asegurar tu plaza y facilitarnos la gestión de su realización<br>(además de registrarte también en el congreso si tienes interés en participar en otras actividades).</strong></p>

-   Horario: Viernes 12 de mayo de 16:00 a 18:00 en el Aula 4 de Etopia.
-   Formulario de inscripción para el taller: <https://eventos.librelabgrx.cc/events/998f6e8c-fd3f-4a56-b580-f961b17103f1>
-   Registro de asistentes al congreso: <https://eventos.librelabgrx.cc/events/798b67b2-21d2-4811-8201-7e1e14c69458>

### Info personal:

-   Mastodon (u otras redes sociales libres): <https://linuxrocks.online/@slimbook>
-   Twitter: <https://twitter.com/@slimbook>
-   GitLab (u otra forja) o portfolio general: <https://github.com/Slimbook-Team>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
