---
layout: 2023/post
section: proposals
category: workshops
author: Mauricio Baeza
title: Mis primeras macros en LibreOffice, con Python
---

# Mis primeras macros en LibreOffice, con Python

>Escribir macros en LibreOffice con Python con un conocimiento básico de Python y de desarrollo.

## Detalles de la propuesta:

-   Tipo de propuesta: Taller / Presencial
-   Idioma: Español

-   Descripción:

>El API de LibreOffice es uno de los más grandes de las aplicaciones de Software Libre. Dada su herencia de C++, tiene todavía bastantes áreas donde falta concisión y sencillez para su uso más claro. Con Python podemos hacer el camino más fluido para quien quiera probar todo el potencial de LibreOffice.

-   Web del proyecto: <https://doc.cuates.net/easymacro/es/>

-   Público objetivo:

>Cualquier usuario de LibreOffice o desarrollador de Python que quiera interactuar con LibreOffice.

<p style="padding-top: 5px; text-align: center; font-size: 1.20rem;"><strong>Por favor, si tienes interés en este taller apúntate para asegurar tu plaza y facilitarnos la gestión de su realización<br>(además de registrarte también en el congreso si tienes interés en participar en otras actividades).</strong></p>

-   Horario: Viernes 12 de mayo de 18:45 a 20:15 en el Aula 6 de Etopia.
-   Formulario de inscripción para el taller: <https://eventos.librelabgrx.cc/events/43f7d497-eaa3-4a2c-a729-2eeae4fba723>
-   Registro de asistentes al congreso: <https://eventos.librelabgrx.cc/events/798b67b2-21d2-4811-8201-7e1e14c69458>

## Ponente:

-   Nombre: Mauricio Baeza

-   Bio:

>Desarrollador de Software Libre. Miembro de la TDF (The Document Foundation). Expositor en las conferencias Latinoamericanas de LibreOffice Asunción, Paraguay 2019 y Brasilia, Brasil en 2022.
Bibilofilo, cinefilo, melomano...

### Info personal:

-   Web personal: <https://cuates.net>
-   Mastodon (u otras redes sociales libres): <https://mstdn.mx/@elmau>
-   GitLab (u otra forja) o portfolio general: <https://git.cuates.net/elmau>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
