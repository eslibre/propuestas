---
layout: 2023/post
section: proposals
category: workshops
author: Juan José Martín Romero
title: Presentaciones libres con Sozi
---

### Redirigiendo a la página de la sala...

[Acceder directamente](https://propuestas-vitalinux.eslib.re/2023/talleres/presentaciones-libres-sozi)

<script>
  document.body.onload = function() {
    window.location = "https://propuestas-vitalinux.eslib.re/2023/talleres/presentaciones-libres-sozi";
  }
</script>
