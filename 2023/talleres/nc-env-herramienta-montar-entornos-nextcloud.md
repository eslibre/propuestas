---
layout: 2023/post
section: proposals
category: workshops
author: Pietro Marini
title: nc-env&#58; una herramienta para montar entornos Nextcloud en tu ordenador
---

# nc-env: una herramienta para montar entornos Nextcloud en tu ordenador

<p style="padding-top: 5px; text-align: center; font-size: 1.5rem;"><strong>Toda la información actualizada y detallada sobre el taller se encuentra <a href="https://codeberg.org/pmarini/nc-env/wiki/Taller-esLibre-2023" target="_blank">aquí</a>.</strong></p>

>Much@s de nosotr@s, con algo de conocimientos técnicos, ya tenemos y gestionamos una instalación de Nextcloud en nuestras casas o en nuestras oficinas. La puesta en servicio de una instancia básica no esta complicada, pero si queremos ofrecer un servicio completo y de calidad a nuestros usuarios, que aproveche plenamente de las funcionalidades ofrecidas por la plataforma, deseamos disponer de una herramienta que nos proporcione entornos de prueba que nos permiten validar nuestros escenarios de despliegue.<br><br>
nc-env es una herramienta de código abierto concebida para responder a esta exigencia y que no requiere recursos de computación en la nube. En este taller haremos una introducción, seguiremos los pasos de instalación y desplegaremos un entorno Nextcloud aprovechando unos de sus templates.

## Detalles de la propuesta:

-   Tipo de propuesta: Taller / Presencial
-   Idioma: Español

-   Descripción:

<p style="padding-top: 5px; text-align: center; font-size: 1.5rem;"><strong>Toda la información actualizada y detallada sobre el taller se encuentra <a href="https://codeberg.org/pmarini/nc-env/wiki/Taller-esLibre-2023" target="_blank">aquí</a>.</strong></p>

-   Web del proyecto: <https://codeberg.org/pmarini/nc-env/>

-   Público objetivo:

>Personas con conocimientos en entornos Linux y habilidades a linea de comando. Idealmente sysadmins o arquitectos encargados o interesados al mantenimiento y operaciones del aplicativo Nextcloud

<p style="padding-top: 5px; text-align: center; font-size: 1.20rem;"><strong>Por favor, si tienes interés en este taller apúntate para asegurar tu plaza y facilitarnos la gestión de su realización<br>(además de registrarte también en el congreso si tienes interés en participar en otras actividades).</strong></p>

-   Horario: Sábado 13 de mayo de 12:00 a 14:00 en el Aula 3 de Etopia.
-   Formulario de inscripción para el taller: <https://eventos.librelabgrx.cc/events/2f16da2a-b360-4c31-a1b4-69953a345965>
-   Registro de asistentes al congreso: <https://eventos.librelabgrx.cc/events/798b67b2-21d2-4811-8201-7e1e14c69458>

## Ponente:

-   Nombre: Pietro Marini

-   Bio:

>Tras licenciarse en Física en 2011 en la Universidad de Pavía (Italia), Pietro ha trabajado como desarrollador y consultor en el sector de la Business Intelligence y la analítica de datos para empresas de toda Europa. En la actualidad vive con su familia en Galicia. Orientado al cliente, autodidacta y apasionado del mundo del software de código abierto, Pietro se ha incorporado al equipo Nextcloud GmbH en Junio 2021 y disfruta trabajando con los clientes para resolver los desafíos técnicos más complejos.

### Info personal:

-   GitLab (u otra forja) o portfolio general: <https://codeberg.org/pmarini/>
-   Mastodon: <https://mastodon.social/@pmarini/>

## Comentarios

>- Duración taller: 2 horas

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
