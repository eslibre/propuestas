---
layout: 2023/post
section: proposals
category: workshops
author: Ricardo J. Rodríguez
title:  Desarrollo de plugins para análisis forense personalizado con Volatility
---

#  Desarrollo de plugins para análisis forense personalizado con Volatility

>El análisis forense de volcados de memoria es una parte más de las fases de respuesta a incidentes. En determinados escenarios puede ser, además, una de las únicas fuentes posibles a investigar en un incidente (por ejemplo, ante la búsqueda de determinadas claves de cifrado o de malware file-less). En este taller se va a indagar en el análisis forense de memoria con objeto de extracción de indicadores de compromiso que sirvan para detectar la presencia de software dañino en el sistema analizado. Como herramienta para el análisis se utilizará Volatility3, estándar de facto en el campo de análisis forense de memoria. Se comentará cómo se pueden desarrollar nuestros propios plugins de análisis para esta herramienta, enseñándose a los participantes cómo tienen que desarrollar nuevos plugins para usos específicos y determinados que requieran en determinados incidentes de seguridad.

## Detalles de la propuesta:

-   Tipo de propuesta: Taller / Presencial
-   Idioma: Español

-   Descripción:

> Taller de 2 horas cuyo contenido se adaptará de un taller previo de mayor duración y más especializado (ver web del proyecto).

-   Web del proyecto: <https://webdiis.unizar.es/~ricardo/sbc-2022/malware-forense-memoria/>

-   Público objetivo:

>Público técnico con interés en temas de ciberseguridad

<p style="padding-top: 5px; text-align: center; font-size: 1.20rem;"><strong>Por favor, si tienes interés en este taller apúntate para asegurar tu plaza y facilitarnos la gestión de su realización<br>(además de registrarte también en el congreso si tienes interés en participar en otras actividades).</strong></p>

-   Horario: Sábado 13 de mayo de 12:00 a 14:00 en el Aula 6 de Etopia.
-   Formulario de inscripción para el taller: <https://eventos.librelabgrx.cc/events/cd50590e-3efa-4be1-914c-1c38033960ec>
-   Registro de asistentes al congreso: <https://eventos.librelabgrx.cc/events/798b67b2-21d2-4811-8201-7e1e14c69458>

## Ponente:

-   Nombre: Ricardo J. Rodríguez

-   Bio:

>Ricardo J. Rodríguez es Doctor en Informática e Ingeniería de Sistemas por la Universidad de Zaragoza desde 2013. Actualmente, trabaja como Profesor Titular en la misma universidad. Sus intereses de investigación incluyen el análisis de sistemas complejos, con especial énfasis en el rendimiento y su seguridad, el forense digital y el análisis de aplicaciones binarias. Participa como ponente habitual y profesor de talleres técnicos en numerosas conferencias de seguridad del sector industrial, como NoConName, Hack.LU, RootedCON, Hack in Paris, MalCON, SSTIC CCN-CERT, o Hack in the Box Amsterdam, entre otras. Lidera una línea de investigación dedicada a seguridad informática en la Universidad de Zaragoza (https://reversea.me).

### Info personal:

-   Web personal: <https://ricardojrdez.github.io/>
-   Twitter: <https://twitter.com/RicardoJRdez>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
