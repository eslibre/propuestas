---
layout: 2023/post
section: proposals
category: workshops
author: José Pujol, Jorge Lobo, Xabier Rosas
title: Robótica educativa e IA con Echidna
---

# Robótica educativa e IA con Echidna

>En este taller exploramos la conexión entre el mundo físico y el digital, a través de distintas propuestas que incluyen el uso de entornos gráficos de programación e inteligencia artificial.

## Detalles de la propuesta:

-   Tipo de propuesta: Taller / Presencial
-   Idioma: Español

-   Descripción:

>En este taller de 2 horas exploraremos la conexión entre el mundo físico y el digital mediante software y hardware libre, como son el entorno gráfico de programación EchidnaScratch y la placa microcontroladora Echidna Black.<br><br>
A través de varias actividades conoceremos el funcionamiento de algunos de los sensores y actuadores de la placa, para terminar con un pequeño proyecto que incluirá el uso de Machine Learning que es una de las disciplinas de la inteligencia artificial.<br><br>
Material necesario: los asistentes deben llevar al menos un ordenador por pareja con el IDE de  Arduino y Echidna Link  instalados (para utilizar EchidnaScratch Online). En caso de que en algún equipo diera algún problema de conexión utilizaríamos Snap4Arduino (offline).<br><br>
Recursos:<br>
- Arduino: <https://www.arduino.cc/en/Main/Software><br>
- Snap4Arduino: <http://snap4arduino.rocks/><br>
- EchidnaLink: <https://echidna.es/a-programar/echidnascratch/como-empezar/echidnalink/><br>
- EchidnaScratch: <https://scratch.echidna.es/><br>

-   Web del proyecto: <https://echidna.es/>

-   Público objetivo:

>Cualquier persona que se sienta atraída por el mundo de la robótica, puede resultar especialmente interesante para docentes. No se requiere ningún conocimiento previo, pero resultará más fácil si se tiene alguna experiencia con entornos gráficos de programación, como Scratch o Snap. Serán bienvenidas niñas y niños a partir de 8 años acompañados por un adulto.<br><br>
Se trabajará por parejas y los asistentes deben llevar al menos un ordenador por pareja con el IDE de Arduino y EchidnaLink instalado.

<p style="padding-top: 5px; text-align: center; font-size: 1.20rem;"><strong>Por favor, si tienes interés en este taller apúntate para asegurar tu plaza y facilitarnos la gestión de su realización<br>(además de registrarte también en el congreso si tienes interés en participar en otras actividades).</strong></p>

-   Horario: Sábado 13 de mayo de 16:00 a 18:00 en el Aula 3 de Etopia.
-   Formulario de inscripción para el taller: <https://eventos.librelabgrx.cc/events/769cacee-9af8-4275-905b-b1493236dacd>
-   Registro de asistentes al congreso: <https://eventos.librelabgrx.cc/events/798b67b2-21d2-4811-8201-7e1e14c69458>

## Ponente:

-   Nombre: José Pujol, Jorge Lobo, Xabier Rosas

-   Bio:

>- Jorge Lobo: Maestro de Educación Primaria. Jorge es uno de los profesores pioneros en la introducción de robótica educativa e impresión 3D en Primaria.
Imparte cursos sobre robótica educativa. Participa también en el proyecto Escornabot. Imparte formación sobre robótica educativa e IA a docentes.<br><br>
- Xabier Rosas: Profesor de Ciclos Formativos Electrónica. Xabier cuenta con un amplio bagaje en el mundo del open source ha desarrollado el proyecto Escornabot y es el responsable del desarrollo electrónico en Echidna. Imparte formación sobre robótica educativa.<br><br>
- Jose Pujol: Profesor de Tecnología Educación Secundaria.  Jose lleva introduciendo programación y robótica en sus clases usando hardware libre desde el 2006. Desarrolló el proyecto Kiwibot. Imparte formación sobre robótica educativa.


### Info personal:

-   Twitter: <https://twitter.com/echidnaSTEAM>
-   GitLab (u otra forja) o portfolio general: <https://github.com/EchidnaShield>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
