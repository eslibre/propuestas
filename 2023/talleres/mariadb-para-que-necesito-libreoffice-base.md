---
layout: 2023/post
section: proposals
category: workshops
author: Juan C. Sanz
title: ¿Si tengo MySQL o MariaDB para qué necesito LibreOffice Base?
---

# ¿Si tengo MySQL o MariaDB para qué necesito LibreOffice Base?

>Taller de hora y media de duración en el que se instalarán MariaDB y LibreOffice y se muestra como LibreOffice Base puede facilitar la manipulación de bases de datos de servidor.

## Detalles de la propuesta:

-   Tipo de propuesta: Taller / Presencial
-   Idioma: Español

-   Descripción:

>
* Instalación y configuración de MariaDB en Ubuntu
* Instalación de LibreOffice y la extensión EasyMariaDB
* Interacción directa con MariaDB, creación de una base de datos y una tabla
* Conexión e interacción con la base de datos mediante Dbeaver
* Conexión e interacción con el servidor MariaDB a través de LibreOffice
    - Creación de una base de datos y una tabla
    - Permisos de usuarios
    - Creación de un formulario para introducir datos en la tabla
* Análisis de la anterior

-   Público objetivo:

>Cualquiera persona interesada en las bases de datos o en LibreOffice

<p style="padding-top: 5px; text-align: center; font-size: 1.20rem;"><strong>Por favor, si tienes interés en este taller apúntate para asegurar tu plaza y facilitarnos la gestión de su realización<br>(además de registrarte también en el congreso si tienes interés en participar en otras actividades).</strong></p>

-   Horario: Sábado 13 de mayo de 10:00 a 11:30 en el Aula 4 de Etopia.
-   Formulario de inscripción para el taller: <https://eventos.librelabgrx.cc/events/285b1120-93bf-42ae-9b66-0071be11b767>
-   Registro de asistentes al congreso: <https://eventos.librelabgrx.cc/events/798b67b2-21d2-4811-8201-7e1e14c69458>

## Ponente:

-   Nombre: Juan C. Sanz

-   Bio:

>- Técnico especialista en informática de gestión (Titulación MEC).
- Certified Professional Trainer de LibreOffice
- Colaborador de LibreOffice desde los inicios y miembro de The Document Foundation, antes colaborador de OpenOffice
- He trabajado como programador de aplicaciones en las varias empresas y como responsable de informática (más de 40 PC y servidores) en un organismo público

### Info personal:

-   GitLab (u otra forja) o portfolio general: <https://easymariadb.github.io/>

## Comentarios

>Para poder realizar el taller es necesaria una conexión a internet que permita la descarga e instalación de las aplicaciones. Si la conexión no fuera posible, se realizaría el taller con las aplicaciones instaladas, pero no se conseguirá el objetivo de quitar el miedo a la instalación y configuración.

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
