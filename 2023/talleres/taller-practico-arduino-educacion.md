---
layout: 2023/post
section: proposals
category: workshops
author: Elena Lopez de Arroiabe, Aitziber Ruiz, Joseba Mouriz, Maria Gutiérrez-Segú
title: Taller práctico de Arduino Educación
---

### Redirigiendo a la página de la sala...

[Acceder directamente](https://propuestas-vitalinux.eslib.re/2023/talleres/taller-practico-arduino-educacion)

<script>
  document.body.onload = function() {
    window.location = "https://propuestas-vitalinux.eslib.re/2023/talleres/taller-practico-arduino-educacion";
  }
</script>
