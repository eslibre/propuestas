---
layout: 2023/post
section: proposals
category: workshops
author: Ismael Fanlo
title: ¡Festival de tablas dinámicas!
---

# ¡Festival de tablas dinámicas!

>El análisis de datos con LibreOffice Calc puede ser muy divertido si sabes utilizar tablas dinámicas. Las tablas dinámicas te permitirán resumir la información y obtener toda clase de cálculos ¡sin escribir ni una sola fórmula!<br><br>
De la misma manera que Sheldon Cooper tenía su canal "Fun with flags", aquí tenemos un "Fun with Calc".  

## Detalles de la propuesta:

-   Tipo de propuesta: Taller / Presencial
-   Idioma: Español

-   Descripción:

>Se propone un taller de una hora de duración sobre el trabajo con tablas dinámicas en LibreOffice Calc, tratando también la creación de gráficos dinámicos vinculados a las tablas resultantes. Se aportarán previamente materiales de prácticas para que las personas participantes puedan seguir y reproducir las mismas en directo.  Se expondrán ventajas y limitaciones con relación a la hoja de cálculo dominante del mercado (Excel) para que personas usuarias de esta última cojan confianza en el uso de Calc.<br><br>
Materiales para el taller disponibles aquí: <https://cloud.aulatraining.com/index.php/s/69NYKR3SdbxrpKG>

-   Web del proyecto: <https://es.libreoffice.org/>

-   Público objetivo:

>* Cualquier persona interesada en el trabajo con la hoja de cálculo de LibreOffice.  
* Contables, personal de administración, de estadística y, en general, cualquier persona relacionada con el análisis de datos.
* Personas acostumbradas a Microsoft Excel interesadas a migrar a LibreOffice Calc.

<p style="padding-top: 5px; text-align: center; font-size: 1.20rem;"><strong>Por favor, si tienes interés en este taller apúntate para asegurar tu plaza y facilitarnos la gestión de su realización<br>(además de registrarte también en el congreso si tienes interés en participar en otras actividades).</strong></p>

-   Horario: Viernes 12 de mayo de 10:30 a 11:30 en el Aula 3 de Etopia.
-   Formulario de inscripción para el taller: <https://eventos.librelabgrx.cc/events/4189ea17-bffc-432c-822f-1f52d8ee1e66>
-   Registro de asistentes al congreso: <https://eventos.librelabgrx.cc/events/798b67b2-21d2-4811-8201-7e1e14c69458>

## Ponente:

-   Nombre: Ismael Fanlo

-   Bio:

>Viejo contable reciclado en profesor de ofimática con el cambio de milenio. Su entusiasmo por el software libre, en general, y por Open/LibreOffice, en particular, le han llevado a impartir charlas, conferencias y talleres en diferentes ciudades españolas.<br><br>
También participó muy activamente en las listas de correo y foros de OpenOffice.org en español, colaborando en la resolución de problemas de los usuarios del programa. Fruto de esta dedicación, en mayo de 2009 asumió diferentes responsabilidades dentro del proyecto OpenOffice.org:
* Contacto de Marketing para España del proyecto OpenOffice.org
* Dirección para España de oooES (OpenOffice.org en español)<br><br>
Desde 2011, con la creación del proyecto LibreOffice, bifurcado desde OpenOffice.org, renuncia a estas responsabilidades para poder impulsar ambos proyectos manteniendo plena independencia. Excusa formidable para disimular su profundo desagrado por las actividades comerciales :-D<br><br>
Está certificado por Microsoft como Master MOS (Microsoft Office Specialist) y MCT (Microsoft Certified Trainer), lo que le permite afrontar con garantías de éxito proyectos formativos orientados a migraciones corporativas desde Microsoft Office a Open/LibreOffice, al ser un experto conocedor de ambos entornos.<br><br>
Desde 2019 está certificado como LibreOffice Professional Trainer y se siente muy orgulloso de haber publicado sus cursos online de Writer y Calc en <https://oficinalibre.net>.<br><br>
También desde 2019 es miembro oficial de The Document Foundation<br><br>
P.S. Lamentablemente, sigue ganándose la vida con el software privativo :-(

### Info personal:

-   Web personal: <https://ifanlo.com>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
