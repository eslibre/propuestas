---
layout: 2023/post
section: proposals
category: workshops
author: Ismail Ali Gago
title: Elaboración de REAs con herramientas de autoría abiertas
---

# Elaboración de REAs con herramientas de autoría abiertas

>Elaboración de Recursos Educativos Abiertos en formato digital e interactivos utlizando eXeLearning (https://exelearning.net/), aplicación integrada en MAX (https://www.educa2.madrid.org/web/max) y su complementación con las aulas virtuales (basadas en Moodle) y espacios de alojamiento web (Liferay) en Educamadrid (https://www.educa2.madrid.org/educamadrid/).<br><br>
Todo un ecosistema basado en software libre para la generacion, publicación y difusión de contendidos educativos abiertos e interactivos en formato digital.<br><br>
Adjunto ejemplo de REA elaborado con eXeLearnig y publicado en mi espacio web de Educamadrid:<br><br>
Trabajando los ODS en un Instituto de Enseñanza Secundaria: <https://www.educa2.madrid.org/web/ismail.ali/trabajando-los-ods>

## Detalles de la propuesta:

-   Tipo de propuesta: Taller / Presencial
-   Idioma: Español

-   Descripción:

>Taller de 1 hora de duración sobre elaboración de Recursos Educativos Abiertos en formato digital e interactivos utlizando eXeLearning (https://exelearning.net/), aplicación integrada en MAX (https://www.educa2.madrid.org/web/max) y su complementación con las aulas virtuales (basadas en Moodle) y espacios de alojamiento web (Liferay) en Educamadrid (https://www.educa2.madrid.org/educamadrid/).<br><br>
Todo un ecosistema basado en software libre para la generacion, publicación y difusión de contendidos educativos abiertos e interactivos en formato digital.<br><br>
Adjunto ejemplo de REA elaborado con eXeLearnig y publicado en mi espacio web de Educamadrid.

-   Web del proyecto: <https://www.educa2.madrid.org/web/ismail.ali/trabajando-los-ods>

-   Público objetivo:

>- Profesorado y alumnado de todos los niveles y etapas educativas.
- Desarroladores de software libre aplicado a la educación.
- Cualquier persona interesada en las aplicaciones de las Tecnologías de la Información y la Comunicación en la docencia.
- Cualquier persona interesada en el software libre y en el conocimiento libre y abierto.
- STEAM, TIC, OpenScience.

<p style="padding-top: 5px; text-align: center; font-size: 1.20rem;"><strong>Por favor, si tienes interés en este taller apúntate para asegurar tu plaza y facilitarnos la gestión de su realización<br>(además de registrarte también en el congreso si tienes interés en participar en otras actividades).</strong></p>

-   Horario: Viernes 12 de mayo de 12:00 a 13:00 en el Aula 5 de Etopia.
-   Formulario de inscripción para el taller: <https://eventos.librelabgrx.cc/events/27db41a8-7539-4a2a-9da2-8d84ea7da770>
-   Registro de asistentes al congreso: <https://eventos.librelabgrx.cc/events/798b67b2-21d2-4811-8201-7e1e14c69458>

## Ponente:

-   Nombre: Ismail Ali Gago

-   Bio:

>Profesor de Biología y Geología a nivel de enseñanza secundaria. También he sido profesor Asociado de Nuevas Tecnologías aplicadas a la Educación en la Facultad de Formación de Profesorado de la Universidad Autónoma de Madrid.<br>
- Promotor y primer coordinador del Grupo de Desarrollo MAX MAdrid_linuX (https://www.educa2.madrid.org/web/max)<br>
- Promotor y primer coordinador del Grupo de Desarrollo eXeLearning (https://exelearning.net/)<br>
- Actulmente sigo colabaorando con ambos grupos y como formador de formadores en las áreas TIC y STEAM<br>
- Scientix Ambassador

### Info personal:

-   Web personal: <https://www.educa2.madrid.org/web/ismail.ali> / <https://es.linkedin.com/in/ismail-ali-gago-3aa5862b>
-   Twitter: <https://twitter.com/@ismagago>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
